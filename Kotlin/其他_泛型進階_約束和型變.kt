/*
#   泛型約束(Generic constraints) : 限定泛型類型，將泛型參數限定為某幾個特定類型
    https://www.jianshu.com/p/1042ac6586ef
	-	設定上限 (上限，指限定只接受宣告類型的子類型)，語法，<T:類型>
		Kotlin 的泛型約束和類的繼承一樣，使用：代對泛型的的類型進行約束
		例如，class SwipeRefreshableView<T : View>{ ... }，限定只接受 View 類或其子類

	-	設定多個上限，where
		Kotlin 支持多個類型的上界約束,,使用where關鍵字
		例如，
		class SwipeRefreshableView<T>
			where T : View,
				  T : Refreshable {
		}

#   泛型型變 : 父子類型的相容性 (協變 和 逆變)
	什麼是協變 (輸出類型必為輸入類型的子類，父->子)
	協變，返回類型一定是輸入類型的子類

	什麼是逆變 (定義為父類，帶入為子類，子->父)
	逆變，調用泛型時，將T設置為父類類型，但賦值時，傳入的類型卻是子類 (把子類帶到父類中)

#   kotlin 的型變，泛型中的 in 和 out，
	-	in/out 用來標註類型是否可轉變，和該如何轉變，讓使用泛型的類或函數中，可以使用不同類型(變型)
	    kotlin 默認的泛型是不變，即輸入類型與返回類型同，若要使用型變，必須在建立泛型類時，使用 in/out 來顯式宣告
	-	記憶，
		<in T>，in 代表泛型參數T 只能放在內部方法的引數類型，不可放在返回類型
		函數的輸入類型可以與返回值類型不同，例如，定義的泛型是父類，傳入的類型是子類，代表逆變
		消費者 consume = input = in

		<out T>，out 代表泛型參數T 只能放在內部方法的返回類型，不可放在引數類型
		返回值類型必須是輸入類型的子類，例如，定義的泛型是子類，傳入的類型是父類，代表協變
		生產者 produce = output = out

	- <in T> 逆變範例，
		// 定義泛型類
		abstract class Comparable<in T> {
			// 使用in的话，T只能放在引數類型
			abstract fun compareTo(other: T): Int
		}

		// 調用泛型類
		fun demo(x: Comparable<Number>) {
			// 1.0 是 Double 類型，是 Number 的子類型，因此可以將 Double(子) 賦值給 Number(父)
			// 逆變，把子類型的值帶到父類型的宣告中
			x.compareTo(1.0)
			val y: Comparable<Double> = x
		}

	- <out T> 協變範例，
		abstract class Source<out T> {
			// 使用out的话，T只能放在返回值類型
			abstract fun nextT(): T
		}

		fun demo(strs: Source<String>) {
			val objects: Source<Any> = strs // This is OK, since T is an out-parameter
		}

#   調用處聲明 in/out，又稱為類型投射(Type projection)
    對於泛型類，需要同時在 1_輸入引數類型 和 2_返回類型使用泛型，不方便在建立泛型類時使用 in/out時，
    則可以在調用時使用 in/out 來加以限定該類型在泛型類中的位置
    範例請參考，http://www.liying-cn.net/kotlin/docs/reference/generics.html


#   泛型中的通配符(*)，或稱 星號投射(Star-projection)
		使用時機，在泛型調用時，不確定具體的泛型類型，卻又需要調用泛型類
	-	若泛型定義為 Foo<out T>，使用 Foo<*>調用時，自動將 Foo<*> 轉譯為 Foo<out TUpper>
		代表自動讀取T的類型，並限定T的類型為T的上限
	-	若泛型定義為 Foo<in T>，使用 Foo<*>調用時，自動將 Foo<*> 轉譯為 Foo<in Nothing>
		代表不能向 Foo<*> 中寫入任何東西
	-	若泛型定義為 Foo<T>，Foo<*> 代表 Foo<out TUpper> 和 Foo<in Nothing>

	範例，https://wafer.li/Kotlin/Kotlin%20%E6%B3%9B%E5%9E%8B/
		若泛型接口宣告為 interface Function<in T, out U>，則調用時
		Function<*, String> = Function<in Nothing, String>
		Function<String, *> = Function<String, out Any?>
		Function<*, *>		= Function<in Nothing, out Any?>
*/

// ===== out example =====
// CovariantHolder<out A>，宣告 CovariantHolder 使用協變泛型，且該泛型類型A，只能在放返回類型的地方
//	(val a: A)使用泛型的參數為 a
class CovariantHolder<out A>(val a: A) {
    // 因為 <out A>，使用泛型的地方只能放在返回類型的地方
    fun foo(): A {
        return a
    }
}

// ===== in example =====
class ContravarintHolder<in A>(a: A) {
    fun foo(a: A) {
    }
}

fun main(args: Array<String>) {
    out_example()
    in_example()
}

fun out_example(){
    var strCo: CovariantHolder<String> = CovariantHolder("a")
    var anyCo: CovariantHolder<Any> = CovariantHolder<Any>("b")
    anyCo = strCo
}

fun in_example(){
    var strDCo = ContravarintHolder("a")
    var anyDCo = ContravarintHolder<Any>("b")
    strDCo = anyDCo
}
