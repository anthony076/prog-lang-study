## 進程間通訊

- 不同進程之間通訊的幾種方法
  - 方法，透過 [socket](#方法socket)，透過網路進行通訊
  - 方法，透過 [memory-mapped-file](#方法memory-mapped-file)，將數據保存在進程的虛擬記憶體空間中，由進程自行管理，需要同步機制
  - 方法，透過 [pipe](#方法3pipe)，將數據保存在管道的緩衝區，由OS進行管理，適合簡單場景
  - 方法，透過 Message Queues，將數據保存在由內核管理的訊息佇列中，透過系統調用來訪問和操作內核中的訊息佇列，適合複雜場景
  - 方法，透過 Signal，一種用於通知進程事件發生的IPC機制

## 方法，socket 

參考，[socket基礎](https://gitlab.com/anthony076/linux-usage/-/blob/main/linux-core/10_network/10_network.md)

## 方法，memory-mapped-file

- 使用場景
  - memory-mapped-file 允许将整个文件或文件的一部分直接映射到进程的地址空间中，通过内存访问文件内容，而不需要使用传统的文件 I/O 操作。
  - 多个进程可以共享同一个 memory-mapped 文件，这使得进程间通信变得更加高效
  - 在 windows-api中，使用 memory-mapped-file 可以不建立文件的方式使用，詳見範例3
  - memory-mapped-file 是一種持久的通訊機制，映射文件的內容會存儲在磁盤上，即使所有使用它的進程都結束，映射文件的內容仍然存在於磁盤上

- 範例1，python 透過 mmap 與子進程進行數據共享

  ```python
  import mmap
  import os
  import time

  # 創建一個共享記憶體文件
  file_path = 'shared_memory.bin'
  file_size = 1000  # 共享記憶體的大小

  # 打開文件
  with open(file_path, 'wb') as f:
      f.seek(file_size - 1)
      f.write(b'\x00')  # 在文件末尾寫入一個空字節，以擴展文件大小
      f.flush()  # 確保數據被寫入磁盤

  with open(file_path, 'r+b') as f:
      # 將文件映射到記憶體中
      mm = mmap.mmap(f.fileno(), 0)

      # 在共享記憶體中寫入數據
      mm[:5] = b'Hello'

      # 創建一個子進程
      pid = os.fork()

      if pid == 0:  # 子進程
          # 子進程也映射同一個文件
          mm_child = mmap.mmap(f.fileno(), 0)

          # 在共享記憶體中讀取數據
          print("Child process reads:", mm_child[:5])

          # 修改共享記憶體中的數據
          mm_child[5:11] = b' World'

          # 解除記憶體映射
          mm_child.close()

          os._exit(0)  # 子進程退出

      else:  # 父進程
          # 等待子進程修改共享記憶體
          time.sleep(1)

          # 讀取共享記憶體中的數據
          print("Parent process reads:", mm[:])

          # 解除記憶體映射
          mm.close()

      # 等待子進程退出
      os.waitpid(pid, 0)

  # 刪除共享記憶體文件
  os.unlink(file_path)
  ```

- 範例2，python 讀取c進程數據

  c語言建立數據
  ```c
  #include <stdio.h>
  #include <sys/mman.h>
  #include <fcntl.h>
  #include <unistd.h>
  #include <string.h>
  #include <semaphore.h>

  #define SHARED_MEM_SIZE 1000
  #define DATA_SIZE 50
  #define SEM_NAME "/shared_sem"

  int main() {
      int fd;
      char *shared_memory;
      sem_t *sem;

      // 創建共享記憶體文件描述符
      fd = shm_open("/shared_memory", O_CREAT | O_RDWR, 0666);

      // 設定共享記憶體的大小
      ftruncate(fd, SHARED_MEM_SIZE);

      // 映射共享記憶體
      shared_memory = mmap(0, SHARED_MEM_SIZE, PROT_WRITE, MAP_SHARED, fd, 0);

      // 創建信號量
      sem = sem_open(SEM_NAME, O_CREAT | O_RDWR, 0666, 0);

      // 寫入數據到共享記憶體
      strncpy(shared_memory, "This is data from C process.", DATA_SIZE);

      // 發送信號量，錶示數據已寫入完畢
      sem_post(sem);

      // 等待信號量
      sem_wait(sem);

      // 解除記憶體映射
      munmap(shared_memory, SHARED_MEM_SIZE);

      // 關閉文件描述符
      close(fd);

      // 關閉信號量
      sem_close(sem);
      sem_unlink(SEM_NAME);

      return 0;
  }
  ```

  在 python 中透過 mmap 進行讀取
  ```python
  import mmap
  import os
  import time

  SEM_NAME = "/shared_sem"

  # 打開共享記憶體
  fd = os.open("/shared_memory", os.O_RDONLY)

  # 映射共享記憶體
  mm = mmap.mmap(fd, 0, mmap.MAP_SHARED, mmap.PROT_READ)

  # 打開信號量
  sem = None
  while sem is None:
      try:
          sem = os.open(SEM_NAME, os.O_RDWR)
      except FileNotFoundError:
          time.sleep(0.1)

  # 等待信號量
  while True:
      try:
          os.read(sem, 1)
          break
      except OSError as e:
          if e.errno == errno.EINTR:
              continue
          else:
              raise

  # 讀取共享記憶體中的數據
  data_from_c = mm[:].decode('utf-8')

  # 列印數據
  print("Data read from C process:", data_from_c)

  # 發送信號量通知給 C 程式中的sem_wait()
  os.close(sem)

  # 解除記憶體映射
  mm.close()

  # 關閉文件描述符
  os.close(fd)
  ```

- 範例3，利用 memory-mapped-file 寫一個進程之間的聊天室
  
  - from [nir9](https://gist.github.com/nir9/7f61b01c3299a8a3bcebc2e19151146f) 或 [介紹影片](https://www.youtube.com/watch?v=BSyLjD3NG88)
  
  - CreateFileMappingW()的輸入參數，hFile，不是必需的，可以不建立文件，並以INVALID_HANDLE_VALUE取代，
    詳見，[CreateFileMappingW的使用說明](https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-createfilemappingw)

  server.c
  ```C
  #include <Windows.h>
  #include <stdio.h>

  int main() {
    HANDLE handle = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 256, L"ChatSyncFileMap");

    LPVOID address = MapViewOfFile(handle, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    
    HANDLE event = CreateEventW(NULL, FALSE, FALSE, L"ChatSyncEvent");
    for (;;) {
      scanf_s("%255s", (char*)address, 256);
      SetEvent(event);
    }
  }
  ```

  client.C
  ```c
  #include <Windows.h>
  #include <stdio.h>

  int main() {
    HANDLE handle = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 256, L"ChatSyncFileMap");

    LPVOID address = MapViewOfFile(handle, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    
    HANDLE event = CreateEventW(NULL, FALSE, FALSE, L"ChatSyncEvent");
    for (;;) {
      WaitForSingleObject(event, INFINITE);
      printf("%s", (char*)address);
    }
  }
  ```

## 方法3，pipe

- 性質
  - 一種單向通訊機制，通常用於兩個相關的進程之間進行通訊。其中一個進程充當寫入端，另一個進程充當讀取端
  - 是臨時的通訊機制，它只在相關的進程存在時才存在
  - 常用於快速、簡單的進程間通訊，通常是具有父子關係的進程之間(非必要)

- 範例，透過 pipe 進行聊天
  
  [來源](https://www.geeksforgeeks.org/named-pipe-fifo-example-c-program/)
  
  先寫後讀
  ```c
  #include <stdio.h>
  #include <string.h>
  #include <fcntl.h>
  #include <sys/stat.h>
  #include <sys/types.h>
  #include <unistd.h>
  
  int main()
  {
      int fd;
  
      // FIFO file path
      char * myfifo = "/tmp/myfifo";
  
      // Creating the named file(FIFO)
      // mkfifo(<pathname>, <permission>)
      mkfifo(myfifo, 0666);
  
      char arr1[80], arr2[80];
      while (1)
      {
          // Open FIFO for write only
          fd = open(myfifo, O_WRONLY);
  
          // Take an input arr2ing from user.
          // 80 is maximum length
          fgets(arr2, 80, stdin);
  
          // Write the input arr2ing on FIFO
          // and close it
          write(fd, arr2, strlen(arr2)+1);
          close(fd);
  
          // Open FIFO for Read only
          fd = open(myfifo, O_RDONLY);
  
          // Read from FIFO
          read(fd, arr1, sizeof(arr1));
  
          // Print the read message
          printf("User2: %s\n", arr1);
          close(fd);
      }
      return 0;
  }
  ```

  先讀後寫
  ```c
  #include <stdio.h>
  #include <string.h>
  #include <fcntl.h>
  #include <sys/stat.h>
  #include <sys/types.h>
  #include <unistd.h>
  
  int main()
  {
      int fd1;
  
      // FIFO file path
      char * myfifo = "/tmp/myfifo";
  
      // Creating the named file(FIFO)
      // mkfifo(<pathname>,<permission>)
      mkfifo(myfifo, 0666);
  
      char str1[80], str2[80];
      while (1)
      {
          // First open in read only and read
          fd1 = open(myfifo,O_RDONLY);
          read(fd1, str1, 80);
  
          // Print the read string and close
          printf("User1: %s\n", str1);
          close(fd1);
  
          // Now open in write mode and write
          // string taken from user.
          fd1 = open(myfifo,O_WRONLY);
          fgets(str2, 80, stdin);
          write(fd1, str2, strlen(str2)+1);
          close(fd1);
      }
      return 0;
  }
  ```