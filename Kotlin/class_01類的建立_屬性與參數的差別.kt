
/*
# 記憶點
    - 屬性的宣告不是放在主構造函式，就是放在類的程式碼中手動宣告
    - 主構造函式的代碼放在 init區塊(init {})中

#1. 宣告class時，
    - 語法，class 類別名稱 constructor(屬性宣告, ...) { 程式碼 }
    - constructor 建構式的功能在於建立屬性(或把參數變成類的屬性)
      如果要手動建立屬性，就可以不需要主/次構造函式
    - 建立 class實例 不需要透過 new 關鍵字
    - 宣告時，沒有 可見性的修飾符 或 註解＠(用來添加中繼資料) 時，constructor 關鍵字可省略
    - 引數須透過 val/var 來定義，才會成為class的屬性，
      不透過 val/var，可以用來
        1_自定義該引數的 setter()/getteer
        2_變數延遲加載
        3_快速建立 class

#3. init 區塊(init{})，
    - 是主要建構函式的延伸區塊，
    - 主建構函式中聲明的參數，可以在 init區塊(init{})中直接使用
    - 主要用於 1_檢查 和 2_處理屬性值

#4. 建立次要建構函式的語法，在class 的程式碼區塊中使用以下
    - constrctor(var/val 引數名稱1：型態＝初始值, ...) : this(...){ 程式碼 }
    - class 可以有多個次構造函數

#5. 主要建構函式 和 次要建構函式 的差別
    - 主要建構函式 的位置在 class的 宣告中，主要建構函式的關鍵字    可省略，用於創建class實例
    - 次要建構函式 的位置在 class的 程式碼中，次要建構函式的關鍵字 不可省略，用於創建class實例時的額外工作
    - 主構造函式只能有一個，次構造函式可以有很多個
*/

//     class 類別名稱 constructor(屬性宣告, ...) { 程式碼 }
// #2. 建立 class實例 不需要透過 new 關鍵字，class 本身的 constructor 就是主要建構函式
// #3. 宣告時，主要建構函式的 constructor 關鍵字可省略
//     若使用 主要建構函式可見性的修飾符，如 public/private/internal 等，constructor 就不可省略
// #4. 引數須透過 val/var 來定義，才會成為class的屬性，請參考 Item04 class 的範例
//     除非是要自定義該引數的 setter()/getteer，就可以不透過 val/var，請參考 Item05 class 的範例
class Item01 constructor(var id:Long, var title:String, var content:String){
    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"
}

//  快速建立class的方式，
//  使用此方式建立class，引數需要給予預設值，
//  若不想給予預設值，需加上 lateinit，請參考 class_02屬性_getter_setter_私有變數(field)
class Item01_speed {
    var id:Long = 0
    var title = ""
    var content = ""

    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"

}


// ===== 使用初始化區塊(init{}) 的範例 =====
// 初始化區塊(init{}) 是主要建構函式的延伸區塊，主要用來 1_檢查 和 2_處理屬性值
class Item02 (var id:Long, var title:String, var content:String){

    // class 的初始化區塊
    init {
        title = if(title.isEmpty()) "Title require" else title
        content = if(content.isEmpty()) "Content require" else content
    }

    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"
}


// ===== 使用 次要建構函式() 的範例 =====
// 需要在建立物件時，執行一些額外的工作，就可以使用 次要建構函式
// 語法，在class 的程式碼區塊中使用以下
//      constrctor(var/val 引數名稱1：型態＝初始值, ...) : this(...){ 程式碼 }
//      其中，
//      constrctor()       是次要建構函數，不同於主要建構函式，主要建構函式是放在 class 的程式碼中
//      this               是用來呼叫主要建構函式，this 不可省略
class Item03 (var id:Long, var title:String, var content:String){
    // 宣告並執行主要建構函式， 透過 返回this 將執行完畢的結果，返回主要建構函式
    constructor(id:Long):this(id, "", "")

    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"
}


// ===== 主要建構式的參數 與 類別屬性 的差異 =====
// #1. 若要成為類別的屬性，要在宣告class時，透過 var/val 來定義引數，
//     否則只是定義 主要建構函數需要的參數而已 (即創建該class實例時必須帶入的參數)，
//     這些參數需要經過額外的處理才會成為 Class 的屬性
class Item04 (id:Long, title: String, content: String){

    // 沒有在宣告class時，透過 var/val 來定義引數，需要經過額外的處理才會成為 Class 的屬性
    val id = id
    val title = title
    val content = content
    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"
}


// ===== 自定義 setter()/getter() =====
// #1. 若要自定義該引數的 setter() 或 getter()，
//     則該引數在宣告class時，不要透過 val/var 宣告，不讓kotlin為該引數自動添加 setter() 或 getter()
//     讓該引數成為主要建構函式的參數就好，才有機會自行修改該引述的 setter() 或 getter()
class Item05 (var id:Long, _title:String, _content:String=""){
    var title = _title
        set(value:String) {
            if (value.isNotEmpty()){
                field = value   //field 是變數的默認值, 在getter()或setter()中才能訪問到
            }
        }

    var content = _content
        get() {
           return if(field.isEmpty()) "Empty" else field
        }

    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"
}

fun main(args: Array<String>) {

    // 宣告 class 的範例,
    val item01 = Item01(12345,"test", "hello World")
    println(item01.getDetails())

    // 快速建立 class 的範例
    val item01_speed = Item01_speed()
    println(item01_speed.getDetails())
    item01_speed.id = 5566
    println(item01_speed.getDetails())

    // 使用初始化區塊 的範例
    val item02 = Item02(5566,"", "")
    println(item02.getDetails())

    // 使用次要建構函式 的範例
    val item03 = Item03(7788)
    println(item03.getDetails())

    // 建構式的參數 與 類別屬性(須透過 val/var) 的差異
    val item04 = Item04(9900, "", "")
    println(item04.getDetails())

    // 自定義 setter()/getter()
    val item05 = Item05(3344, "Niceday")
    item05.title = ""
    println("title=${item05.title}")

    item05.content= ""
    println("content=${item05.content}")

    item05.content= "aa"
    println("content=${item05.content}")
}
