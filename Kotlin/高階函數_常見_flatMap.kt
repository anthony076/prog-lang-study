/*
常見高階函數
flatMap函數：可把帶區間的函數轉換成元素相同但不帶區間的列表
*/

fun main(args: Array<String>) {
    // 建立不同區間的列表
    // [1..5, 2..4, 100..104]
    val list = listOf( 1..5, 2..4, 100..104)

    // 把區間展開，並轉換成單行列表
    // [1, 2, 3, 4, 5, 2, 3, 4, 100, 101, 102, 103, 104]
    val flatList = list.flatMap {
        it
    }

    // 將單航列表透過map，為每個元素轉換成字元，並添加字串
    // [No.1, No.2, No.3, No.4, No.5, No.2, No.3, No.4, No.100, No.101, No.102, No.103, No.104]
    val flatList2 = list.flatMap {
        intRange -> intRange.map {
            intElement -> "No.$intElement"
        }
    }

    flatList.forEach(::println) //打印生成的列表
    flatList2.forEach(::println) //打印生成的列表2
}

