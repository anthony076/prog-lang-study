cmake_minimum_required(VERSION 3.10)
project( BoostPythonHelloWorld )
set(CMAKE_CXX_STANDARD 17)

# to fix hypot of cmath in python.h
# check https://stackoverflow.com/questions/10660524/error-building-boost-1-49-0-with-gcc-4-7-0/12124708#12124708 for details
#add_definitions(-D_hypot=hypot)


# Find python3 packages
find_package(PythonLibs 3.5 REQUIRED)
#find_package(PythonLibs REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})

# Find boost packages
#find_package(Boost REQUIRED)
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})
link_directories(C:/Python35/libs)

set(SOURCE_FILES greet_ext.cpp greet.cpp greet.h)

#set(PYTHON_INCLUDE_DIR "C:/Python35/include")

add_library(greet_ext SHARED ${SOURCE_FILES})
target_link_libraries(greet_ext ${Boost_LIBRARIES})
set_target_properties(greet_ext PROPERTIES PREFIX "")
