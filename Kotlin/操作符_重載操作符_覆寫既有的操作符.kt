/*
Ref:
https://www.kotlincn.net/docs/reference/operator-overloading.html
https://www.jianshu.com/p/d596b334df3a
*/

//  Complex class 為創建複數的類，Complex class 中有+(加法)的操作符
//  其中，
//  引數real 為複數中的實部數字
//  引數imaginary 為複數中的虛部數字
class Complex(var real: Double, var imaginary: Double){

    // 重新定義 plus (+) 且 被加數為 Comple類型 的操作符內容
    // 引數 others 為運算子後方的變數，使用該操作符時，運算子後方的變數會自動帶入 引數other 中
    /* 範例
        val c1 = Complex(3.0, 4.0)      // real = 3, imaginary = 4, c1 = 3+4i
        val c2 = Complex(2.0, 8.5)      // real = 2, imaginary = 8.4, c1 = 2+8.5i
        println(c1 + c2) //5 + 12.5i    // raal = 3, imaginary = 4, others = 2+8.5i
     */
    operator fun plus(other: Complex): Complex {
        return Complex(real + other.real, imaginary + other.imaginary)
    }

    // 重新定義 plus (+) 且 被加數為 Int類型 的操作符內容
    operator fun plus(other: Int): Complex {
        return Complex(real + other, imaginary + other)
    }

    // 重新定義 plus (+) 且 被加數為 Any類型 的操作符內容
    operator fun plus(other: Any): Int {
        return real.toInt()
    }

    operator fun invoke(): Double {
        return Math.hypot(real, imaginary)
    }

    override fun toString(): String {
        return "$real + ${imaginary}i"
    }
}

fun main(args: Array<String>) {
    val c1 = Complex(3.0, 4.0) //3 + 4i
    val c2 = Complex(2.0, 8.5) //2 + 8.5i

    println(c1 + c2) //5 + 12.5i
    println(c1 + 5) //8 + 9i
    println(c1 + "HelloWorld") //3

    println(c1()) //執行invoke()方法
}

