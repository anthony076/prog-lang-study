/*

    vector 的基本概念
    #1 透過 vector 建立的 object，使用自動記憶體管理

    #2 使用 vector 容易產生失效的狀況，請參考

    #3 部分 vector 的方法會用到 iterator, iterator是一個智慧指針
       智慧指針是將指針封裝在類中的物件，可以像指針一樣的操作，但具體是個物件

    #4 .size() 和 .capacity() 的差別
        .size() 顯示 vector 中的元素數量
        .capacity() 顯示 vector 佔用的記憶體空間大小

        vector 會自動配置需要的記憶體空間，只有在空間不夠的時候才會增加記憶體空間
        範例請見，add_element_example()

    #5 .clear()清空所有元素，但記憶體空間仍然保持不變
        可以透過 swap 的 方式，將指針指向另一個記憶體空間為0的vector，
        而原來佔用的記憶體空間會因為沒有被指針指向而被回收
        範例請見，empty_element() 和 release_vector()
        Ref : https://read01.com/o2GAj.html#.WvK2w9OFPY0

    #6  釋放 指針vector(內容物是指針的vector) 指向的記憶體空間
        如果vector中存放的是指針，那麼當vector銷毀時，這些指針指向的對像不會被銷毀，
        需要手動將這些指針指向的對象進行釋放
        範例請見，release_object_in_pointorsVector()

    #7  vector 和 array 的比較
        - 和 arrary 儲存連續空間
        - vector 能自動儲存元素，且自動增長或縮小儲存空間
        - vector 會消耗更多內存
        - vector 提供更高級的操作

 Ref:
    C++ vector用法（詳解！！函數，實現）
    https://blog.csdn.net/msdnwolaile/article/details/52708144
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include "main.h"

using namespace std;

// 印出 vector 結果的函數模板
template <typename T>
void output_vector_result(T vec, const string &name = "")
{
    if (!name.empty()){
        cout << name << ": ";
    }

    for(int i=0; i<vec.size() ;i++)
    {
        cout << vec[i] << " ";
    }

    cout << endl;
}

int main(int argc, const char * argv[])
{
    // 將一般的array 轉成 vector (呼叫vector的構造函數)
    array_to_vector();

    // 新增元素
    add_element_example();

    // 使用 for-loop 遍歷 元素
    travel_by_forloop();

    // iterator 遍歷 vector
    travel_by_iterator();

    // 插入元素，需要 iterator
    insert_element();

    // 刪除元素，需要 iterator
    del_element();

    // 清空容器，需要 iterator
    empty_element();

    // 排列array中的元素
    sort_element();

    // 反轉元素
    reverse_element();

    // 尋找元素
    find_element();

    // 釋放 vector 佔用的記憶體空間
    release_vector();

    // 釋放 指針vector指向的記憶體空間
    release_object_in_pointorsVector();

    // 記憶體操作相關函數
    memory_relate_example();

    // 複製元素
    assign_example();
}

void array_to_vector(){
    cout << "===== 將一般的array 轉成 vector (呼叫vector的構造函數) =====" << endl;

    // 建立一般的array
    int arr[] = {3,1,2,5,4};

    // 調用vector的構造函數將array轉換成vector的物件
    vector<int> vec(arr,arr+5);

    for (int i : vec) {
        cout << i << endl;     // 印出 3 1 2 5 4
    }
}

void add_element_example(){
    cout << "===== 新增元素 =====" << endl;

    vector<int> vec ;

    // 方法一，使用賦值表達式
    // vec = { 1,2,3,4,5,6,7,8,9};  或使用 = 直接賦值

    // 方法二，透過 for-loop
    for(int i=0; i<10; i++)
    {
        vec.push_back(i);
        cout << "capacity: " << vec.capacity() << endl;

    }

    // 注意，capacity 適當空間不夠的時候才會增加記憶體空間
    cout << "size: " << vec.size() << endl;
    cout << "capacity: " << vec.capacity() << endl;
}

void travel_by_forloop(){
    cout << "===== 使用 for-loop 遍歷 vector =====" << endl;

    vector<int> vec ;
    vec = { 0,1,2,3,4,5,6,7,8,9 };

    for (int i : vec) {
        cout << i << " ";
    }

    cout << endl;


    // 遍歷 字串vector
    vector<string> strArr = { "Sunday", "Monday" };
    output_vector_result(strArr);
}

void travel_by_iterator(){
    cout << "===== 使用 iterator 遍歷 vector =====" << endl;

    vector<int> vec ;
    vec = { 0,1,2,3,4,5,6,7,8,9 };

    // 建立 iterator object
    vector<int>::iterator it;

    // 使用 iterator 遍歷 vector
    // 注意
    // .begin() 返回第一個元素的iterator(智慧指針)
    // .end()   返回最後一個元素的iterator(智慧指針)
    for( it=vec.begin(); it!=vec.end(); it++){
        cout << "adddress of it: " << &it << endl;
        cout << "value of it: " << *it << endl;
    }
}

void insert_element(){
    cout << "===== 插入元素至指定位置_需要 iterator =====" << endl;

    vector<int> vec ;
    vec = { 0,1,2,3,4,5,6,7,8,9 };

    //  .insert(插入位置, 值, 插入次數)
    vec.insert(vec.begin()+4,555);  // 在 index = 4 的位置插入 555
    output_vector_result(vec);      // 印出 0 1 2 3 555 4 5 6 7 8 9
}

void del_element(){
    cout << "===== 刪除元素_需要 iterator =====" << endl;

    vector<int> vec ;
    vec = { 0,1,2,3,555,4,5,6,7,8,9 };

    // .erase(刪除位置)
    // .begin()返回開始位置的指針
    vec.erase(vec.begin()+4);       // 把 index = 4 的元素刪除 (刪除 555)

    output_vector_result(vec);      // 印出 0 1 2 3 4 5 6 7 8 9
}

void empty_element(){
    cout << "===== 清空容器 =====" << endl;

    vector<int> vec ;
    vec = { 1,2,3,4,5,6,7,8,9 };

    vec.clear();       // 清空 vector內所有的元素

    // .empty() 判斷 vector 是否為空
    if (vec.empty()){
        cout << "null" << endl;
    }else{
        output_vector_result(vec);
    }

    // 注意，.clear()清空所有元素，但記憶體空間仍然保持不變
    cout << vec.capacity() << endl;
}

void sort_element(){
    cout << "==== 排列array中的元素 ====" << endl;

    vector<int> vec;
    vec = {3,1,2,5,4};

    // 排列vector中所有的元素，inline change
    // sort(開始位置, 結束位置)
    // 只重新排列前三個元素，排列第0,1,2個元素，第3個元素停止
    sort(vec.begin(),vec.begin()+3);
    output_vector_result(vec);      // 印出 1 2 3 5 4

    // 全部排序
    sort(vec.begin(),vec.end());
    output_vector_result(vec);      // 印出 1 2 3 4 5
}

void reverse_element(){
    cout << "==== 反轉元素 ====" << endl;

    vector<int> vec;
    vec = {1,2,3,4,5};

    // 反轉vector中的元素，inline change
    // reverse(開始位置, 結束位置)
    reverse(vec.begin(),vec.end());
    output_vector_result(vec);      // 印出 1 2 3 4 5
}

void find_element(){
    cout << "==== 尋找元素 ====" << endl;

    vector<int> vec;
    vec = {1,2,0,4,5};

    // 尋找vector中的元素，inline change
    // find(開始位置, 結束位置, 要尋找的值)，若找不到就會回傳vec.end()
    vector<int>::iterator result;

    result = find(vec.begin(),vec.end(),0);

    if(result == vec.end()){
        cout << "Item not found" << endl;
    }else{
        cout << "Item found" << endl;
    }
}

void release_vector(){
    vector<int> vec, tmp;

    vec = { 1,2,3,4,5};
    cout << "交換前的記憶體空間大小： " << vec.capacity() << endl;

    vec.swap(tmp);
    cout << "交換後的記憶體空間大小： " << vec.capacity() << endl;
}

void release_object_in_pointorsVector(){

    cout << "==== 釋放(指針vector)指向的記憶體空間 ====" << endl;

    // 建立指針
    auto * piA = new int;
    auto * piB = new int;

    // 建立指針指向的對象
    *piA = 10;
    *piB = 20;

    cout << "釋放前： " << piA << endl;

    // 建立 指針vector
    vector<int *> pvec, tmp;

    // 將指針放進vector中
    pvec[0] = piA;
    pvec[1] = piB;

    vector<int *>::iterator it;

    // 釋放 pvec Vector中，每個指標指向的對象
    // it 相當於是指標的指標
    for(it=pvec.begin(); it!=pvec.end(); it++){
        if(nullptr != *it){
            delete *it;
            *it = nullptr;
        }
    }

    // 最後再將 vector 釋放
    pvec.swap(tmp);

    // 檢查是否釋放成功
    // 連記憶體空間都沒有了，所以印不出來
    cout << "釋放後： " << piA << endl;
}

void memory_relate_example(){

    cout << "==== 記憶體操作相關操作 ====" << endl;

    vector<int> vec {1};

    // .capacity() 顯示記憶體空間大小
    cout << vec.capacity() << endl;     // 印出 1

    // .reserve() 手動重新配置記憶體空間
    // .reserve() 可以最小化記憶體空間重分配的次數，減少分配所造成的開銷
    // 注意，使用 .reserve() 後，vector 就不會再自動將記憶體空間衝新配置
    vec.reserve(2);
    cout << vec.capacity() << endl;     // 印出 2

    // .max_size() 顯示目前還可以放入多少個元素，容器擴展到這個極限值就不會再自動增長
    cout << vec.max_size() << endl;

}

void assign_example(){
    cout << "==== 複製元素 ====" << endl;

    vector<int> v1, v2, v3;
    v1 = {1,2,3,4,5,6};

    output_vector_result(v1, "v1");   // 印出 v1: 1 2 3 4 5 6
    output_vector_result(v2, "v2");   // 印出 v2:

    // 使用 .assign() 進行複製元素
    v2.assign(10,0);     // 複製10個 0
    output_vector_result(v2, "v2");   // 印出 v2: 0 0 0 0 0 0 0 0 0 0

    v3.assign(v1.begin(),v1.end());
    output_vector_result(v3, "v3");   // 印出 v3: 1 2 3 4 5 6
}
