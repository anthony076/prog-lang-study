
#include <iostream>
#include <thread>
#include <chrono>

using namespace std;
using namespace std::this_thread;
using namespace std::chrono;

void while_example(){
    cout << "==== while-loop exmple ====" << endl;


    // 語法，while(條件) { 程式碼 }
    short i = 0;

    while (i <= 10){
        cout << i << endl;  // 印出，0 1 2 3 4 5 6 7 8 9 10
        i++;
    }

}


void for_exampleA(){
    cout << "==== for-loop，一般使用方式 ====" << endl;

    // 語法，for(初始化; 停止條件; step條件) { 程式碼 }
    for (int i = 11; i<=20; i++){
        cout << i << endl;  // 印出， 11 12 13 14 15 16 17 18 19 20
    }

}


void for_exampleB(){
    cout << "==== for-loop，使用 array ====" << endl;

    // 建立 int array
    short my_array[5] = { 1,2,3,4,5 };

    // 語法，for( 變數類型 變數名 : 可迭代的物件)
    // 將 int array 中的元素，分別帶到變數i 中
    // 使用 auto，自動獲取 array 中的類型
    for (auto i : my_array){
        i *= 5;
        cout << i << endl;  // 印出，5 10 15 20 25
    }
}


void do_while_example(){
    cout << "==== do-while exmple ====" << endl;

    short i = 21;

    // 語法，do { 程式碼 } while(條件);
    do {
        cout << i << endl;
        i++;
    } while (i <= 30);

}


void infinity_loop_for(){
    cout << "==== 用 for-loop 實現無窮迴圈 ====" << endl;

    for (; ;){
        cout << "This loop will run forever \n" << endl;
        sleep_for(milliseconds(1000));
    }
}


void break_example(){
    cout << "==== break 使用範例 ====" << endl;

    short i = 0;

    while(true){
        if (i == 10){
            break;
        } else{
            cout << i << endl;
            i++;
        }
    }

}


void continue_example(){
    cout << "==== continue 使用範例 ====" << endl;

    for(auto i =0; i<10; i++){
        if (i <= 5 ){
            continue;
        } else {
            cout << i << endl;
        }
    }

}


void goto_example(){
    cout << "==== goto 使用範例 ====" << endl;

    // 局部变量声明
    int a = 10;

    // goto 也適用於深度迴圈嵌套中，
    // 對於深度迴圈嵌套，break 只能退出一層循環，goto 可一次退出到最外層的循環
    LOOP:do {

    if( a == 15) {
        // 跳过迭代
        a = a + 1;
        goto LOOP;
    }

    cout << "a 的值：" << a << endl;
    a = a + 1;

    } while( a < 20 );

}

int main() {
    // while-loop 範例
    while_example();

    // for-loop 範例
    for_exampleA();
    for_exampleB();

    // do-while 範例
    do_while_example();

    // 用 for-loop 實現無窮迴圈範例
    //infinity_loop_for();

    // break example
    break_example();

    // contunie example
    continue_example();

    // goto example
    goto_example();

    return 0;
}