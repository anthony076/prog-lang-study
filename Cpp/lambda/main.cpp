#include <iostream>

using namespace std;

void declare_use_lambda_noparas(){
    cout << "==== 定義無參數的 lamda 表達式並調用 ====" << endl;

    // 定義 Lambda 表達式 (定義匿名函數)
    auto basicLambda = [] { cout << "Hello, world!" << endl; };

    // 調用 匿名函數
    basicLambda();
}

void declare_use_lambda_withparas(){
    cout << "==== 定義有參數的 lamda 表達式並調用 ====" << endl;

    // 建立有參數 lambda 表達式，有指名返回類型
    auto add = [](int a, int b) -> int { return a + b; };

    // 建立有參數 lambda 表達式，無指名返回類型，使用自動推斷返回類型
    auto multiply = [](int a, int b) { return a * b; };

    int sum = add(2, 5);   // 印出 7
    int product = multiply(2, 5);  // 印出 10

    cout << "sum: " << sum << endl;
    cout << "product: " << product << endl;
}

void use_capture(){
    cout << "==== 使用參數捕獲列表 ====" << endl;

    int i = 1024;

    // [=] 表明将外部的所有变量拷贝一份到该Lambda函数内部
    auto func = [=]{
        cout << i << endl;  // 印出 1024
    };

    // 調用 Lambda 表達式
    func();
}

void reference_capture(){
    cout << "==== 引用捕獲 ====" << endl;

    int i = 1024;
    cout << &i << endl;     // 印出 0x61ff1c

    auto fun1 = [&]{
        cout << &i << endl; // 印出 0x61ff1c
    };

    // 調用 Lambda 表達式
    fun1();
}

void use_mutable(){
    cout << "==== 使用 mutable 修飾符 ====" << endl;

    // 建立常量
    int x = 10;

    // 錯誤使用方式，會出現 error: assignment of read-only variable 'x' 錯誤
    //auto add_x = [x](int a) { x *= 2; return a + x; };

    // 正確的使用方式
    auto add_x = [x](int a) mutable { x *= 2; return a + x; };

    // 調用 lambda 表達式
    cout << add_x(10) << endl; // 印出 30
}

int main() {
    // 定義無參數的 lamda 表達式並調用
    declare_use_lambda_noparas();

    // 定義有參數的 lamda 表達式並調用
    declare_use_lambda_withparas();

    // 使用參數捕獲列表
    use_capture();

    // 引用捕獲
    reference_capture();

    // 使用 mutable 修飾符
    use_mutable();

    return 0;
}