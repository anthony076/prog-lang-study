//
// Created by ChingHua Yang on 2018/5/15.
//

#ifndef CATCH2_BASIC_FUNCTIONS_H
#define CATCH2_BASIC_FUNCTIONS_H
    int add(int a, int b);
    int minus(int a, int b);
    double float_test();
#endif //CATCH2_BASIC_FUNCTIONS_H
