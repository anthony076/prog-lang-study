/*
 建立 DLL

1   #define DLL __declspec(dllexport)
    透過自定義的巨集 DLL，將所有 DLL 替換為 __declspec(dllexport)
    其中，
        __declspec(dllexport) 聲明該函數被 export 到 dll 中
2  extern "C" { }，避免函數名被修改
    由於 C++ 具備函數重載的能力，但 C 沒有，因此製作DLL時對於重載的函數會進行更名
    若要給非 C++ 的語言使用 DLL，透過 extern C，讓export 的函數依照C語言的方式連結
    就不會有發生函數名被修改，但該函數也不具有重載的功能了

3   __declspec(dllexport) 的簡潔修飾語 DLL，放類型前面，或放類型後面都可以
    __declspec(dllexport) 可以加在宣告的時候，實作的時候可有可無，但加了比較好確認那些有 export

4   #ifdef  MYDLL_EXPORTS
    建立特有的標示符，來區分匯出或匯入，發布 DLL 時候用，當該DLL被其他程序使用時會變成匯入
*/

#define DLL __declspec(dllexport)

extern "C"{
    DLL int add(int x, int y);
}

