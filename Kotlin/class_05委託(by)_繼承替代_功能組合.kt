/*

# 什麼是委託
    - 將收到的請求，轉發給其他的類去執行
      當處理請求時，涉及到兩個對象：1_接收者對象 和 2_委託其操作給委託人對象
      而在委託中，接收者將自己傳遞給委託人，以讓被委託的操作指向接受者

# 為什麼要用委託
    - 委託比繼承靈活，1_可以動態配置，不會造成子類的成長，2_可以透過合成來實現多種功能
    - 當兩個類之間沒有明顯的繼承關係的類，但方法在另一個類中實現
    - 為了在一個類中使用另外一個類的方法時，透過委託執行而不需要繼承整個類
    - 委託是黑盒重用(看不到父類方法)，繼承是白盒重用，應該多使用委託少用繼承
    - 繼承打破封裝，子類的實現和父類的實現關係密切，父類實現的任何改變會引起子類的變化
    - 繼承是靜態編譯的，委託可以在運行時動態合成

# 什麼時候用委託
    - 能委託的就不要用繼承，兩個類明顯有繼承關係的時候用繼承
    - 僅僅只是為了在一個類中使用另外一個類的方法時，應該用委託

# 委託的使用方法
  被委託的類，需要當作參數帶入引數中
  語法，class 類名(已經實現接口的類a)：要實現的接口 by 已經實現接口的類名，其中
    - 已經實現接口的類a，要透過 val 引數名：類型名(實現的接口名) 來宣告
    - 已經實現接口的類又稱為，被委託類，實現接口的類，被委託的對象

*/

// ===== 範例一 =====
// 建立 ISports接口
interface ISports {
    fun doSports()
}

// 建立被委託類 SwimForSports，並實現 ISports接口的內容 (被委託類，實現接口的類，被委託的對象)
class SwimForSports: ISports{
    override fun doSports() {
        println("do swim")
    }
}

// 在SportsManager聲明中，帶入被委託類，編譯器會生成繼承自ISports接口的所有方法，並將調用轉發給被委託類。
class SportsManager(sport: ISports): ISports by sport

// ===== 範例二 =====

// 定義 Driver 抽象接口
interface Driver{
    fun drive()
}
// 定義 Writer 抽象接口
interface Writer{
    fun writer()
}

// 被委託類CarDriver，實現 Driver抽象接口的方法
class CarDriver:Driver{
    override fun drive() {
        println("正在駕駛")
    }
}

// 被委託類PPTWriter，實現 Writer抽象接口的方法
class PPTWriter:Writer{
    override fun writer() {
        println("正在寫作")
    }
}

// 委託範例
// #1 引數帶入了 1_實現Ｄriver接口方法的類 2_實現Writer接口方法的類，
//    透過 by，在 Manager類中，所有Ｄriver接口的實現會交給帶入的委託類 driver，
//    而在 Manager的類中，會手動實現Ｗriter接口中的writer()
// #2 在此例中，實現接口方法的類放在引數中，透過 by 告知要將接口方法委託給誰
class Manager(val driver: Driver, val writer: Writer): Driver by driver, Writer {
    override fun writer(){
        writer.writer()
    }
}



fun main(args: Array<String>) {
    // 範例一
    exampleA()

    // 範例二
    exampleB()
}

fun exampleA(){
    println("===== 範例一 =====")

    val swimSports: SwimForSports = SwimForSports()
    SportsManager(swimSports).doSports()// Log：do swim
}

fun exampleB(){
    println("===== 範例二 =====")

    val driver = CarDriver()    // 實現Driver接口的CarDriver類
    val writer = PPTWriter()    // 實現Writer接口的PPTWriter類
    val manager = Manager(driver,writer)

    manager.drive()
    manager.writer()


}