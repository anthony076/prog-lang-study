# Package

version       = "0.1.0"
author        = "chinghua0731"
description   = "iup tutorial"
license       = "MIT"
srcDir        = "src"
bin           = @["nim_iup"]

# Dependencies

requires "nim >= 0.18.0"
requires "iup >= 3.0.0"
