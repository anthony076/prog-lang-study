;===================
; 測試環境 Chez Scheme Version 9.5.6
; doc @ https://cisco.github.io/ChezScheme/csug9.5/csug9_5.pdf
;===================

; 基本概念，利用 object 類 (初始類)，讓父類繼承自 object
;     object 類使得父類也具有 super 的方法，父類的 super 指向 object 的實例
;     object 是為了讓父類有東西可以繼承，讓父類也具有 super 方法，沒有其他作用
;     因此，object 不需要實現任何屬性或方法
;     object 讓上層類別的寫法格式與下層類別的一致

(define call
  (lambda (obj method-name . args)  
    (apply (obj method-name) args)
  )
)

; 初始類，讓父類繼承，使父類具有 super 實例
(define Object 
  (lambda () 
    (lambda (method-name) 
      (error "method ~s not found:"  method-name)))) 

; 改造後的父類
(define Counter 
  (lambda (count)
    ; 取得父類實例，super = 父類實例的引用
    (define super (Object)) 

    ; method-1，定義 Counter 的內部函數
    (define add1 
      (lambda () (set! count (+ count 1))))

    ; method-2，定義 Counter 的內部函數
    (define get-count 
      (lambda () 
        (display (format "[Counter] count = ~a \n" count))
        count
      )
    )

    ; method-3，定義 Counter 的內部函數
    (define set-count
      (lambda (n)
        (set! count n)))

    ; method-4，mapping-function，將函數名轉換為實際函數
    ; lambda 會返回最後一個語句，因此，(Counter 0) 會返回此函數
    (lambda (method-name) 
      (cond 
        ((eq? method-name 'add1) add1)            ; 返回 add1 函數
        ((eq? method-name 'get-count) get-count)  ; 返回 get-count 函數
        ((eq? method-name 'set-count) set-count)  ; 返回 set-count 函數
        (else (error "Method ~s not found:" method-name)) 
      )
    )
  )
)

; 定義子類
(define Counter2 
  (lambda () 
    ; 取得父類實例，super = 父類實例的引用
    (define super (Counter 0)) 

    ; 定義子類才有的方法
    (define add2 
      (lambda () 
        (display "[Counter2] add2")
        (newline)

        ; 透過 super 調用父類的方法
        (call super 'add1)
        (call super 'add1)
      )
    ) 
    
    ; function mapping，將函數名轉換為實際函數
    (lambda (method-name) 
      (cond 
        ; 若被調用的方法是子類的 add2，就直接調用子類的方法
        ((eq? method-name 'add2) add2)
        ; 若被調用的方法不是子類的 add2，就直接調用父類的方法
        (else (super method-name))
      ))
    )
)

(define c0 (Counter 0))
(call c0 'add1)
(call c0 'get-count)

(define c2 (Counter2))
(call c2 'add2)
(call c2 'get-count)

;===================
(exit)