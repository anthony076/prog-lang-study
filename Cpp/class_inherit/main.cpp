#include <iostream>
#include "main.h"

using namespace std;

// ==== 單一繼承範例 ====
class father {

public:
    void setWidth(double w){
        width = w;
    }

    void setHeight(double h){
        height = h;
    }


protected:
    double width = 0;
    double height = 0;
};


class son : public father {
private:

public:
    double getArea(){
        return width * height;
    };
};

// ==== 多繼承範例 ====
class baseA {
    public:
    void setWidth(double w){
        width = w;
    }

    protected:
    double width = 0;
};


class baseB {
    public:
        void setHeight(double h){
            heigh = h;
        }

    protected:
        double heigh = 0;
};

class inherit : public baseA, public baseB{
    public:
    double getArea(){
        return width * heigh;
    }
};



int main(int argc, const char * argv[])
{
    // 繼承的簡單範例
    inherit_simple_example();

    // 多繼承範例
    multi_inherit();
}

void inherit_simple_example(){
    cout << "==== 單一繼承範例 ====" << endl;

    son s1;
    s1.setHeight(3.5);
    s1.setWidth(4.8);

    cout << s1.getArea() << endl;   // 印出 16.8
}

void multi_inherit(){
    cout << "==== 多繼承範例 ====" << endl;

    inherit s2;
    s2.setHeight(3.5);
    s2.setWidth(4.8);

    cout << s2.getArea() << endl;   // 印出 16.8
}

