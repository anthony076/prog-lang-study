/*
建立與繼承父類
#1. 在父類中，加上 open修飾符代表 class 可被繼承，
    沒有open修飾符，代表 class 不可被繼承，與 final修飾符的效果同
#2. 在子類中
    - 從父類繼承的引數，不再使用 val 或 var 宣告，已經在父類中宣告過
      但需要手動填到父類中，才能這些值帶到父類中
    - 子類中新增的屬性，才需要另外使用 val|var 宣告
    - 在子類中透過返回父類（ : 父類名稱() ） 來呼叫父類中的主要建構函式

複寫父類中的函式
#1. 在父類中使用 open修飾符，使該函式可被子類覆寫
    例如，open fun 名稱(引數) { 程式碼 }
#2. 在子類中使用 override修飾符 來覆寫父類中的函式
    例如，override fun 名稱(引數) { 程式碼 }
#3. 在子類中，使用 super.函式名稱() 來調用父類的函式
#4. 在父類中已經宣告過的函式，可以直接使用，不需要在子類中重新宣告

多型 (多種類型)
#1. 多型：以父類名當作類型宣告子類
#2. 多型宣告方式
    var 變數名稱：父類名稱 = 子類名稱(...)
#3. 使用多型可大幅減少代碼
    範例請參考，http://www.codedata.com.tw/kotlin/kt13/

注意事項
#1  繼承過程的順序
    在子類實例化的過程中，第一步先完成父類的初始化，再來才是子類主構造函式的初始化
*/

// ===== 建立與繼承父類 =====
// 建立父類
// #1. open修表 class 可被繼承，沒有open修飾符，代表 class 不可被繼承，與 final修飾符的效果同
// #2.
open class baseItem(val id:Long, var title:String, var content:String)


// 用主構造函式建立子類 (繼承父類)
// #1 子類的宣告方式
//    語法， class 類名稱(參數名稱：型態, val|var參數名稱：型態) : 父類名稱()
//    其中，
//    - 從父類繼承的引數，不再使用 val 或 var 宣告，已經在父類中宣告過
//      但需要手動填到父類中，才能這些值帶到父類中
//    - 子類中新增的屬性（在本例中為ImageFile)，才需要另外使用 val|var 宣告
//    - 透過 : 父類名稱() 來呼叫父類中的主要建構函式
class ImageItem(id:Long,
                title: String,
                content: String,
                var imageFile:String):baseItem(id,title,content)

// 用次構造函式建立子類 (繼承父類)
// 次構造函式 1_必須使用 super 關鍵字初始化父類的類型
//          2_委託給另一個次構造函數來初始化父類的類型
// (...補範例...)


// ===== 複寫父類中的函式 =====
// #1. 在父類中使用 open修飾符，使該函式可被子類覆寫
//     例如，open fun 名稱(引數) { 程式碼 }
// #2. 在子類中使用 override修飾符來覆寫父類中的函式
//     例如，override fun 名稱(引數) { 程式碼 }
// #3. 在子類中，使用 super.函式名稱() 來調用父類的函式
// #4. 在父類中已經宣告過的函式，可以直接使用，不需要在子類中重新宣告
// 建立父類
open class override_father (val id:Long, var title:String, var content:String){

    // 建立不能被覆寫的函式
    // #1. 因為不需要讓子類覆寫，所以不加入open修飾符
    // #2. 在子類中亦可直接調用此函式， 在父類中已經宣告好的函式，不用在子類中重新宣告
    fun getReduceContent(length:Int=5) = "${content.substring(0 until length)}..."

    // 建立可以被覆寫的函式
    // 因為要讓子類覆寫，所以加入open修飾符
    open fun getDetails() = "id=$id, title=$title, content=$content"

}

// 建立子類
class override_son(id:Long,
                title: String,
                content: String,
                var imageFile:String):override_father(id,title,content){

    // 改寫父類中的既有的函式，方法一，此方式會造成重複代碼，不推薦
    //override fun getDetails() = "id=$id, title=$title, content=$content, imageFile=$imageFile"

    // 改寫父類中的既有的函式，方法二，透過 super 呼叫父類中的函式，可避免重複代碼
    override fun getDetails() = "${super.getDetails()}, imageFile=$imageFile"
}

fun main(args: Array<String>) {
    // ===== 建立與繼承父類 =====
    val image = ImageItem(55688,"test","hello","1.png")
    println(image.imageFile)

    // ===== 複寫父類中的函式 =====
    val father = override_father(123, "aa", "bb")
    // 改寫前
    println(father.getDetails())

    val son = override_son(55688,"test","hello","1.png")
    // 改寫後
    println(son.getDetails())

    //在子類中可直接調用 在父類中已經宣告好的函式，不用在子類中重新宣告
    println(son.getReduceContent())

    // 多型宣告範例，以下兩種宣告方式是相同的
    // 以父類名當作類型建立子類物件，注意，引數須與父類同，否則會報錯
    val imgA : baseItem = ImageItem(123,"test","hello","1.png")
    // 以子類名當作類型建立子類物件
    val imgB : ImageItem = ImageItem(456,"test","hello","1.png")

}