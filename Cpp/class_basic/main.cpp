
#include <iostream>
#include "main.h"

using namespace std;

// ==== 宣告 class 基礎範例 ====
class Box{
    // 若不加訪問權限修飾符，預設是 privated
    double size = 100;

    public:
        // 宣告公開屬性成員
        double length = 10;
        double height;

        // 建立公開函數成員
        // 注意，類的函數可以在類的外部來實作
        double getlength();
};

// 實作類的函數
// #1 透過 範圍選擇符 :: 來選擇要實作的類，相當於作用域的延伸
// #2 實作時，可直接存取類的成員
double Box::getlength(){
    return length;
}

// ==== 構造函數的範例 ====
class Line{
    public:
        double length = 10;

        // 建立 Line class 的構造函數
        // #1 構造函數需與類同名
        // #2 必須寫在public 區塊中
        // #3 構造函數是特殊函數，不會有返回值，不需要定義返回值類型
        Line(void){
            cout << "Object is being created" << endl;
        }
};

// ==== 建立帶參數的構造函數範例 ====
class Circle {
    private:
        double _r = 0;
        double _angle = 0;

    public:

        // 宣告 Circle class 的構造函數，實作放在外面
        Circle(double r, double angle);

        // 宣告並實作get_r()，取得 私有變數 _r 的值
        void get_r(void){
            cout << "r = " << _r << endl;;
        }

        // 宣告並實作get_angle()，取得 私有變數 _angle 的值
        void get_angle(void) {
            cout << "angle = " << _angle << endl;
        }

    };

/*
不使用初始化列表的寫法
Circle::Circle(double r, double angle){
    _r = r;         // 將引數 r 傳遞給私有變數_r
    _angle = angle; // 將引數 angle 傳遞給私有變數_angle

    cout << "Object is being created, r= " << r;
    cout << " ,angle= " << angle << endl;
}
*/

// 使用初始化列表的簡化構造函數的實作
Circle::Circle(double r, double angle):_r(r), _angle(angle){
    cout << "Object is being created, r= " << r;
    cout << " ,angle= " << angle << endl;
}


// ==== 解構函數範例 ====
class A {
    private:

    public:
        A(){
            cout << "Object is being created " << endl;
        };

        ~A(){
            cout << "Object is being deleted " << endl;
        };
};

// ==== 類指標的使用 ====
class B {
    public:
        // 定義成員屬性
        double length = 0;
        double height = 0;

        // 定義構造函數
        B(double l, double h){
            length = l;
            height = h;
        };

        // 定義成員函數
        double square(double n){
            return n * n;
        }
};

// ======================
int main() {

    // 宣告 class 基礎範例
    //declare_class_basic();

    // 建立不帶參數的構造函數範例
    //class_constractor_noPara();

    // 建立帶參數的構造函數範例
    //class_constractor_withPara();

    // 解構函數範例
    //class_deconstractor();

    // 類指標的使用
    usage_of_classPointer();

    return 0;
}

void declare_class_basic(){
    cout << "==== 宣告 class 基礎範例 ====" << endl;

    // 宣告並實例化 class
    Box b1;

    b1.height = 1000;

    // 調用類的公開成員
    cout << b1.length << endl;
    cout << b1.height << endl;
    cout << b1.getlength() << endl;
}

void class_constractor_noPara(){
    cout << "==== 建立不帶參數的構造函數範例 ====" << endl;

    // 宣告並實例化 class
    Line l1;            // 印出 Object is being created
    // 調用 class 成員
    cout << l1.length << endl;  // 印出 10

}

void class_constractor_withPara(){
    cout << "==== 建立帶參數的構造函數範例 ====" << endl;

    // 宣告並實例化 class
    Circle c1(10,45);   // 印出 Object is being created, r= 10 ,angle= 45
    c1.get_r();         // r = 10
    c1.get_angle();     // angle = 45
}

void class_deconstractor(){
    cout << "==== 解構函數範例 ====" << endl;
    A a;
}

void usage_of_classPointer(){
    cout << "==== 類指標的使用 ====" << endl;

    // 宣告並實例化 class
    B b1(10,20);

    B* pb1 = &b1;

    cout << pb1->height << endl;    // 印出 20
    cout << pb1->length << endl;    // 印出 10
    cout << pb1->square(1.1) << endl;   // 印出 1.21
}