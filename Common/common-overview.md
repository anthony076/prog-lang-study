## 編程語言共通的基本概念

- 程式語言的分類
  - 分類1，指令式(Imperative-programming)，
  - 分類2，物件導向式(Object-Oriented)，
  - 分類3，函數式(Functional)，
  
  - 三種方式的差異，主要在於對`狀態`的處理方式不同
    - 指令式的狀態，是外部的
    - 物件導向式的狀態，是內部的
    - 函數式沒有使用到狀態，
      - 沒有內部變數來追蹤狀態的變化，所有的狀態都只透過輸入或輸出來傳遞
      - 透過`多個函數的組合`來完成特定的操作
      - 沒有for或while等迴圈語法，迴圈語法需要狀態來追蹤每次迴圈的狀態，例如，迭代次數
      - 透過`遞歸來取代迴圈語法`，透過遞歸不斷的循環執行
        - 將循換次數放在函數參數進行傳遞，
        - 必須要在閉包內設置停止條件

## 比較

| 項目 | python        | go        | c                             | rust                          | js                    |
| ---- | ------------- | --------- | ----------------------------- | ----------------------------- | --------------------- |
| 協程 | asyncio       | goroutine | 第三方庫(pthread)             | async/await + 第三方庫(tokio) | async/await + Promise |
| 通道 | asyncio.Queue | channel   | unistd (linux) 或 windows-api | std::sync::mpsc::channel()    | rxjs 的 EventEmitter  |


## 函數式編程基本概念

- 基本原則
  - 減少不必要的狀態
  - 使用 pipelines 簡化 (多使用 filter、map、sort、等函數簡化)
  - 對於清量的一般代碼，多使用 lambda 進行簡化

- 性質，函數式`沒有使用狀態`
  - 沒有內部變數來追蹤狀態的變化，所有的狀態都只透過輸入或輸出來傳遞
  - 透過`多個函數的組合`來完成特定的操作
  - 沒有for或while等迴圈語法，迴圈語法需要狀態來追蹤每次迴圈的狀態，例如，迭代次數

- 性質，透過`遞歸`，來取代迴圈語法，透過遞歸不斷的循環執行
  - 將循換次數放在函數參數進行傳遞，
  - 必須要在閉包內設置停止條件

- 性質，透過`閉包`，來記住某些狀態
  - 閉包函數，是用來建立內部狀態的特殊函數，該函數會返回一個`保存並調用內部狀態的新函數`，

  - 因為閉包函數可以`像實例對象一樣保存狀態`，因此`閉包函數的返回值得類型`，也被稱為`閉包函數實例`，
    必須先調用閉包函數，才能得到閉包函數實例的內部狀態

  - 範例，透過閉包保存狀態範例
    
    ```javascript
    // 建立閉包函數
    function outer() {
        // 閉包函數實例保存的內部狀態
        let outerVariable = "I am from outer!";

        // 調用內部狀態的新函數
        function inner() {
            console.log(outerVariable);
        }

        return inner; // 閉包函數實例，返回的是使用内部狀態的新函數
    }

    // 透過調用閉包函數實例，取得使用内部狀態的新函數
    const closureExample = outer();

    // 執行使用内部狀態的新函數，相當於執行 inner()
    closureExample(); 
    ```

  - 範例，在 javascript 中，透過 `bind()` 和 `this` 來簡化閉包和內部狀態的建立

    - 在閉包函數中，使用 `this` 專門來代表`閉包函數實例`，注意，使用 this 的函數就是閉包函數
    - 透過bind()，將閉包函數中的 this 的符號，與保存數據的對象進行綁定
    - 透過this和bind()，隱藏了 1_內部數據的建立，2_內部函數的建立
    
    ```javascript
    // 建立閉包函數，使用 this 的函數就是閉包函數
    // 此處隱藏了 1_內部數據的建立，2_內部函數的建立，此函數等效於閉包函數中的內部函數
    const originalFunction = function() {
      // this 指的是閉包函數返回的函數實例，函數實例和對象實例作用一樣，都可用來儲存狀態，
      // 差別僅在函數實例需要透過函數調用的方式，取得閉包函數實例中的新函數
      console.log(this.name);
    };

    // 保存數據的對象實例
    const obj = { name: "Example Object" };

    // 使用 bind()，將內部閉包函數的內部函數，originalFunction，將其中的 this 與傳入的obj進行綁定，並返回新函數
    // boundFunction() 是保存狀態的新函數
    const boundFunction = originalFunction.bind(obj);

    boundFunction();  // 調用新函數
    ```

  - 範例，透過 lambda 簡化 bind() 和 this

    - 使用箭頭的函數為 lambda 函數
    - lambda 函數沒有自己的 this，這裏的 this 將繼承自外部作用域
  
    ```javascript
    const originalFunction = function() {
      console.log(this.name);
    };

    const obj = { name: "Example Object" };

    // 使用箭头函数，它会捕获外部作用域的 this
    const arrowFunction = () => {
      originalFunction.call(obj);
    };

    arrowFunction(); // 输出 "Example Object"
    ```

- 基於lambda的常用函數 : filter、map、take、sort、slice、...，透過這些函數可以將操作數據的函數，以 Pipeline 的方式加以簡化，
  而不需要使用迴圈語法

## Ref

- [函數式編程中的遞歸](https://www.youtube.com/watch?v=nuML9SmdbJ4&t=284s)