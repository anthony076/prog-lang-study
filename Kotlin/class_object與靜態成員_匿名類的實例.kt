/*
# object 的作用
    - object 是一種特殊情形的類，同樣有屬性和方法，可以繼承類和實現接口
    - object 表達式(object {...}) 可以作為變量的定義，是立即實例化 (不像類，類需要手動實例化)，用於快速建立物件
      因為是立即實體化，所以不會有構造函數
    - object 是立刻實體化的匿名類，具有部分類的特性，所以可以繼承或實現多個接口
    - object 是單例(只會產生一個實體的類)的，可以定義在全局，也可以在類內部使用，主要提供存取該物件的統一方法
    - 在類中，使用 companion object 取代 java 中的 static 來建立靜態的成員
      注意，companion 只能放在類中，是專門給類用來建立靜態成員的

# 在類中 kotlin 無法用 static 關鍵字創建靜態成員，改用companion object 取代 static 關鍵字，主要用於來建立類的靜態成員
  - 什麼是靜態成員 (靜態變量 與 實例變量 的差別)
    https://dongchuan.gitbooks.io/java-interview-question/java/the_difference_between_static_variables_and_instance_variables.html
    - 在語法定義上的區別：
      靜態變量前要加靜態的關鍵字(static)，而實例變量前則不加
    - 在程序運行時的區別：
      實例變量必須創建對象後才可以通過這個對象來使用，靜態變量則可以直接使用類名來引用。
    - 相當於 python 中的 staticmethod

  - 為什麼需要靜態成員
    在程式語言中，透過構建函數(new)可以為同一個類創建多個實體對象，每個實體對象都有自己的實體變量，
    但透過靜態變量，即使創建再多的實體對象，靜態變量都是同一個

  - 為什麼 kotlin 沒有 static
        1. 因為Java沒有全局變量，也不存在包級函數，一切屬性和方法都是在類裡面，
           所以在寫一些工具函數和全局變量時，都需要用到static關鍵字修飾的靜態成員。
        2. Kotlin之所以能拋出靜態成員，主要原因在於它允許包級屬性和函數的存在，
           而且Kotlin為了維持與Java完全的兼容性， 透過 companion 來為靜態成員提供相容的方案

# 對象表達式 (是一個立刻實體化的匿名類，可把 object 賦值給變數，建立變數或屬性時用)，分為兩種，
    語法， var|val 變數名 ＝ object{ object 成員 }
    1_使用內建的匿名類創建 object， 適用於單純的對象
    2_透過某個父類創建 object

# object 既然是一個類，所以也可以用來當作函數的類型宣告使用
    作為 返回類型 使用
    - 公開函數的返回值如果是 object ，若沒有聲明其他父類，則實際返回的類型為 Any，objec中添加的成員就無法使用
    - 私有函數的返回值如果是 object , 其返回類型為 object

# 對象聲明 (建立靜態成員，與類一樣可以建立自己的屬性和方法)
    - 語法，object 對象名稱 { 靜態成員的內容 }
    - 對象聲明與對象表達式不同，
        對象表達式，是立即執行及初始化，可用於賦值
        對象聲明  ，是延遲初始化，不可用於賦值
    - 對象聲明和一般類一樣，所以可以繼承父類
    - 對象聲明不能直接嵌套在函數裡，但可以嵌套到其他對象聲明或非內部類中
    - 對象聲明的屬性必須提供訪問器使用
    注意，定義在類內部的object無法訪問類的成員

# 伴生對象 companion object (在類中使用的對象聲明，取代java中的 static 關鍵字，用於建立類的靜態成員)
    - 在類中建立對象聲明，必須使用 companion 關鍵字，代表類的靜態成員
      語法，companion object 名稱 { 對象內容 }
    - 該伴生對象(靜態成員)可以只使用類名來調用，而不需要將類實例化
    - 該伴生對象(靜態成員)在運行時，仍然是真實對象的實例成員，因此可以用來實現接口
    - 給伴生對象的屬性或方法加上@JvmStatic註解，可以在java文件中直接使用，不需要訪問companion
      (kotlin 會直接將 companion 轉換成 java 的 static)
    注意，companion object 在同一個類中，只能宣告一次

Ref
    區別 kotlin中的 object 和 companion object 的差別
    http://liuqingwen.me/blog/2017/06/20/object-vs-companion-object-in-kotlin/
*/

// ===== 創建一個object(建立匿名類)，且該匿名類有繼承父類和引數 =====
open class baseCls(x:Int){
    public open val y:Int = x
}

interface B {}

// ===== 對象聲明範例 (單例用) =====
object myObject{
    val x:Int = 100
}

// ===== 在類中利用companion建立靜態對象範例 =====
class companion_example{

    companion object {
        var x:Int = 200
    }
}

// ===== 在伴生對象中添加@JvmStatic註解範例 =====
class Latitude (val value: Double){ //采用私有构造器
    companion object { //伴生对象
        @JvmStatic //使用该注解后，可在java类中不访问Companion，直接使用伴生对象里面的方法
        fun ofDouble(double: Double): Latitude { //设置纬度
            return Latitude(double)
        }

        fun ofLatitude(latitude: Latitude): Latitude { //拷贝纬度
            return Latitude(latitude.value)
        }

        val TAG: String = "Latitude" //相当于静态属性
    }
}


fun main(args: Array<String>) {
    // 利用 對象表達式 創建對象
    simple_object()

    // 利用 對象表達式 創建 繼承指定父類的對象
    inherit_object()

    // 對象聲明範例 (單例用)
    println("===== 對象聲明範例 (單例用) =====")
    println(myObject.x)

    // 在類中利用companion建立靜態對象範例
    println("===== 在類中利用companion建立靜態對象範例 =====")
    println(companion_example.x)

    // 在伴生對象中添加@JvmStatic註解範例
    println("===== 在伴生對象中添加@JvmStatic註解範例 =====")
    val latitude = Latitude.ofDouble(3.0)
    val latitude2 = Latitude.ofLatitude(latitude)
    println(latitude)
    println(latitude2)
    println(Latitude.TAG)

}


fun simple_object(){
    println("===== create_object() =====")

    val a = object{
        // 建立匿名類的屬性
        var x: Int = 10
        var y: Int = 20
    }
    println(a.x + a.y)
}
fun inherit_object(){
    // 因為 object(匿名類) 繼承父類 baseCls，所以變數宣告時，變數類型需宣告為 baseCls 的類別
    // 前面的 baseCls 是宣告類型用，後面的 baseCls 是告訴編譯器，該匿名類繼承父類 baseCls
    // 變數 ab 的類型是 baseCls，返回一個object(返回一個匿名類)
    // object(匿名類)是繼承至 baseCls，並將 x:1 帶入 baseCls 的主構造函數中
    val ab: baseCls = object : baseCls(1){
        override val y = 15
    }
}
