/*
什麼是擴展
    - 能夠擴展一個類的新功能而無需 1_繼承該類 或 2_使用像裝飾者這樣的任何類型 的設計模式。
    - Kotlin支持 1_擴展函數 和 2_擴展屬性。
    - 擴展不是真正修改他們所擴展的類，而是在原有的基礎上，在該類中新增函數
      擴展不能為該類中插入新的成員，但可以使用該類既有的成員，和 調用該擴展函數時帶入的參數，
      帶入的參數不屬於被擴展類的成員

使用擴展
    擴展函數語法，
        fun 類名.擴展函數名()
    調用擴展函數語法，
        fun 函數名(引數名：被擴展的類名)

注意事項
    - 在擴展函數中定義類型為父類，但調用時傳入的是子類，且函數重名，這種情況下
      擴展函數的調用是由擴展函數定義的類型決定的，不是由當時運行的類型決定的
    - 若擴展函數與被擴展類的成員函數重名，且都適用給定的類型，此時依類的成員函數為優先
*/

// ===== 擴展自訂類的函數 =====
// 建立父類C
open class C{
    val a:String = "abccc"
}

// 建立子類Ｄ，並繼承父類Ｃ
class D: C()

//擴展(新增)父類C的函數，Ｃ.foo()
fun C.foo() = "c" + a

//擴展(新增)子類Ｄ的函數，D.foo()
fun D.foo() = "d"

fun printFoo(cls: C) {
    println(cls.foo())
}

// ===== 擴展 內建類的函數 =====
// 為內建類 MutableList<Int>類，擴展新的函數 swap()
fun MutableList<Int>.swap(index1:Int, index2:Int){
    println("===== 擴展內建類的函數 =====")
    // this 對應到 接收者對象 (即該列表)
    val tmp = this[index1]
    this[index1] = this[index2]
    this[index2] = tmp
}

// ===== 擴展函數與類的成員函數重名 =====
class origin_class{
    fun foo() { println("i am member") }
}

fun origin_class.foo() { println("i am extension") }

fun main(args: Array<String>) {
    // 擴展 自訂類的函數
    self_Extensions_example()

    // 擴展 內建類的函數
    val list = mutableListOf<Int>(1,2,3)
    list.swap(0,2)
    println(list)   // 返回 [3,2,1]

    // 擴展函數與類的成員函數重名
    println("===== 擴展函數與類的成員函數重名 =====")
    origin_class().foo()
}

fun self_Extensions_example(){
    println("===== self_Extensions_example =====")

    // 被調用的擴展函數，只取決於參數的聲明類型，
    // 雖然此處帶入的是子類Ｄ(繼承自父類Ｃ)，但在調用擴展函式中，引數cls聲明的類型是 Ｃ，
    // 因此輸出是 C.foo() = c
    // 注意，在擴展函數中定義類型為父類，但調用時傳入的是子類，且函數重名，這種情況下
    // 擴展函數的調用是由擴展函數定義的類型決定的，不是由當時運行的類型決定的
    printFoo(D())   // 輸出 c
    println(D().foo())
}

