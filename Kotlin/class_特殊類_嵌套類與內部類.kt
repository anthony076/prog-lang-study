/*

嵌套類，類可以嵌套在其他類中，被嵌套的類，預設為靜態類，不能存取外部類的成員，除非透過inner宣告為內部類

內部類，
    - 嵌套在其他類體中的類，透過 inner 宣告為內部類，讓內部類可以訪問外部類的成員
    - 透過inner宣告的內部類，為非靜態類

內部類的屬性存取
    - 內部類 和 外部類 若有重名的屬性，可透過 this@類名.屬性名 來存取

*/

// ===== 嵌套類範例 =====
class outer {
    private val bar: Int = 1
    class Nested {
        val value = 2

        // 存取內部類屬性的兩種方式
        //fun foo() = value
        fun foo() = this.value
    }
}

// ===== 內部類訪問外部類成員 =====
class out{
    private val bar: Int = 55688

    // 宣告為內部類
    inner class Inner {
        val value = 55981

        // 在內部類中存取外部類的變數 bar
        fun foo() : Int {
            println(bar)
            println(this@Inner.value)
            return this@out.bar
        }
    }
}

fun main(args: Array<String>) {
    // 嵌套類範例
    val demo = outer.Nested().foo()
    println("demo:$demo")

    // 內部類訪問外部類成員
    val demo2 = out().Inner().foo()
    println("demo2:$demo2")
}