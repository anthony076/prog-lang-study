/*
# 密封類 : 只保存數據的類，密封類是枚舉類的擴展
# 語法 : sealed class
  標記 sealed 的密封類，會自動產生以下的類成員，不需要手動建立，在調用密封類的時候可直接使用


# 密封類的使用要求
    - 密封類是自身抽象的，它不能直接實例化，可以有抽象（abstract）成員。
    - 需要被密封的子類只能 1_定義在密封類的內部(子類必須在父類內部聲明)
                       2_或同一个文件中，因為密封類的構造函數是私有的(private)
    - (不需要被密封的子類)密封類可以被數據類繼承，且該數據類可以放在任何地方，不一定與密封類所在的同一個文件下

# 與枚舉的比較
    - 集合的枚舉類型是嚴格要求的，但是每個枚舉常量僅存在一個單例，而密封類的子類可以包含狀態
    - 密封類適用於(子類)可數的情況，
    - 枚舉適用於(實例)可數的情況
*/

//演奏控制类（密封类）
sealed class PlayerCmd {
    val playerName: String = "Player"

    //演奏类
    class Player(val url: String, val position: Long = 0): PlayerCmd() {
        fun showUrl() {
            println("$url, $position")
        }
    }

    //快进類
    class Seek(val position: Long): PlayerCmd()

    //暫停類(單例)
    object Pause: PlayerCmd() //暂停（无需进行重载的类适合用单例object）
}

// 密封类的子类也可以定义在密封类的外部，但要在同一个文件中）
// 繼續類(單例)
object Resume: PlayerCmd()
// 停止類(單例)
object Stop: PlayerCmd()

enum class PlayerState { //枚举适合表现简单的状态
    IDLE, PAUSE, PLAYING, STOP
}


fun main(args: Array<String>) {
    PlayerCmd.Player("苍茫的天涯").showUrl()
    println(Resume.playerName)
}