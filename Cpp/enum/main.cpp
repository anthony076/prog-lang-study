#include <iostream>
using namespace std;

void example1(){
    cout << "====建立枚舉+枚舉變數範例====" << endl;

    // ==== 建立枚舉color 和 枚舉變數c ====
    // 宣告枚舉類，並添加枚舉變數 c
    // 注意，枚舉變數只能使用該枚舉中所列舉的值
    enum color
    {
        red,
        green=5,
        blue
    } c ;

    // ==== 枚舉變數 c 初始化 ====
    c = blue;

    // ==== 檢查枚舉變數c ====
    // 因為 green = 5，因此在其後的 blue 為 6
    cout << c << endl;  // 印出 : 6

}

void example2(){

    cout << "====建立枚舉範例====" << endl;

    // 枚舉變數名非必要
    enum Day { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday};
    cout << "Sunday : " << Sunday << endl;     // 印出 : 0
    cout << "Monday : " << Monday << endl;     // 印出 : 1
    cout << "Friday : " << Friday << endl;     // 印出 : 5

    // 枚舉名可當類型名稱使用
    // 語法，枚舉名 變數名 ＝ 值
    // 注意，宣告枚舉變數時，就只能使用該枚舉中所列舉的值
    Day today = Sunday;
    cout << today << endl;  // 印出 : 0

}

int main() {
    // 建立枚舉+枚舉變數範例
    example1();

    // 建立枚舉類範例
    example2();
}

