/*

# Lambda 表達式
    語法  { 傳入變數1, 傳入變數2, ... -> 程式碼}

    其中，
    { ... }  代表使用 lambda 表達式
    ->       Lambda表達式中的返回，代表 return 的意思

# 承接 匿名函數 或 Lambda表達式 結果 的變數名稱，
  就等同於 該匿名函數或Lambda函數 的函數名稱，
  呼叫該變數名就相當於執行匿名函數或Lambda函數
  範例，
      var sum = { x:Int, y:Int -> x + y}
      print(sum(5,5))
*/

fun main(args: Array<String>) {
    // 一般函數的使用方式
    println(add_general(5,10))

    // 匿名函數的使用方式
    // 匿名函數的變數名稱就等同於函數名稱
    add_anonymous()

    // 用單行 Lambda 表達式，有帶參數
    add_lambda_withParams()

    // 用單行 Lambda 表達式，無帶參數
    add_lambda_noParams()

    // 用多行 Lambda 表達式，有帶參數
    add_lambda_multilines()
}

fun add_general(x:Int, y:Int): Int{
    println("==== add_general() ====")

    return x + y
}

fun add_anonymous(){
    println("==== add_anonymous() ====")

    // 匿名函數的變數名稱就等同於函數名稱
    val sum = fun(x:Int, y:Int) : Int{
        return x + y
    }

    println(sum(5,10))
}

fun add_lambda_withParams(){
    println("==== add_lambda_withParams() ====")

    var sum = { x:Int, y:Int -> x + y}

    println(sum(5,10))
}

fun add_lambda_noParams(){
    println("==== add_lambda_noParams() ====")

    var sum = { 5 + 5}

    println(sum())
}

fun add_lambda_multilines(){
    println("==== add_lambda_multilines() ====")

    var sum = {
        x:Int, y:Int ->
        println("x:$x, y:$y")
        x + y   //最後一行是返回值
        }

    sum(10,10)
}

