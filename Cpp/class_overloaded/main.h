//
// Created by ChingHua Yang on 2018/4/23.
//

#ifndef CLASS_OVERLOADED_MAIN_H
#define CLASS_OVERLOADED_MAIN_H
    void simple_overloaded_example();
    void overloaded_operator_inClass();
    void overloaded_operator_outClass();
#endif //CLASS_OVERLOADED_MAIN_H
