//
// Created by ChingHua Yang on 2018/4/26.
//

#ifndef TEMPLATE_MAIN_H
#define TEMPLATE_MAIN_H
    void function_template_example();
    void class_template_example();
    void function_template_in_general_class();
    void function_template_in_class_template();
    void static_in_class_template();
    void template_specialization();
    int func(int i);
#endif //TEMPLATE_MAIN_H
