//
// Created by ChingHua Yang on 2018/4/27.
//

#ifndef ERROR_MAIN_H
#define ERROR_MAIN_H
double throw_example(int a, int b);
void try_catch_example();
void self_define_exception();
#endif //ERROR_MAIN_H
