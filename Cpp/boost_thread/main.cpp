#include <iostream>
#include <cstdio>
#include <boost/thread.hpp>

using namespace std;

void BoostThread(int v1, int v2){
    printf("(Boost)value 1 = %d, value 2 = %d\n", v1, v2);
}

int main(int argc, const char * argv[])
{

    boost::thread t;
    t = boost::thread(BoostThread, 100,200);
    t.join();
    return 0;
}
