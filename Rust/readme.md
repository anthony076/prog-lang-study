## 名詞介紹
- rustup: rust 的安裝程序，用來安裝/管理 rustc和標準庫
- rustc: Rust 編譯器，一般不會直接調用 rustc，而是透過 Cargo命令行執行 rustc 的命令
- cargo: project管理器 + 包管理器 + rustc 執行器

## 教學
- [Cargo命令的配置(config.toml)](https://sdk.nnsdao.com/docs/rust-guide/rust-cargo-config/)
- [專案配置(Cargo.toml)](cargo-toml-usage.md)

## 參考
- [Rust程式設計語言](https://rust-lang.tw/book-tw/)
- [Rust語言聖經](https://course.rs/about-book.html)
- [The Rust primer for beginners](https://shihyu.github.io/RustPrimerBook/)
- [Cargo 教程](https://learnku.com/docs/cargo-book/2018)

- [Rust Cargo](https://llever.com/cargo-book-zh/)
- [The Cargo Book](https://doc.rust-lang.org/cargo/getting-started/index.html)