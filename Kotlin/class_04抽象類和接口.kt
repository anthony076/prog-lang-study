
/*
# 抽象類 :
    - 與覆寫父類的方法不同，在父類中是有實現方法的，在抽象類中可以只定義方法名稱
    - 在抽象類中，可以只定義方法的名稱，方法的實作內容是由子類完成
      也可以在抽象類中實現方法的內容

# 接口 :
    - 接口不是類，是抽象方法的集合，在接口定義抽象方法，在類實現抽象方法的內容
    - 定義常量或定義抽象方法的函數，可以在接口或子類中實現方法
    - 接口可以有屬性但無法直接賦值(不能用 變數 ＝ 值)，必須 1_聲明為抽象屬性 或 2_提供getter()/setter()實現
    - 一個接口可以被多個類繼承, 一個類可以繼承多個接口

    接口與一般類的差異
    - 接口無法被實例化，沒有構造方法
    - 接口的函式可以是抽象的方法，類的函式是抽象方法的實現

    接口與抽象類的差異
    - 接口無法直接賦值，抽象類可以


# 接口和抽象類的使用時機
  多個子類需要共同的抽象方法時用接口，因為子類可以實現多個接口，但只能繼承一個父類

# 類可繼承一個父類，實現多個接口
    class 類名()：父類, 接口1, 接口2, 接口3 ... { 程式碼 }

# 多重繼承的函數覆寫原則
    - 多重繼承中具有相同的函數名的話，就強制一定要在子類中覆寫衝突的函數
    - 多重繼承者，在子類中，透過 super<繼承者名稱>.函數名()來指定要覆寫的對象
    https://www.kotlincn.net/docs/reference/classes.html

Ref:
http://www.runoob.com/java/java-interfaces.html

*/

// ===== 抽象類的範例 =====
// 建立抽象類
abstract class Person(open val age:Int){
    open fun work(){ }
}

// 建立子類1 Marker
class Marker (age: Int):Person(age){
    //覆寫父類中的變數
    override val age :Int
        get() = 0

    override fun work() {
        println("我是碼農，在寫代碼")
    }
}

// 建立子類2 Doctor
class Doctor (override val age: Int):Person(age){
    override fun work() {
        println("我是醫生，在看病")
    }
}

// ===== 接口的範例 =====
interface MyInterface{
    fun bar()
    fun foo(){
        println("I am foo in MyInterface")
    }
}

class Child : MyInterface{
    override fun bar() {
        println("Override bar in Child")
    }
}

// ===== 接口中的屬性範例 =====
interface interA{
    // 接口中的屬性必須是抽象的
    val aa : Int

    // 接口中的屬性必須提供 getter() 或 setter() 訪問
    val bb : String
        get() = "bb"

    // 存取 interface 中的屬性範例
    fun foo() {
        println(aa)
        println(bb)
    }
}

class A: interA{
    override val aa:Int = 10
}


// ===== 多重接口衝突的覆寫範例 =====
interface F { fun x(): Int = 2 }

interface G { fun x(): Int = 1 }

// 透過 super<interface名稱> 來指定 interface
// 透過 super<interface名稱>.函數名稱來指定要覆寫的函數
class I(var y: Int = 0): F, G{
    override fun x(): Int {
        if (y > 0) {
            println("call x(): Int int I")
            return y
        } else if (y < -200) {
            println("call x(): Int int F")
            return super<F>.x()
        } else {
            println("call x(): Int int G")
            return super<G>.x()
        }
    }
}

fun main(args: Array<String>) {
    // 抽象類的範例
    abstract_example()

    // 接口的範例
    interface_example()

    // 接口中的屬性範例
    properties_in_interface()

    // 多重接口衝突的覆寫範例
    duplicate_method_inInterface()
}

// 抽象類的範例
fun abstract_example(){
    println("===== abstract_example =====")

    val person:Person = Marker(23)
    person.work()
    println(person.age)

    val person2:Person = Doctor(29)
    person2.work()
    println(person2.age)
}

// 接口的範例
fun interface_example(){
    println("===== interface_example =====")

    val child = Child()
    child.bar()
    child.foo()
}

// 接口中的屬性範例
fun properties_in_interface(){
    println("===== properties_in_interface =====")

    val child = A()
    child.foo()
}


// 多重接口衝突的覆寫範例
fun duplicate_method_inInterface(){
    println("===== duplicate_method_inInterface =====")
    println(I(3).x())
}
