//
// Created by ChingHua Yang on 2018/5/2.
//

#ifndef THREAD_MAIN_H
#define THREAD_MAIN_H
    void thread_without_paras();
    void thread_with_paras();
    void shared_variable_in_differentThread();
#endif //THREAD_MAIN_H
