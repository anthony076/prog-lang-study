/*
# Range 的使用 ： http://www.liying-cn.net/kotlin/docs/reference/ranges.html
# Range 變數宣告
    var range: IntRange = 0..10
# 快速用法
    0..10
# 常用關鍵字
    step    指定間距
    downTo  指定反向遍歷
    until   不包含末尾元素
*/

fun main(args: Array<String>) {
    useRange()
}

fun useRange(){
    // 宣告 Range 變數
    var range: IntRange = 0..10

    // 透過 Range 變數進行 for...loop...
    println("===============")
    for (i in range){
        println(i)
    }

    // 不透過 Range 變數進行 for...loop...
    println("===============")
    for (i in 1..5){
        println(i)
    }

    // 使用 step 關鍵字指定間隔
    println("===============")
    for (i in 0..10 step 2){
        println(i)
    }

    // 反向遍歷，錯誤的用法
    // 注意，此寫法不會報錯，但也不會有任何的輸出
    println("===============")
    for (i in 4..1){
        println(i)
    }

    // 反向遍歷，正確的用法，使用 downTo 關鍵字
    // 注意，此寫法不會報錯，但也不會有任何的輸出
    println("===============")
    for (i in 4 downTo 1){
        println(i)
    }

    // until 的用法
    println("===============")
    for (i in 1 until 10){  // 1到10，但不包含10
        println(i)
    }

}