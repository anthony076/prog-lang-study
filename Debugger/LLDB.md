## lldb 的使用

## 安裝 

- 注意，lldb 是 LLVM 框架的一部分，在部分 distro 上可以直接安裝，在部分 distro 上需要透過LLVM套件安裝

- NIXOS : `$ nix-shell gcc lldb`

- Ubuntu : 
  - $ apt search lldb
  - $ apt install lldb-19

- Windows :
  - github : https://github.com/llvm/llvm-project/releases

  - 利用 winget 安裝
    - $ winget search llvm
    - $ winget install llvm

## 常用指令

- 以下列代碼為例

  ```c
  // hello.c

  #include <stdio.h>

  // 遞迴版本
  int fibonacci(int n) {
    if (n <= 1) {
      return n;
    }
    return fibonacci(n - 1) + fibonacci(n - 2);
  }

  int main() {
    int n = 10; // 計算費氏數列的第 n 個數字 (這裡設為 10)

    printf("Fibonacci(%d) = %d\n", n, fibonacci(n));

    return 0;
  }
  ```

- 注意，使用lldb命令前，需要將`源碼`編譯為可執行檔，並添加`-g`的編譯參數，該參數會在編譯時添加debugger相關的符號

  例如，
  - 執行編譯，並產生debugger相關符號，`$ gcc -g -o hello hello.c`
  - 查看產生的debugger符號，`$ readelf --debug-dump=info hello | less`

- 查詢指令用法
  - help，列出所有 lldb 命令
  - help b
  - help br clear

- [指令] 加載執行檔 (創建target)

  `$ lldb ./hello` 等效於

  ```shell
  $ lldb 
  
  # file 是 target create 的縮寫
  (lldb) target create ./hello 或 (lldb) file ./hello
  ```

  `$ lldb -- ./hello foo` 實際上會拆分為兩個 lldb 指令，
  - 加載執行檔
  - 傳遞執行檔參數

  因此，`$ lldb -- ./hello foo` 等效於

  ```shell
  # step，加載執行檔
  (lldb) target create hello 或 (lldb) file hello
  
  # step，傳遞執行檔參數，此處的 target 指向 hello
  (lldb) settings set -- target.run-args "foo"
  ```

- [指令] 斷點相關

  - 印出所有的中斷點 : 
    - 方法，(只縮寫breakpoint)，`$ breakpoint list` 或 `$ break list` 或 `$ br list` 或 `$ br l`，
      
      其中 
      - breakpoint，可縮寫為 break 或 br
      - listl，可縮寫為 l

    - 方法，(縮寫breakpoint set ...)，`$ b`，
      
      b 是智能縮寫，
      - 若已經設置 breakpoint 時，`b = breakpoint list`
      - 若沒有設置任何 breakpoint 時，`b = breakpoint set`，因此，`b 5`等效於`breakpoint set 5`

  - 設置斷點
    - 語法，breakpoint set的以下三種語法是等效的
      - breakpoint set ...  (breakpoint的縮寫)
      - br s ...            (breakpoint的縮寫)
      - b ...               (breakpoint set的縮寫)
  
    - breakpoint set 可用的子參數
      - `--file` 或 `-f` : 指定源碼檔名，通常和 --line 一起搭配使用
      - `--line` 或 `-l` : 指定源碼中的指定行數，通常和 --file 一起搭配使用
  
      - `--name` 或 `-n` : 指定一般函數名
      - `--method` 或 `-M` : 指定類函數名
      - `--selector` 或 `-S` : 指定Objective-C的類方法或實例方法
  
      - 範例，`breakpoint set --name 全局函數名|一般函數名|靜態函數`
      - 範例，`breakpoint set --method 類成員函數|虛函數|重載方法`
      - 範例，`breakpoint set --selector Objective-C的類方法或實例方法`

    - 設置條件式斷點(conditional-break)

      ```
      for(PlayerItem *item in player.inventory) {
          totalValue += item.value;
      }
      ```

      ```
      (lldb) b MerchantViewController.m:32
      Breakpoint 3: where = lootcollector`-[MerchantViewController] ...
      (lldb) br mod -c "totalValue > 1000" 3
      ```

    - 快速設置斷點
      - `b 函數名`，例如，`b main`
      - `b 行號`，例如，`b 10`，注意，必須是有效的行，`空白行`或是`預編譯行`都是無效的行
      - `b 檔案路徑:行號`，例如，`b src/main.c:19`，對(特定檔案)中的(特定行號)設置斷點

    - 注意，沒有設置成功的斷點仍然會有斷點ID，但是是無效的中斷點

      例如，若將中斷點設置在空白行，會造成無效的中斷點

      以下為例，雖然有 breakpoint-id 但是沒有 locations

      ```
      (lldb) b 1
      Breakpoint 2: no locations (pending).
      WARNING:  Unable to resolve breakpoint to any actual locations.
      ```

  - 暫時禁用斷點/啟用斷點
    - 暫時禁用 : 
      - 禁用單個斷點，`breakpoint disable 斷點ID` 或 `br di 斷點ID`
      - 禁用多個斷點，`breakpoint disable 1.*`
  
    - 恢復啟用 : `breakpoint enable 斷點ID` 或 `br e`

  - 刪除斷點
    - `breakpoint delete`或 `br del`，一次性刪除所有的斷點

    - `breakpoint delete 斷點ID 斷點ID 斷點ID ...` 或 `br del 斷點ID 斷點ID 斷點ID ...`，完全刪除斷點，包含刪除斷點ID
    
    - `breakpoint clear -f 源碼位置 -l 行號`，僅刪除匹配的位置，但不刪除斷點ID，
      - 例如，breakpoint clear -f hello.c -l 10
      - 需要和 -f、-l 一起使用，不接受輸入斷點ID
  
  - 修改斷點觸發條件，modify
    - 可用參數
      `--condition`(-c) : 必須評估為true才能停止的表達式
      `--ignore-count`(-i) : 指定停止之前跳過斷點的次數
      `--one-shot`(-o) : 第一次停止時刪除斷點，例如，`breakpoint modify --one-shot 1`
      `--queue-name`(-q) : 指定斷點停止的隊列的名稱
      `--thread-name`(-T) : 指定斷點停止的線程的名稱
      `--thread-id`(-t) : 指定斷點停止的線程的ID（TID）
      `--thread-index`(-x) : 指定斷點停止的線程的索引
    - `breakpoint modify --condition "totalValue > 1000" 3`
      
      修改斷點3的觸發條件(中斷條件)，只有在變數 totalValue 的值大於1000時，
      才觸發斷點3的中斷

    - `br mod -c "" 3`，將斷點3的中斷條件設置為空 (重置斷點3的中斷條件)

- [指令] 中斷時執行 lldb 命令，command
  
  - 範例

    ```shell
    # 建立斷點1
    (lldb) breakpoint main

    # 建立斷點1被觸發時，要執行的 lldb 命令，輸入完畢後，最後一行要輸入DONE才能結束
    (lldb) breakpoint command add 1
    > bt 
    > continue
    > DONE

    # 列出斷點1所有的命令列表
    (lldb) breakpoint command list 1

    # 執行 
    (lldb) run
    ```

- [指令] 觀察點相關(watchpoint)

  - 比較 : 斷點(breakpoint) VS 觀察點(watchpoint) 的差異

    - 觀察點在變數值變動時，會`自動打印`出新舊變數值，而中斷點需要`手動打印`變數值
    - 設置觀察點有數量上的限制，取決於CPU支援的程度

    | 類別     | 斷點（Breakpoint）             | 觀察點（Watchpoint）                                  |
    | -------- | ------------------------------ | ----------------------------------------------------- |
    | 觸發條件 | 當執行到特定代碼行或函數時觸發 | 需要先設置斷點，當特定內存地址的值被讀/寫或修改時觸發 |
    | 設置方式 | 設定代碼行號、函數名或內存地址 | 設定變量或內存地址                                    |
    | 使用場景 | 調試特定函數或代碼執行流程     | 監視變量何時、如何被修改                              |
    | 作用範圍 | 影響整個代碼流的執行           | 影響特定內存地址的訪問                                |
    | 性能開銷 | 較低，僅在命中時檢查           | 較高，需要 CPU 硬件支持                               |
    | 適用變量 | 適用於所有類型的變量           | 僅適用於`存儲在內存中的變量`（寄存器變量無法監視）    |

  - 設置觀察點

    - 觀念，
      
      觀察點主要是`監控內存中變數值的變化`(某個內存位置的內容變化)，
      而不像斷點是由特定代碼行觸發，觀察點`必須是程式運行的狀態下才能設置觀察點`，

      因為觀察點需要在程式運行後，才能加載並監控內存中的變數值

      因此

      - 需要`先設置斷點`，`執行run`並停在斷點(表示已經載入當前函數的frame)後，
        才設置觀察點

        以上述的 hello.c 為例

        ```shell
        gcc -g -O0 -o hello hello.c
        lldb ./hello

        (lldb) b main            # 在 main() 設置斷點
        (lldb) run               # 運行程序，停在 main()
        (lldb) watchpoint set variable n 或 (lldb) w s v 變數名

        # 以下是被建立的觀察點
        Watchpoint created: Watchpoint 1: addr = 0x7fffffffbabc size = 4 state = enabled type = m declare @ '/root/cTutor/hello.c:12:7'
          watchpoint spec = 'n'
          watchpoint resources:
            #0: addr = 0x7fffffffbabc size = 4    # 提供變數n在內存中的位址
        
        Watchpoint 1 hit: # 表示 watchpoint 有找到一個具有內存位址的實際目標
        new value: 32767  # 且該位址當前的值為 32767

        (lldb) n

        Watchpoint 1 hit:
        old value: 32767
        new value: 10     # 變數n的植被改變為 10
        ```

      - 若使用 -O2 編譯代碼，有可能會使`透過變數名設置watchpoint`失敗

        - 解決方法，改用 -O0 編譯，例如，`gcc -g -O0 -o hello hello.c`

        - 解決方法，改用變數內存地址設置watchpoint
          
          以上述的 hello.c 為例

          ```shell
          lldb ./hello

          (lldb) b main            # 在 main() 設置斷點
          (lldb) run               # 運行程序，停在 main()
          (lldb) frame variable n  # 查看變量 n 的信息
          (lldb) expression &n     # 獲取變量 n 的地址
          (lldb) watchpoint set expression -w write -- &n
          ```

  - 設置觀察點，
    - 手動設置監控模式，`watchpoint set var -w 監控模式 變數名`
      - -w 可用的參數值
        - `-w read` : 內存位址被讀取時觸發
        - `-w write` : 內存位址被寫入時觸發，預設值
        - `-w read_write` : 內存位址被讀取或寫入時觸發

    - 使用預設監控模式，`watchpoint set variable 變數名` 或 `watch set var 變數名` 或 `w s v 變數名` 或 `w s v struct名.變數名`，
      不使用 -w 時，預設自動添加 -w write

  - 列出所有觀察點，`watchpoint list` 或 `watch list` 或 `w list`

  - 修改觀察點觸發時機，`watch modify -c '(變數名=變數值)'`，其中，-c 代表 --condition
  
  - 刪除觀察點
    - 刪除所有觀察點，`watchpoint delete` 或 `watch delete` 或 `w del`
    - 刪除指定觀察點，`watchpoint delete ID` 或 `watch delete ID` 或 `w del ID`

- [指令] 顯示代碼，list
  - $ list，顯示當前執行位置的以下幾行，注意，需要先執行指定位置才會有效，例如，run 或 list 5
  - $ list 5，顯示行號以下幾行

- [指令] 執行程式
  - `$ process launch 執行檔名` 或 `$ run` 或 `$ r`，啟動或重新執行到第一個斷點
    - $ run 是 'process launch -c /nix/store/c3ym86hnrsh03k11mhbvvcw7f0y403vr-bash-interactive-5.2p37/bin/bash --' 的語法糖
    - $ r 是 $ run 的縮寫

  - 其他執行方式
    - process attach --pid 123
    - process attach --name Sketch
    - process attach --name Sketch --waitfor
    - 詳見 `$ help process`

- [指令] thread | frame (執行控制)
  
  - 觀念
    - thread 指的是線程，每個執行檔運行時，至少會有一個用於執行的主線程
    - frame 指的是 call-stack 中的其中一個上下文(frame)，每個函數被執行時，都會有自己的frame，
      用於保存該函數被執行時，函數作用域內所有的變量和相關訊息
  
  - 注意，fr 是 frame 命令的縮寫，f 是 frame select 的縮寫

  - 注意，以下操作需要在中斷狀態中，在中斷狀態下代表正位於某一個 frame 中

  - `$ continue` 或 `$ c`，繼續運行程序，直到下一個斷點，若沒有其他斷點會直接結束執行，(continue)，是 `$ process continue` 的語法糖
  - `$ step` 或 `$ s`，進入到下一個函數內，(step-into 或 step-in)，是 `$ thread step-in` 的語法糖
  - `$ next` 或 `$ n`，執行到下一行指令，(next-line 或 step-over)，是 `$ thread step-over` 的語法糖
    - 若下一行指令是調用函數，會直接執行完函數並停在下一行，不會進入函數中
  - `$ finish`或`$ fin`，跳出當前的函數(離開當前frame並返回)，(step-out)，是 `$ thread step-out` 的語法糖

  - `$ thread backtrace` 或 `$ th back` 或 `$ th b`，列出完整的 call-stack (列出所有的frame)

    ```shell
    (lldb) thread backtrace
    * thread #1, name = 'hello', stop reason = step in
      * frame #0: 0x0000000000401132 hello`fibonacci(n=9) at hello.c:5:6        # 最新frame
        frame #1: 0x000000000040114a hello`fibonacci(n=10) at hello.c:8:10
        frame #2: 0x000000000040117c hello`main at hello.c:14:3
        frame #3: 0x00007ffff7c2a1fc libc.so.6`__libc_start_call_main + 124
        frame #4: 0x00007ffff7c2a2b9 libc.so.6`__libc_start_main@@GLIBC_2.34 + 137
        frame #5: 0x0000000000401065 hello`_start + 37                          # 最舊frame(第一個被執行的frame)
    (lldb)
    ```

  - `$ bt`，列出當前thread 的 call-stack，效果和`$ th b`類似，但只列出當前 thread

  - `$ thread select THREAD-index` 或 `$ t THREAD-index`，跳轉到特定的 thread，
    - t 是 thread select 的縮寫

  - `$ frame select FRAME-id` 或 `fr s FRAME-id` 或 `$ f FRAME-id`，跳轉到特定的 frame
    - f 是 frame select 的縮寫

  - `$ thread info`，打印current-thread的summary訊息

- [指令] 打印變數值，print | frame variable
  - `$ frame variable` 或 `fr v`，打印當前 frame 中的所有變數值
  - `$ frame variable 變數名`，打印當前 frame 中的指定變數的值
  - 更多 frame 命令的用法，詳見 help frame

  - `$ print 變數名` 或 `$ p 變數名`，(Print object type)，打印指定變數的值，打印出原始類型的變數值，是 $ dwim-print -- 的語法糖
  - `$ po 變數名`，(Print primitive type)，打印指定變數的值，打印出原始類型的變數值

- [指令] 為常用指令建立別名
  - `command alias 別名 <別名擴展內容>`，
    - 例如，`command alias bfl breakpoint set -f %1 -l %2`
    - 調用，`bf1 foo.c 12`

- [指令] 打開GUI版lldb，`$ gui`

  注意，進入gui後才執行 target create 會看不到任何訊息，
  需要透過命令設置斷點並運行後才看的到

  ```shell
  lldb ./hello
  (lldb) b main
  (lldb) r
  (lldb) gui
  ```

- [指令]，退出正在執行的程式，`$ kill`

- [指令]，退出 lldb，`$ exit`

- 快速鍵
  - 重複上一個 lldb 命令，`<Enter>`
  - 打印可用的命令，`<Tab>`
  - 退出lldb，`<ctrl + D>`

## ref 

- [GDB to LLDB command map](https://lldb.llvm.org/use/map.html)

- [lldb cheatsheet](https://gist.github.com/ryanchang/a2f738f0c3cc6fbd71fa)

- [斷點和觀察點的使用](https://developer.apple.com/library/archive/documentation/General/Conceptual/lldb-guide/chapters/C3-Breakpoints.html)

- [LLDB Quick Tutorial for Beginners](https://www.youtube.com/watch?v=r5HQqU_6siA)

- [設置斷點的進階使用 @ y2b-constref](https://www.youtube.com/watch?v=2GV0K9Y2MKA)

- [gui版lldb的使用](https://youtu.be/v_C1cvo1biI?si=NB779kDPZVD8MssB&t=467)