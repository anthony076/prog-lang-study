import kotlin.reflect.full.declaredFunctions

/*
#0 預備知識
    -   什麼是反射，詳細請參考 高階函數_01反射(引用)
        在運行時，透過引用(::)來獲取類的函數, 屬性, 父類, 接口, metadata, 泛型, ...等, 類的內部訊息
        又稱為 RTTI(Run-Time Type Information)，運行時類型訊息，讓程序可以訪問、檢測和修改它本身狀態或行為的一種能力
        通常用在函數式編程中(高階函數)

    -   引用的方式
        類引用
        函數引用
        屬性引用
        構造器引用

Annotation(註解) 是什麼
#1. 註解可以用來
    1_生成源碼或者其它文件 (範例，請參考 https://blog.csdn.net/a296777513/article/details/79205798)
    2_產生提示
    3_檢查錯誤。

#2. 使用註解的三步驟
    1. 建立註解類 (或使用內建或第三方庫的註解)
    2. 標註註解
    3. 調用註解類 (非必要，看註解的內容是什麼，有的只是要添加代碼不需要調用)

    注意，要讓 kotlin 能夠識別註解，必須添加 kotlin.reflect.full.declaredFunctions


#3  註解類的建立
    -   註解是一個類(註解類)，需要透過 annotation class 類名，來建立
        透過 @註解類名 讓編譯器在編譯時，自動將註解類添加至對象中

    建立註解類時，也可以使用元註解 (用來定義註解的註解)，來為該註解類添加特定的特性
    -   @Target(AnnotationTarget.CLASS,AnnotationTarget.FUNCTION,AnnotationTarget.VALUE_PARAMETER,AnnotationTarget.EXPRESSION)
        表示該註解可以注解的元素类型（类、函数、属性、表达式等）
    -   @Retention(AnnotationRetention.RUNTIME)
        表示該註解是否存储在编译后的类文件中，是否在运行时可见
    -   @Repeatable
        单个元素是否可以多次使用注解
    -   @MustBeDocumented
        表示該註解是公共API的一部分。生成的API文档是否显示类或方法包含的注解

#4. 註解類的調用步驟 (註解類不像一般類，無法透過實例化來取用)
        Step1.  通過反射來取得具有註解的對象
                val rClass = sword::class
        Step2.  透過 .declaredFunctions 取得對象中所有宣告的函數集合
                rClass.declaredFunctions
        Step3.  透過 .annotations 該函數中所添加的註解集合
                f.annotations
        Step4.  註解類的屬性與函數的調用
                f.annotations.id 取得註解中的參數
                f.call(函數, 參數)  將註解中的參數丟到函數中執行

#5. kotlin 支援透過 @目標.註解名 的方式，來限定使用註解的對象
    其中，目標可以是以下其中之一
    property:代表kotlin中的属性，不能被Java的注解所应用
    field:      为属性生成的字段（包括后备字段）
    get:        属性的getter
    set:        属性的setter
    receiver:   扩展函数/属性的接收者
    param:      构造函数的参数
    setparam:   属性setter的参数
    delegate:   委托属性存储委托实例的字段
    file:       在文件中声明的顶层函数与类
*/

// 建立註解類 TestCase
// 標註時，透過 @TestCase() 使用
annotation class TestCase(val id: String)

class SwordTest {
    // 標註註解使用的對象
    @TestCase(id = "1")
    fun testCase(testId: String) {
        println("Run SwordTest ID = ${testId}")
    }
}

fun main(args: Array<String>) {
    println("==== 調用註解範例 ====")

    val sword = SwordTest()

    // ===== Step1. 透過反射取得 sword類 的引用 =====
    val rClass = sword::class   // 返回 class SwordTest

    // ===== Step2. 利用 rClass 取得 sword類中所有宣告的 Function =====
    val Functions = rClass.declaredFunctions    // 返回 [fun SwordTest.testCase(kotlin.String): kotlin.Unit]

    for (f in Functions){
        // ===== Step3. 透過 f.annotations 取得有添加註解的所有Function =====
        f.annotations.forEach{
            println(it)
            if (it is TestCase){
                // 取得註解類中的參數
                println(it.id)

                // 將註解類中的參數丟給 sword 執行
                f.call(sword, it.id)
            }
        }
    }
}

