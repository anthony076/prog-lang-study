#include <iostream>

using namespace std;

void if_example(){
    cout << "==== if 使用範例 ====" << endl;

    short a = 0;

    if (a<=0){
        cout << "a is zero" << endl;
    }
}


void if_elseif_else_example(){
    cout << "==== if_else 使用範例 ====" << endl;

    // 取得使用者輸入
    cout << "請輸入數字 : \n" << endl;
    int input;
    cin >> input;


    // 對使用者輸入進行判斷
    if (input < 0){
        cout << "a is negtive" << endl;
    } else if (input == 0){
        cout << "a is zero" << endl;
    } else {
        cout << "a is positive" << endl;
    }
}


void switch_example(){
    cout << "==== switch使用範例 ====" << endl;

    cout << "input color code : " << endl;
    char score;
    cin >> score;


    /*
    #   switch 语句中的 expression 必须是一个整型或枚举类型，或者是一个 class 类型，
        其中 class 有一个单一的转换函数将其转换为整型或枚举类型。
    #   case 的 constant-expression 必须与 switch 中的变量具有相同的数据类型，且必须是一个常量或字面量。
    #   当被测试的变量等于 case 中的常量时，case 后跟的语句将被执行，直到遇到 break 语句为止。
    #   (非必要)一个 switch 语句可以有一个可选的 default case，出现在 switch 的结尾。
    */
    switch(score){
        case 'y':
            cout << "Yellow" << endl;
            break;
        case 'g':
            cout << "Green" << endl;
            break;
        case 'r':
            cout << "Red" << endl;
            break;
        default:
            cout << "Unknown color code" << endl;
    }
}


void ternary_example(){
    cout << "==== 三元運算符的使用範例 ====" << endl;

    // ?: 三元運算符，用來取代 if..else..
    int score;
    cout << "Input your score : ";
    cin >> score;

    score > 60 ? cout << "You are PASS" << endl : cout << "You are Fail" << endl;

}

int main(int argc, const char * argv[])
{
    // if 的使用範例
    if_example();

    // if..else if..else的使用範例
    if_elseif_else_example();

    // switch的使用範例
    switch_example();

    // 三元運算符的使用範例
    ternary_example();
}
