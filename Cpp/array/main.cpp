#include <iostream>
#include <cstdio>
#define MAX_NUMBER 12

using namespace std;

// ==== 計算陣列長度範例 ====
void cal_size(){
    cout << "==== 計算陣列長度 ====" << endl;

    short array[] = { 11,22,33,44,55};

    // 計算 array 的長度
    // (sizeof array) array 的總長度，此處為 10
    // (sizeof array[0]) 其中一個元素的長度，此處為 2
    const int SIZE = (sizeof array)/(sizeof array[0]);
    cout << "Size: " << SIZE << endl;       // 印出 Size: 5
    cout << (sizeof array) << endl;         // 印出 10
    cout << (sizeof array[0]) << endl;      // 印出 2
}


// ==== 二維陣列範例 ====
int create_tuple(int tuple[][MAX_NUMBER]){
    int i,j;

    if (tuple == NULL){
        return -1;
    }

    for(i = 0; i < MAX_NUMBER; i++){
        for(j = 0; j < MAX_NUMBER; j++){
            tuple[i][j] = (i + 1)*(j + 1);
        }
    }
    return 0;
}

void two_diamention_example(){
    cout << "==== 二維陣列範例 ====" << endl;

    int i,j;
    int tuple[MAX_NUMBER][MAX_NUMBER] = {{0}};
    create_tuple(tuple);

    for(i = 0; i < MAX_NUMBER; i++){
        for(j = 0; j < MAX_NUMBER; j++){
            printf("% 4d",tuple[i][j]);
        }
        printf("\n");
    }
}


// ==== 傳遞陣列參數給函數範例 ====
double getAverage(int arr[], int size){

    // 注意，此處傳進來的是 array 第一個元素的記憶體位址，不是傳進整個 array
    cout << arr << endl;    // 印出 0x7ffeee3abaa0

    int sum = 0;

    for(int i=0; i<size ; ++i){
        sum += arr[i];
    }

    // 注意，此處 sum和size 都是 int 類型，要先轉型後，return 才會是 double 浮點數
    return (double)sum/size;
}

void pass_array_to_function(){
    int balance[] = { 1000,2,3,17,50 };

    // 計算 array 的大小
    // 注意，傳遞 array 的時候只有傳遞array 第一個元素的記憶體位址，不是傳進整個 array
    // 所以 size 要在外面算
    int size = (sizeof balance)/(sizeof balance[0]);

    // 注意，此處傳進來的是 array 第一個元素的記憶體位址，不是傳進整個 array
    double avg = getAverage(balance,size);

    cout << "average of array : " << avg << endl;   // 印出 average of array : 214.4
}

int main() {

    // 計算陣列長度範例
    //cal_size();

    // 二維陣列範例
    //two_diamention_example();

    // 傳遞陣列參數給函數範例
    pass_array_to_function();

    return 0;
}