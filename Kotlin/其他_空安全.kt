
/*
# 安全調用空變數的各種方法
    - 利用 if 確保變數的空安全，語法複雜但返回可自訂
    - 利用 ？ 確保變數的空安全，語法複雜但返回有可能是 null
    - 利用 ? 和 ?: 確保變數的空安全，語法簡單且返回可自訂
    - 利用 !! 非空斷言操作符，如果變數為空，會主動拋出異常
    - 利用 as 操作符作安全的類型轉換，用於將值做顯式的類型轉換

      例如， val aInt:Int? = a as? Int
      若是使用 as : 如果該變數與指定的類型相容，就會成功轉換，若不相容，就會拋出異常
      若是使用 as? : 如果該變數與指定的類型相容，就會成功轉換，若不相容，就會返回 null
*/

fun main(args: Array<String>) {

    // 利用 if 確保變數的空安全，語法複雜但返回可自訂
    check_safety_if()

    // 利用 ？ 確保變數的空安全，語法複雜但返回有可能是 null
    check_safety_operator()

    // 利用 ? + ?: 確保變數的空安全，語法簡單且返回可自訂
    check_safety_elvis()

    // 利用 !! 非空斷言操作符，如果變數為空，會主動拋出異常
    check_safety_NPE()
}

fun check_safety_if(){
    println("==== check_safety_if() ====")

    // 定義可空變數，
    // 透過在類型後方加上問號？，代表此變數可為空
    var a: String? = "hello"
    a = null
    println(a)

    // 因為變數a可為空，因此使用a的屬性時會有危險
    //val length = a.length   // 此行會造成編譯錯誤

    // 安全調用可空變數的方法
    val length = if (a == null) -1 else a.length
    println(length)     // 返回 -1
}

fun check_safety_operator(){
    println("==== check_safety_operator() ====")

    // 定義可空變數，
    // 透過在類型後方加上問號？，代表此變數可為空
    var a: String? = "hello"
    a = null
    println(a)

    // 因為變數a可為空，因此使用a的屬性時會有危險
    //val length = a.length   // 此行會造成編譯錯誤

    // 安全調用可空變數的方法
    var length = a?.length
    println(length)       // 返回 null

}

fun check_safety_elvis(){
    println("==== check_safety_elvis() ====")

    // 定義可空變數，
    // 透過在類型後方加上問號？，代表此變數可為空
    var a: String? = "hello"
    a = null
    println(a)

    // 因為變數a可為空，因此使用a的屬性時會有危險
    //val length = a.length   // 此行會造成編譯錯誤

    // 安全調用可空變數的方法
    // 如果 ?: 左侧表达式非空，elvis 操作符就返回其左侧表达式，否则返回右侧表达式
    var length = a?.length ?:-1
    print(length)       // 返回 -1

}

fun check_safety_NPE(){
    println("==== check_safety_NPE() ====")

    // 定義可空變數，
    // 透過在類型後方加上問號？，代表此變數可為空
    var a: String? = "hello"
    a = null
    println(a)

    // 因為變數a可為空，因此使用a的屬性時會有危險
    //val length = a.length   // 此行會造成編譯錯誤

    // 安全調用可空變數的方法
    var length = a!!.length
    print(length)       // 如果變數為空，會拋出異常
}