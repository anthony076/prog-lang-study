#include <iostream>
#include <csignal>
#include "unistd.h"
#include "main.h"

using namespace std;

// 訊號處理函數,當訊號產生時，會自動調用此函數
// 必須先先透過 signal() 將訊號與此函數連結
void signalHandler(int signNum){
    cout << "Interrupt signal (" << signNum << ") received." << endl;

    // std::lib(int) 關閉呼叫的程序
    // http://www.cplusplus.com/reference/cstdlib/exit/
    exit(signNum);

}

int main(int argc, const char * argv[])
{
    // 註冊訊號和產生訊號範例
    register_produce_signal();


}

void register_produce_signal(){
    cout << "==== 註冊訊號和產生訊號範例 ====" << endl;


    // 註冊訊號，signal(註冊訊號種類, 信號處理函數)
    // SIGINT 偵測中斷訊號
    // 其他訊號種類，請參考 http://www.runoob.com/cplusplus/cpp-signal-handling.html
    signal(SIGINT,signalHandler);

    // 執行十次，第四次時自動產生中斷的訊號
    for(int i=0;i<10;i++){

        if(i==3){
            // 產生中斷訊號
            raise(SIGINT);
        }else{
            cout << "Going to sleep..." << endl;
        }

        sleep(1);
    }

}

