

fun main(args: Array<String>) {
    // 一般使用方式
    example1()

    //把 try...catch...放在賦值
    val values= intArrayOf(5,10)
    example2(values)

    //把 try...catch...放在return
    println(example3(10,5))
}

fun sum(x: Int, y: Int): Int = x + y

fun example1(){
    try {
        println("請輸入數字1 :")
        val x:Int = readLine()!!.toInt()
        println("請輸入數字2 :")
        val y:Int = readLine()!!.toInt()
        println("$x + $y = ${sum(x, y)}")
    } catch(e: NumberFormatException) {
        println("確定傳入的是整數吗?")
    } catch (e: Exception) {
        println("出現未知異常，${e.message}")
    } finally {
        println("謝謝使用")
    }
}

fun example2(args: IntArray){

    val result = try {
        args[0].toInt() / args[1].toInt()
    } catch (e: Exception) {
        e.printStackTrace()
        0
    }

    println(result)

    println("3 / 4 = ${sum(3, 4)}")

}

fun example3(x: Int,y: Int):Int{
    return try {
        x / y
    } catch (e: Exception) {
        0
    } finally {
        println("end")
    }
}
