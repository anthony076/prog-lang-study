All of data inside branches coming from website

Branches
- [HelloWorld](https://github.com/chinghua0731/kotlin_android_app/tree/HelloWorld-default)
- [Button](https://github.com/chinghua0731/kotlin_android_app/tree/Button)
- [ListView](https://github.com/chinghua0731/kotlin_android_app/tree/ListView)
- [BMI example](https://github.com/chinghua0731/kotlin_android_app/tree/bmi)
- [Contact example](https://github.com/chinghua0731/kotlin_android_app/tree/contact-example)
- [ATM example](https://github.com/chinghua0731/kotlin_android_app/tree/atm)
- [anko-tutorial](https://github.com/chinghua0731/kotlin_android_app/tree/anko-tutorial)
