/*
# if-else 有一般用法和簡易用法兩種

# while-do

# when 當 Case 使用時，需要帶入變數
    when(變數){
        變數值1 -> 程式碼
        變數值2 -> 程式碼
    }
# when 當 if-else 使用時，不需要帶入參數，when{ ... }
    when{
        邏輯運算式1 -> 程式碼
        邏輯運算式2 -> 程式碼
    }

# break, continue 與 python 語法同
# label  等同 C++ 中的 goto

*/
import java.util.Random

fun main(args:Array<String>) {
    // ===== 迴圈 =====

    // for-loop 的使用
    loop_example()
    // while 的使用
    while_example()
    // do-while 的使用
    do_while_example()

    // ===== 控制 =====
    // if 的使用
    if_example_regular()    // 一般使用
    if_example_express()    // 快速使用

    // when 的使用
    when_example_case()     // when 當 case 使用
    when_example_elif()     // when 當 else if 使用

    // ===== 進階 =====
    // break 的使用
    break_example()
    // continue 的使用
    continue_example()
    // Label 的使用
    lable_example()
}

fun loop_example(){
    println("==== loop_example() ====")

    // 透過 Range 變數進行 for...loop...
    // 更多 Ｒange 的用法，請參考 02_Range語法的使用.kt
    for (i in 1..5){
        println(i)
    }

    // 透過 Ａrray 變數進行 for...loop...
    // 宣告 Array 變數
    val numbers : IntArray = intArrayOf(11,22,33,44,55)
    for (i in numbers){
        println(i)
    }
}

fun while_example(){
    println("==== while_example() ====")

    var i = 0

    while (i < 5){
        print("$i ")
        i++
    }
    println()

}

fun do_while_example(){
    println("==== do_while_example() ====")

    var i = 1
    var total = 0

    do {
        total += i
        i++
    } while (i <= 10 )

    println("total : $total")
}

fun if_example_regular(){
    println("==== if_example_regular() ====")

    val a = Random().nextInt(100)
    val b = Random().nextInt(100)

    println("a : $a, b : $b ")

    if( a > b ){
        println("a > b")
    } else if( a < b){
        println("a < b ")
    } else {
        println("a = b ")
    }
}

fun if_example_express(){
    println("==== if_example_express() ====")

    // 快速寫法，單行程式碼的時候，可以省略 { }
    // 語法，if (條件式) 成立時的程式碼 else 不成立時的程式碼
    val a = Random().nextInt(100)
    val b = Random().nextInt(100)
    println("a : $a, b : $b ")

    // 快速寫法一
    if (a > b ) println("a > b") else println("a < b or a = b")

    // 快速寫法二
    var Result = if (a > b ) "a > b" else "a < b or a = b"
    println(Result)
}

fun when_example_case(){
    println("==== when_example_case() ====")

    // 範例一
    // 字元型態的顏色代碼
    val colorCode: Char = 'B'
    // 判斷顏色代碼變數值
    when (colorCode) {
        'R','B' ->  println("Red or Blue")
        'G' ->  println("Green")
        else -> println("undefined")
    }

    // 範例二
    val a = Random().nextInt(100)
    val b = Random().nextInt(100)
    println("a: $a")

    // 判斷數字位於哪個區間
    when (a) {
        in 0..100 -> println( "a is in the range of 0 - 100")
        in 101..200 -> println( "a is in the range of 100 - 200")
        else -> println( "none of the above")
    }

}

fun when_example_elif(){
    // when 當 Case 使用時，需要帶入參數，when(x){ ... }，
    // when 當 if-else 使用時，不需要帶入參數，when{ ... }
    println("==== when_example_elif() ====")

    val a = Random().nextInt(100)
    val b = Random().nextInt(100)
    println("a: $a")
    when {
        a > b -> println("a > b")
        a < b -> println("a < b")
        else -> println("a = b")
    }
}

fun break_example(){
    println("==== break_example() ====")

    var i = 0

    while (true) {
        println(i)
        i++

        if (i == 5) {
            break
        }
    }
}

fun continue_example(){
    println("==== continue_example() ====")

    for (i in 1..10) {
        // Math.random()產生一個隨機數字
        val temp = (Math.random() * 36).toInt()

        if (temp < 6) {
            continue
        }

        println("Temperature[$i]: $temp")
    }
}

fun lable_example(){
    println("==== lable_example() ====")

    outer@ for (x in 1..5) {
        for (y in 1..5) {
            println("X:$x - Y:$y")

            if (y == 3) {
                break@outer
            }
        }
    }
}