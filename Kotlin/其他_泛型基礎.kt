/*

# 什麼是泛型(Generic)，
    類似一種模版的概念，在調用的時候才將實際的類型填入
    - 加在類名後方，用 <T> 代表的一個多類型的宣告
    - 在類中，在宣告類型的地方使用 T 代表該處使用多類型
    - 在調用該類時，將實際的類型填入 <T> 中，kotlin為自動將類中的所有Ｔ取代為實際類型
    - 類似 C++ 的模板

# 泛型類的建立，把泛型宣告放在  類名後
    - 把泛型類當成是一個模板，所有跟類型相關的，都用Ｔ代表，等到實例化的時候才把<T>的地方，用實際的類型代入
    - 語法，class 類名<泛型代碼>
      其中，泛型代碼可以是其它字元，但一般會使用下列規則以供方便識別
        - 第一個字大寫
        - T : 型態
        - S : 第二形態 U : 第三形態 V : 第四形態
        - E : 元素
        - K : 索引鍵型態
        - V : 資料型態

# 泛型函式的建立
    - 泛型函式  的泛型宣告放在 函數名前

    - 語法，fun <泛型代碼> 函式名(引數1:T, 引數2:類型2)
      調用時，泛型宣告的 <T> 不用寫

# 泛型接口的建立

*/

// ==== 泛型類的建立與使用 =====
// class Box<T>(x:T)，代表 class Box 有使用泛型，其中，x:T 引數 x 為泛型類型
class Box<T>(x:T){
    private var value = x

    // get()返回的類型為T類型
    fun get():T{
        return value
    }

    // set()中 newX的類型為T類型
    fun set(newX:T){
        value = newX
    }
}

fun main(args: Array<String>) {

    // 泛型類的建立與使用
    generic_class_example()

    // 泛型函式的建立與使用1
    println("===== 泛型函式的建立與使用1 =====")
    println(generic_function_example("Hello"))
    println(generic_function_example(123))

    // 泛型函式的建立與使用2
    println("===== 泛型函式的建立與使用2 =====")
    doPrintln(123)
    doPrintln("abc")
}

fun generic_class_example(){
    println("===== 泛型類的建立與使用 =====")

    // 將 Box<T>中的 T類型設置為 String
    val s = Box<String>("hello")
    println(s.get())
    s.set("Anthony")
    println(s.get())

    // 將 Box<T>中的 T類型設置為 Int
    val i = Box<Int>(123)
    println(i.get())
    i.set(456)
    println(i.get())

}

// ===== 泛型函式的建立與使用1 =====
fun <T> generic_function_example(v:T):T {
    val value: T = v
    return value
}

// ===== 泛型函式的建立與使用2 =====
fun <T> doPrintln(content: T) {
    // 判斷傳進來的參數 content 是什麼類型
    when (content) {
        is Int -> println(content % 10)
        is String -> println(content.toUpperCase())
        else -> println("T is not Int and String")
    }
}