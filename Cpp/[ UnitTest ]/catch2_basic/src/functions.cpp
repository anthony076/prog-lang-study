#include <cstdlib>
#include <ctime>

int add(int a, int b){
    return a+b;
}

int minus(int a, int b){
    return a-b;
}

double float_test(){
    double min = 1;
    double max = 2;
    return (max - min) * rand() / (RAND_MAX +1.0)+ min;
}