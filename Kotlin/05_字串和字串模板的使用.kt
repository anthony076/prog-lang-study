
/*
# 在字串中使用 ＄，以取得數據變數的值
# 在字串中使用 ＄{運算式}，以執行單行運算式
# 在字串中使用 \ 來區別特殊字元
# 在字串中使用 .repeat() 來重複相同字串

Ref:
http://www.codedata.com.tw/kotlin/kt08/

*/
fun main(args: Array<String>) {
    // 建立字串
    buildString()

    // 重複相同字串
    str_repeat()

    // 若字串字數小於指定值，自動補上空白
    str_pad()

    // 移除字串的指定內容
    str_remove()

    // 使用字串模板
    useStrTemplate()


}

// 建立字串
fun buildString(){
    println("==== buildString() ====")
    // 一般變數可使用省略寫法
    // 範例，val str = "Anthony"
    val str:String = "Anthony"
    println(str)
}

// 重複相同字串
fun str_repeat(){
    println("==== str_repeat() ====")
    println("#".repeat(10))
}


// 若字串字數小於指定值，自動補上空白
fun str_pad(){
    println("==== str_pad() ====")

    // 字串數量不足，則在前方補上空白
    println("123".padStart(5))

    // 字串數量不足，則在前方補上自訂字元
    println("123".padStart(5, '-'))

    // 字串數量不足，則在後方補上空白
    println("123".padEnd(5))

    // 字串數量不足，則在前方補上空白
    println("123".padEnd(5, '*'))
}

// 移除字串的指定內容
fun str_remove(){
    println("==== str_remove() ====")

    // 移除字串前方的指定內容
    println("123Hello".removePrefix("123"))

    // 移除字串後方的指定內容
    println("Hello123".removeSuffix("123"))

    // 移除範圍內的字串
    println("456Hello".removeRange(0..2))
}

// 使用字串模板
fun useStrTemplate(){
    println("==== useStrTemplate() ====")

    // 在字串中使用 ＄，以取得數據變數的值
    val i = 10
    val j = 5
    println("i=$i, j=$j")

    // 在字串中使用 ＄{運算式}，以執行單行運算式
    println("i + j = ${i+j}")

    // 在字串中使用 \ 來區別特殊字元
    println("c:\\123\\456\\789")

}
