#include <iostream>
#include <ctime>

using namespace std;

void time_by_struct_pointer(){
    cout << "==== 使用結構指標取得tm結構的組成部分 ====" << endl;

    // 基於當前系統的日期與時間
    time_t now = time(0);

    cout << "TimeStamp:" << now << endl;                // 印出 TimeStamp:1523877005

    // tm 為 結構指標，定義在 time.h 中
    tm *ltm = localtime(&now);

    // 使用 -> 存取 tm 結構指標 的各個組成部分
    cout << "Year: "<< 1900 + ltm->tm_year << endl;     // 印出 Year: 2018
    cout << "Month: : "<< 1 + ltm->tm_mon<< endl;       // 印出 Month: : 4
    cout << "Day: : "<<  ltm->tm_mday << endl;          // 印出 Day: : 16
    cout << "Time: "<< ltm->tm_hour << ":";             // 印出 Time: 19:10:5
    cout << ltm->tm_min << ":";
    cout << ltm->tm_sec << endl;

    // 把 now 轉換為字串形式
    char* dt = ctime(&now);

    cout << "String-format Date and time: \n" << dt << endl;

    // 把 now 轉換為 tm 結構
    tm *gmtm = gmtime(&now);
    dt = asctime(gmtm);
    cout << "UTC Date and time: \n"<< dt << endl;
}

int main() {
    // 使用結構指標取得tm結構的組成部分
    time_by_struct_pointer();

    return 0;
}

