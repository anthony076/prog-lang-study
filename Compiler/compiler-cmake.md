## cmake 的使用

- why cmake
  - 開源工具，能更生成這種平台的構建系統配置文件，
    包含 Makefile、Ninja 以及 Visual Studio 等多種平台的配置文件

  - 提供各種便利的函數，可簡化配置文件的撰寫，例如，查找套件、創建文件、變數、執行命令等
  
  - 可自動產生用於 ide 代碼分析的配置文件，compiler_commands.json，用於各種跳轉、查找和自動補全等功能

## CMakeList.txt 的使用

- 注意，
  - 在函數的輸入參數中，`大寫英文字`通常是`關鍵字`，類似欄位名，用於標記與區分不同的數據
  - 若具有中括號的參數，例如，`tt(foo [NAME name])`，代表 [NAME name] 參數是可選參數，視情況添加

- 注意，若要用於特定的編譯目標，改用 target_ 開頭的函數，例如，

  | 用於特定編譯目標的函數       | 功能                   | 對應全局編譯目標的函數                      |
  | ---------------------------- | ---------------------- | ------------------------------------------- |
  | target_include_directories() | 為目標添加包含目錄     | include_directories()                       |
  | target_sources()             | 為目標添加源文件       | NA，在add_executable或add_library中直接指定 |
  | target_precompile_headers()  | 為目標設置預編譯頭文件 | NA                                          |
  | target_compile_definitions() | 為目標添加編譯器定義   | add_definitions()                           |
  | target_compile_options()     | 為目標添加編譯器選項   | add_compile_options()                       |
  | target_compile_features()    | 為目標指定編譯特性     | NA                                          |
  | target_link_libraries()      | 為目標添加鏈接庫       | NA，在add_executable或add_library中直接指定 |
  | target_link_options()        | 為目標添加鏈接器選項   | add_link_options()                          |

- 名詞 : 目標
  - 編譯目標 : 調用`編譯器命令`進行編譯後的產出，通常是`可執行檔或庫文件`
  - 建構目標 : 執行`非編譯器命令`，有可能產出`任意類型的文件`，也有可能`只是執行命令但無產出`

- 基本配置函數
  - `cmake_minimum_required()` : 指定 CMake 的最低版本要求，避免不支援的功能或語法
    - 語法，`cmake_minimum_required(VERSION cmake版本號)`
    - 範例，cmake_minimum_required(VERSION 3.10)

  - `project()` : 定義專案名稱和支援的程式語言
    - 語法，`project(自定義專案名 [LANGUAGES 語言1 語言2])`
    - 範例，project(MyProject LANGUAGES CXX C)
    - 執行後會在內部生成對應的變數，如 PROJECT_NAME

- 變數和資料操作相關函數
  - `set()` : 設定變數值，可用於配置、條件語句或函數參數
    - 語法，`set(變數名 變數值)`
    - 範例，
      ```
      set(foo 10)    # 建立變數 foo = 10
      message(STATUS "MY_VAR is ${MY_VAR}") # 打印變數值
      ```

  - `option()` : 設置快取變數，快取變數會保存在CMakeCache.txt
    - 基本概念
      - 什麼是快取變數 : 保存在CMakeCache.txt的永久變數值，稱為快取變數
      - set() 也可以建立快取變數，但必須利用CACHE關鍵字顯示聲明
        - 語法，set(變數名 變數值 CACHE 變數值 變數用途說明)
        - 例如，set(Foo ON CACHE BOOL "Generate documentation")
      - option()
        - 是對set()建立快取變數的重新封裝，用於`建立bool值`且`不需要顯示聲明為快取變數`
        - 優，第一次執行後，使用cmake-gui或命令行可以輕鬆更改快取變量，不需要手動修改CMakeCache.txt
    
    - 語法，`option(變數名 功能說明 ON或OFF)`

  - 同名變數的修改(覆寫)規則
    - 觀念，
      - 命令行的`-D 選項`可以在不修改 CMakeLists.txt 的情況下調整構建配置(修改變數)
      - 優先權，`FORCE > 命令行 > 普通變數 > 快取變數`，
      - 若有使用 FORCE 或 -D 的快取變數，才會`強制覆寫`CMakeCache.txt中的值

    - 規則，命令優先，
      `$ cmake -D變數名=變數值`，會覆蓋 set() 設置的`同名普通變數`，且該變數`不會`保存在快取中

    - 規則，命令優先，
      `$ cmake -D變數名=變數值`，會覆蓋 set() 設置的`同名快取變數`，且該變數`會`保存在快取中

    - 規則，命令優先，
      `$ cmake -D變數名=變數值`，會覆寫 cmake 內建的內部變數

    - 規則，CMakeList.txt 優先，
      若set(變數名 變數值 CACHE STRING "Description" `FORCE`)，則命令行設置的變數無效
    
    - 規則，沒有 FORCE 關鍵字，則 CMakeCache.txt 優先，
      若 set | option 建立和 CMakeCache.txt 一樣的同名快取變數，則 set(... CACHE ) 或 option 無效

    - 規則，有 FORCE 關鍵字，則 CMakeList.txt 優先，且更新 CMakeCache.txt
      若 set | option 建立和 CMakeCache.txt 一樣的同名快取變數，且有使用 FORCE 關鍵字，
      則 set | option 有效，且 CMakeCache.txt 中的同名快取變數值會被更新
    
    - 規則，普通變數優先，
      若 set() 設置與 CMakeCache.txt 同名的普通變數，則 set 有效，但不會改變CMakeCache.txt中的值

  - `list()` : 操作(列表變數)，例如添加元素或排序
    - 語法，

      ```shell
      list(
        [APPEND 列表變數名 添加元素1 添加元素2] # 在尾部插入元素
        [INSERT 列表變數名 插入位置 添加元素] # 指定位置插入元素
        [REMOVE_ITEM 列表變數名 移除元素1 移除元素2] # 刪除指定元素
        [REMOVE_AT 列表變數名 索引1 索引2] # 根據位置刪除元素
        [REMOVE_DUPLICATES 列表變數名] # 刪除重複元素
        [REVERSE 列表變數名] # 將元素順序反轉
        [LENGTH 列表變數名 儲存長度的變數] # 獲取元素個數並保存在指定變數中
        [SUBLIST 列表變數名 索引 個數 新列表變數名] # 從索引位置開始取指定個數的元素到新變數中
        [FIND 列表變數名 搜尋元素 結果變數] # 在查找指定元素並返回其索引到結果變數中
        [SORT 列表變數名] # 對列表中的元素進行排序（默認為字典順序）
      )
      ```
    - 範例，list(APPEND mylist foo bar)

  - `message()` : 輸出訊息到shell
    - 語法，`message([STATUS|WARNING|ERROR] "text")`，三種不同類型的訊息，打印時顏色上會有差異
    - 範例，message(STATUS "Hello World") 等效於 message("Hello World")

- 查找和依賴管理相關函數
  
  - `find_path()` : 在`預設路徑`或`指定的路徑`中尋找指定的檔案或目錄，並將搜尋結果保存在變數中
    - 語法，
      
      ```shell
      find_path(
        搜尋結果變數 
        NAMES 搜尋目標 
        [PATHS 搜尋路徑1 搜尋路徑2]
      )
      ```

      若沒有指定路徑，預設從以下路徑搜尋
      - 路徑1，使用者透過 CMAKE_INCLUDE_PATH 變數定義的路徑，
        例如，`set(CMAKE_INCLUDE_PATH "/custom/include/path")`

      - 路徑2，使用者透過 CMAKE_PREFIX_PATH 變數定義的路徑，
        例如，`set(CMAKE_PREFIX_PATH "/usr/local;/opt/custom")`，
        cmake 會在這些路徑中自動添加特定子目錄(include、lib、bin) 來查找

      - 路徑3，系統標準目錄
        - linux : /usr、/usr/local、/opt
        - windows : C:/Program Files、C:/Program Files (x86)
      
      - 路徑4，環境變數 PATH 中指定的路徑

    - 範例，
      - `find_path(MY_HEADER_PATH myheader.h)`
      - 在標準路徑中尋找 myheader.h，若找到，將myheader.h的目錄保存在變數MY_HEADER_PATH中
      - 等效於 shell 中的 $ find / -name "myheader.h"

  - `find_package()` : 查找外部庫或工具包，並加載其配置信息
    - 語法，

      ```shell
      find_package(
        套件名 [版本]
        [EXACT]     # 要求查找的套件版本必須精確匹配指定的版本。如果未找到指定版本就報錯
        [REQUIRED]  # 標記指定套件為必需項，未找到時，會立即終止並返回錯誤
        [QUIET]     # 標記指定套件為非必需項，未找到時，不會終止，也不會顯示錯誤或警告
        [NAMES]     # 指定包的別名列表，用於幫助 CMake 查找包時嘗試不同的名稱

        [COMPONENTS 子組件1 子組件2] # 指定需要的特定子模塊或組件，未找到時，會立即終止並返回錯誤
        [OPTIONAL_COMPONENTS 子組件1 子組件2] # 指定需要的特定子模塊或組件，未找到時，不會終止，也不會顯示錯誤或警告
        
        [NO_MODULE]  # 使用套件的配置文件(.cmake 文件)查找套件
        [MODULE]     # 使用Find<Package>.cmake 模塊查找套件
        [CONFIG]     # 只查找套件的配置文件來查找套件 （通常是 PackageConfig.cmake）
        
        [HINTS 搜尋路徑] # 指定提示路徑，先搜尋提示路徑後，才對標準路徑搜尋
        [PATHS 搜尋路徑] # 明確指定查找套件的路徑。這些路徑會在標準路徑搜索之後搜索

        [NO_DEFAULT_PATH] # 不要搜索任何預設的系統路徑，僅在顯式提供的路徑中查找套件
        [NO_CMAKE_PACKAGE_REGISTRY] # 不要使用 CMake 的套件登錄機制來查找套件
        [NO_CMAKE_BUILDS_PATH] # 不要在 CMake 自己生成的臨時構建路徑中查找套件
        [NO_CMAKE_SYSTEM_PATH] # 不要使用系統路徑來查找套件
        [NO_CMAKE_FIND_ROOT_PATH] # 不要使用 CMAKE_FIND_ROOT_PATH 來查找套件
      )
      ```

    - 範例，`find_package(Boost 1.70 REQUIRED COMPONENTS filesystem OPTIONAL_COMPONENTS system)`
      - 尋找 Boost v1.7 的套件，
      - 該套件是必要的，
      - 需要該套件的 filesystem 組件，若沒有就報錯，
      - 若該套件的 system 組件就納入，若沒有也不報錯

- 文件和檔案操作相關函數
  
  - `configure_file()` : 使用模板文件來產生新文件，或用於複製文件，常用於產生配置文件
    - 語法，`configure_file(輸入的模板文件 輸出的新文件 [COPYONLY])`
      - 若只需要複製成新檔案，可以添加 COPYONLY 關鍵字
      - 若模板文件中包含變數，cmake 會自動替換為變數值
  
    - 範例，替換模板文件中的變數
      ```
      set(VERSION "1.0")
      configure_file(config.h.in config.h)
      ```

      若 config.h.in 文件內容包含${VERSION}變數，
      cmake 將自動替換為 "1.0"後，再生成 config.h 的文件

      類似 shell-command 中的 sed 命令的使用

      ```shell
      sed 's/${VERSION}/1.0/g' config.h.in > config.h
      ```

  - `FILE()` : 用於執行多種文件操作
    - 語法，
      ```shell
      FILE(
        [READ 要讀取的檔案路徑 變數名] # 讀取檔案內容，並將結果放入變數中 
        [WRITE 要寫入的檔案路徑 要覆蓋的內容] # 將內容寫入到文件。如果文件不存在，則創建文件
        [APPEND 要寫入的檔案路徑 要插入的內容] # 將內容插入到文件。如果文件不存在，則創建文件
        [COPY 來源 DESTINATION 目的] # 複製檔案或目錄 
        [REMOVE 檔案或目錄路徑] # 刪除指定文件或目錄，如果文件不存在，不會報錯
        [MAKE_DIRECTORY 目錄路徑] # 創建指定目錄及其父目錄（如果不存在）
        [RENAME 舊名 新名] # 重命名文件或目錄
      )
      ```
    - 範例，
      - FILE(COPY /path/to/source DESTINATION /path/to/destination)
      - FILE(MAKE_DIRECTORY /path/to/directory)

- 編譯設置相關函數
  - `add_compile_options()` : 為所有編譯目標添加指定的標誌(flag)
    - 語法，`add_compile_options(要添加的flag)`
    - 注意，要為特定編譯目標添加指定的標誌(flag)，改用 target_compile_options()
    - 範例，add_compile_options(-Wall -Wextra -Wpedantic)
    - 範例，與 CMake 的生成器表達式一起使用
      - `add_compile_options($<BUILD_INTERFACE:-Wall -Wextra>)`，僅在構建該項目時應用 -Wall 和 -Wextra 選項
      - `add_compile_options($<$<CONFIG:Debug>:-g -O0>)`，僅在 Debug 配置下添加 -g 和 -O0 選項
  
  - `add_definitions()` : 添加編譯時的宏定義
    - 語法，`add_definitions(-D變數名=變數值)`
    - 範例，add_definitions(-DMY_MACRO=1)
      - 類似於 Makefile 中的，$ CFLAGS += -DMY_MACRO=1
      - 類似於 編譯命令中的，$ gcc -DMY_MACRO=1

  - `include_directories()` : 用於指定頭文件的搜尋目錄
    - 語法，`include_directories(搜尋路徑1 搜尋路徑2)`
    - 範例，include_directories(${CMAKE_SOURCE_DIR}/include)
      - 等效於 Makefile 中的 CFLAGS += -I./include
      - 等效於 shell 中的 gcc -I./include

- 執行和命令操作相關函數
  - `execute_process()` : 執行外部命令或腳本，並可捕獲其輸出到變數或檔案，用於進一步處理
    - 語法，
      
      ```shell
      execute_process(
        COMMAND 命令 命令參數
        [WORKING_DIRECTORY 指定工作目錄]  # 指定工作目錄
        [INPUT 輸入字串]  # 與 COMMAND 搭配使用，將輸入字串傳遞給 COMMAND
        [INPUT_FILE 檔案路徑]  # 與 COMMAND 搭配使用，將檔案傳遞給 COMMAND
        [OUTPUT_VARIABLE 接收輸出結果的變數]  # 將輸出結果保存到變數
        [OUTPUT_FILE 輸出檔案路徑] # 將輸出結果保存到檔案
        [ERROR_VARIABLE 接收錯誤訊息的變數]   # 將錯誤訊息保存到變數
        [ERROR_FILE 輸出檔案路徑]  # 將輸出結果保存到檔案
        [RESULT_VARIABLE 接收狀態碼的變數]  # 將狀態碼保存到變數
        [RESULTS_VARIABLE 接收所有結果的變數] # 將所有結果(標準輸出、錯誤輸出、返回碼)都儲存在變數中
        [TIMEOUT 超時時間(s)]   # 設置超時時間
        [OUTPUT_STRIP_TRAILING_WHITESPACE]  # 去掉捕獲的標準輸出末尾的空白字符
        [ERROR_STRIP_TRAILING_WHITESPACE]  # 去掉捕獲的標準錯誤輸出末尾的空白字符
      )
      ```

    - 範例，獲取 os 版本
  
      ```
      execute_process(
        COMMAND uname -r
        OUTPUT_VARIABLE os_version
        OUTPUT_STRIP_TRAILING_WHITESPACE)
      ```

      等效於 Makefile 中的，`$ os_version := $(shell uname -r)`

  - `add_custom_command()` : 功能1，設置生成文件的自定義命令，條件式觸發
    
    - 基本概念
      - 此函數在執行自定義命令後，一定會產生輸出文件
  
      - <font color=blue>此函數無法獨立存在，不會立刻被執行，也無法手動觸發，且 <font color=red>必須依賴於其他函數</font></font>

        例如，在 add_executable() 或 add_library() 或 add_custom_target()中，使用了add_custom_command()的輸出文件，
        代表 add_custom_command()的`輸出文件成為別的函數的依賴`時，add_custom_command() 才會有作用

      - cmake 遇到此函數時，會建立對輸出文件的監視，當
        - 輸出文件不存在時，會自動觸發命令執行
        - <font color=blue>輸出文件比依賴文件舊時</font>，會自動觸發命令執行，以更新輸出文件
        - 由於上述兩個規則，因此每次建構時此命令不一定會被執行，只有符合條件時才會被自動執行

    - 語法，

      ```shell
      add_custom_command(
          OUTPUT 輸出檔案1 輸出檔案2 ...
          COMMAND 產生輸出檔案的外部命令 命令參數1 命令參數2 ...
          [MAIN_DEPENDENCY 依賴檔案]  # 設置一個主要依賴項，命令執行前會更新依賴文件
          [DEPENDS <files>...]       # 設置多個依賴項，，命令執行前會更新依賴文件
          [BYPRODUCTS <files>...]    # 指定副生成物，可防止並行構建中的競態條件
          [WORKING_DIRECTORY 工作目錄路徑]
          [COMMENT 註解內容]  # 執行執行命令時，顯示的註釋或信息
          [VERBATIM]  # 以最直接的方式處理命令，避免對命令進行任何額外的解釋或修改，不進行任何轉義
      )
      ```

    - 範例，

      - 開始建構後，檢查 generated_file.cpp 是否存在，以及它是否比 foo.py 新
      - 如果 generated_file.cpp 不存在，或者比 foo.py 舊，將執行 python foo.py 命令。
      - 只有當 foo.py 發生變化時，才會重新執行這個命令

      ```shell
      # 注意，只有add_custom_command()並不會被執行，
      # 還要輸出文件 generated_file.cpp 被其他函數依賴，當符合條件時，才會自動被執行
      add_custom_command(
          OUTPUT generated_file.cpp
          COMMAND python foo.py
          DEPENDS foo.py
          COMMENT "Generating source code"
      )

      add_custom_target(generate_code DEPENDS ${generated_file.cpp})
      # 或者
      # add_executable(myapp main.cpp ${generated_file.cpp})
      ```

      等效於 Makefile 中的
      ```
      generated_file.cpp (input) : foo.py (depends)
        python foo.py (commands)
      ```

  - `add_custom_command()` : 功能2，為特定目標添加構建步驟
  
  - `add_custom_target()` : 自定義建構目標

    - 功能，用於執行外部的命令或腳本，可選擇自動觸發或使用者手動執行，不依賴於其他函數，通常用於執行一些不直接產生構建輸出的任務，
            例如，如運行測試、生成文檔、執行代碼格式化等
    - 語法，
      
      ```shell
      add_custom_target(
        自定義編譯目標名稱   # 或是生成 Makefile，可以透過`make 自定義目標名稱`來調用
        [ALL]              # 將此目標加入自動建構中，若沒有 ALL，使用者可以手動調用
        [COMMAND 要執行的命令 命令參數]  # 要執行的命令和其參數
        [DEPENDS <依賴目標列表>...]  # 指定當前目標的依賴項，需要等待依賴項完成才能執行當前目標
        [BYPRODUCTS <副產物列表>]    # 指定副生成物，可防止並行構建中的競態條件
        [WORKING_DIRECTORY 工作目錄] # 指定工作目錄
        [COMMENT <message>] # 執行自定義目標時顯示的註釋或信息
        [VERBATIM] # 以最直接的方式處理命令，避免對命令進行任何額外的解釋或修改，不進行任何轉義
        [COMMAND_EXPAND_LISTS]
        [USES_TERMINAL] # 以 Terminal 執行命令
        [SOURCES src1 [src2...]]
      )
      ```

    - 範例，
      
      ```
      add_custom_target(print_hello
          COMMAND ${CMAKE_COMMAND} -E echo "Hello, World!"
          COMMENT "Printing Hello World"
      )
      ```

- 建立編譯目標，會調用編譯器執行編譯命令的函數
  - `add_executable()` : 透過編譯器將源文件編譯為`可執行檔`
    - 語法
      
      ```shell
      add_executable(
        指定可執行文件的名稱  # 稍後可以通過 target_link_libraries() 等命令來鏈接庫或設置屬性
        [WIN32] # 生成一個具有 GUI 的應用程序
        [MACOSX_BUNDLE] # 在 macOS 上創建一個應用程序包
        [EXCLUDE_FROM_ALL] # 指名這個庫不會被自動包含在 ALL 目標中，使用者必須手動執行
        源文件1 源文件2 ...
      )
      ```
    - 範例，`$ add_executable(my_test_app EXCLUDE_FROM_ALL main.cpp)`

  - `add_library()` : 透過編譯器將源文件編譯為`連結檔`或`模組`
    - 語法
      
      ```shell
      add_library(
        指定庫的名稱
        [STATIC | SHARED | MODULE]  # 指定庫的類型，STATIC (.a .lib) SHARED (.so .dll .dylib)
        [EXCLUDE_FROM_ALL] # 指名這個庫不會被自動包含在 ALL 目標中，使用者必須手動執行
        源文件1 源文件2 ...
      )
      ```

    - 範例，`$ add_library(my_lib STATIC lib.cpp lib.h)`

- 其他
  - `add_dependencies()` : 用於指定目標(target)之間的依賴關係，目標包含建構目標(add_custom_target)和編譯目標(add_executable、add_library)都適用
    - 語法，`add_dependencies(具有依賴的target 被依賴的target)`，被依賴的target會先執行後，才會執行具有依賴的target
    - 範例

      ```shell
      # 創建一個自定義目標來生成代碼
      add_custom_target(generate_code
          COMMAND ${Python_EXECUTABLE} ${CMAKE_SOURCE_DIR}/scripts/generate_code.py
          WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
          COMMENT "Generating source code"
      )

      # 創建主應用程序
      add_executable(main_app main.cpp ${CMAKE_BINARY_DIR}/generated_code.cpp)

      # 確保在編譯main_app之前生成代碼
      add_dependencies(main_app generate_code)
      ```

  - `add_subdirectory(目錄名)` : 建立目錄

  - `mark_as_advanced(變量名)` : 將指定變量設置為高級變量
    - 將一些不常用或較為複雜的選項與基本選項分開
    - 透過`$ cmake -LA`可以查看高級選項
    - 隱藏不常用的選項，可以改善 CMake 配置過程的用戶體驗，或隱藏一些敏感或不應被隨意更改的設置

- 建立函數的語法

- 比較，add_custom_command | add_custom_target | execute_process 

  |            | add_custom_command                     | add_custom_target            | execute_process              |
  | ---------- | -------------------------------------- | ---------------------------- | ---------------------------- |
  | 使用者觸發 | 無法，必須由cmake依照條件觸發          | 使用者可以手動觸發或自動執行 | 立刻自動執行                 |
  | 使用場景   | 通常用於生成被依賴的文件，需要指定文件 | 用於執行編譯外的其他外部命令 | 執行外部命令，並取回結果使用 |
  | 限制       | 必須依賴於其他函數                     | NA                           | NA                           |

## CMakeList.txt 模組化

https://github.com/thejjw/cmake-kernel-module/tree/master

- 在專案中建立 cmake 目錄，將 CMakeList.txt 的代碼拆分的 *.cmake 檔案中，例如，`./cmake/FindFoo.cmake` 
- 在 CMakeList.txt 中添加以下
  
  ```cmake
  # 載入 cmake 目錄的內容 (將 cmake 目錄添加到 CMAKE_MODULE_PATH變數中，cmake 會自動載入內容)
  list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

  # 調用 FindFoo.cmake 的內容
  find_package(Foo) # 自動調用 FindFoo.cmake 的內容
  ```

## 常用的內建變數

- CMAKE_EXPORT_COMPILE_COMMANDS : 是否輸出 compile_commands.json 檔案，用於IDE的高階功能

- 項目與目錄變數
  - PROJECT_NAME：項目的名稱，使用 project() 命令定義
  - PROJECT_SOURCE_DIR：項目根目錄的絕對路徑，包含 CMakeLists.txt 文件的目錄
  - PROJECT_BINARY_DIR：項目編譯目錄的絕對路徑，生成文件的目錄
  - CMAKE_SOURCE_DIR：最頂層 CMakeLists.txt 所在的源文件目錄
  - CMAKE_BINARY_DIR：最頂層 CMakeLists.txt 的編譯目錄
  - CMAKE_CURRENT_SOURCE_DIR：當前處理的 CMakeLists.txt 文件的源文件目錄
  - CMAKE_CURRENT_BINARY_DIR：當前處理的 CMakeLists.txt 文件的編譯目錄

- 系統相關變數
  - CMAKE_SYSTEM_NAME：正在編譯的操作系統名稱，例如 Linux、Windows
  - CMAKE_SYSTEM_VERSION：操作系統的版本
  - CMAKE_HOST_SYSTEM_NAME：運行 CMake 時所在操作系統的名稱
  - CMAKE_HOST_SYSTEM_VERSION：運行 CMake 時所在操作系統的版本
  - CMAKE_SIZEOF_VOID_P：指針大小，表示系統是 32 位還是 64 位

- 編譯器相關變數
  - CMAKE_C_COMPILER：C 編譯器的路徑
  - CMAKE_CXX_COMPILER：C++ 編譯器的路徑
  - CMAKE_Fortran_COMPILER：Fortran 編譯器的路徑
  - CMAKE_C_COMPILER_ID：C 編譯器的 ID（如 GNU、Clang、MSVC）
  - CMAKE_CXX_COMPILER_ID：C++ 編譯器的 ID
  - CMAKE_CXX_STANDARD：指定 C++ 標準版本（如 11, 14, 17）
  - CMAKE_BUILD_TYPE：構建類型，常見值包括 Debug、Release、MinSizeRel 和 RelWithDebInfo

- 編譯與鏈接相關變數
  - CMAKE_C_FLAGS：C 編譯器的標誌
  - CMAKE_CXX_FLAGS：C++ 編譯器的標誌
  - CMAKE_EXE_LINKER_FLAGS：鏈接可執行文件時使用的標誌
  - CMAKE_SHARED_LINKER_FLAGS：鏈接共享庫時使用的標誌
  - CMAKE_MODULE_LINKER_FLAGS：鏈接模塊庫時使用的標誌

- 安裝相關變數
  - CMAKE_INSTALL_PREFIX：安裝的根目錄，默認為 /usr/local
  - CMAKE_INSTALL_BINDIR：可執行文件的安裝目錄，默認為 bin
  - CMAKE_INSTALL_LIBDIR：庫文件的安裝目錄，默認為 lib 或 lib64
  - CMAKE_INSTALL_INCLUDEDIR：頭文件的安裝目錄，默認為 include

- 檔案與路徑變數
  - CMAKE_SOURCE_DIR：最上層 CMakeLists.txt 文件所在的源代碼目錄
  - CMAKE_BINARY_DIR：最上層 CMakeLists.txt 文件所在的編譯目錄
  - CMAKE_CURRENT_SOURCE_DIR：當前 CMakeLists.txt 文件所在的源代碼目錄
  - CMAKE_CURRENT_BINARY_DIR：當前 CMakeLists.txt 文件所在的編譯目錄
  - CMAKE_ROOT：CMake 的安裝目錄

- 目標相關變數
  - CMAKE_RUNTIME_OUTPUT_DIRECTORY：可執行文件的輸出目錄
  - CMAKE_LIBRARY_OUTPUT_DIRECTORY：共享庫的輸出目錄
  - CMAKE_ARCHIVE_OUTPUT_DIRECTORY：靜態庫和檔案的輸出目錄

- 測試相關變數
  - CMAKE_CTEST_COMMAND：CMake 測試工具 ctest 的路徑
  - CMAKE_TESTING_ENABLED：是否啟用了測試

## cmake 命令的使用

- 注意，cmake 是自動產生建構系統的配置文件，在系統上仍然需要安裝建構系統，
  如 make、gcc、Visual Studio、... 等，來建構項目
  
- 根據 CMakeList.txt 產生特定編譯器的配置文件，和所有必要的文件，`$ cmake 包含CMakeList.txt的目錄路徑`
  - 若產生Makefile，利用 `$ make` 來建構

- 檢視快取變數，
  - 方法，`$ cat CMakeCache.txt`
  - 方法，在含有 CMakeCache.txt 的目錄中
    - `$ cmake -L .`，列出一般的快取變數，隱藏高級選項
    - `$ cmake -LA .`，列出所有的快取變數，含一般選項和高級選項
    - `$ cmake -LH .`，列出一般的快取變數，和選項的幫助訊息
    - `$ cmake --help-variable-list`，僅列出所有變數的名稱
    - `$ cmake --help-variable CMAKE_C_STANDARD`，打印出CMAKE_C_STANDARD的幫助訊息
  
## 範例，add_custom_command | add_custom_target | add_executable | add_dependencies

- add_custom_command，自定義額外的命令來產生必要的檔案
- add_custom_target，自定義建構目標，讓 add_custom_command 被依賴
- add_executable，實際的編譯目標
- add_dependencies，定義實際的編譯目標和自定義建構目標之間的依賴關係，add_custom_target 需要先執行

- 考慮以下的專案結構
  
  ```
  /my_project
    /src
      main.cpp
      generate_code.py    用於產生 generated_file.h 和 generated_file.c
    CMakeLists.txt
  ```

- 流程
  - main.cpp 中的 generated_file.h 和 generated_file.c 由 generate_code.py 產生
  - 編譯 main.cpp 前需要先執行 generate_code.py
  - 有了 generated_file.h 和 generated_file.c 後，將 main.cpp 編譯為執行檔

- 代碼 
  
  generate_code.py

  ```py
  # generate_code.py
  with open("generated_file.h", "w") as header_file:
      header_file.write("""
  #ifndef GENERATED_FILE_H
  #define GENERATED_FILE_H

  void generated_function();

  #endif // GENERATED_FILE_H
  """)

  with open("generated_file.cpp", "w") as source_file:
      source_file.write("""
  #include "generated_file.h"
  #include <iostream>

  void generated_function() {
      std::cout << "Generated function called!" << std::endl;
  }
  """)
  ```

  main.cpp

  ```cpp
  #include "generated_file.h"

  int main() {
      // 使用自生成的代碼
      generated_function();
      return 0;
  }
  ```

  CMakeList.txt

  ```shell
  cmake_minimum_required(VERSION 3.10)

  # 設置項目信息
  project(MyProject)

  # 設置 C++ 標準
  set(CMAKE_CXX_STANDARD 11)

  # 添加源文件
  set(SOURCE_FILES src/main.cpp)

  # 定義生成文件的自定義命令
  add_custom_command(
      OUTPUT ${CMAKE_BINARY_DIR}/generated_file.h ${CMAKE_BINARY_DIR}/generated_file.cpp
      COMMAND ${CMAKE_COMMAND} -E echo "Running code generation script"
      COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${CMAKE_SOURCE_DIR}/src ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/src/generate_code.py
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
      MAIN_DEPENDENCY ${CMAKE_SOURCE_DIR}/src/generate_code.py
      COMMENT "Generating source and header files"
      VERBATIM
  )

  # 定義一個自定義目標來生成文件
  add_custom_target(generate_files ALL
      DEPENDS ${CMAKE_BINARY_DIR}/generated_file.h ${CMAKE_BINARY_DIR}/generated_file.cpp
  )

  # 添加可執行文件，並將生成的文件包含進來
  add_executable(MyExecutable ${SOURCE_FILES} ${CMAKE_BINARY_DIR}/generated_file.cpp)

  # 確保在編譯 MyExecutable 之前生成文件
  add_dependencies(MyExecutable generate_files)
  ```

## ref

- option() 的使用
  - [CMake语法—选项（option）](https://www.cnblogs.com/Braveliu/p/15665143.html)
  - [CMake Day 7 —— option](https://azmddy.top/pages/742cf8/)