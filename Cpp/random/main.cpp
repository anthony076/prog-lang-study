#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

void fixed_int_random(){
    cout << "==== 每次都一樣的隨機數 ===="<< endl;

    int i,j;

    for( i = 0; i < 10; i++ )
    {
        // 產生隨機數
        j = rand();
        cout <<"Random Number: " << j << endl;
    }
}

void true_int_random(){
    cout << "==== 利用 srand()改變亂數的初始值 ===="<< endl;

    int i,j;

    // 利用 srand()改變亂數的初始值
    // 為了防止亂數的初始值每次重複，常常使用系統時間來初始化，即使用 time 函數來獲得系統時間
    srand( (unsigned)time(nullptr) );

    for( i = 0; i < 10; i++ )
    {
        // 產生隨機數
        j = rand();
        cout <<"Random Number: " << j << endl;
    }
}

int main() {
    // 每次都一樣的隨機數，注意，此方式每次執行後，得到的結果都是一樣的，
    // 因為它由上一個數值產生出下一個亂數，而一開始系統都是 0，所以每次產生出來的亂數才會一樣
    fixed_int_random();

    // 利用 srand()改變亂數的初始值
    true_int_random();

    return 0;
}