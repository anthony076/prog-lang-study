// ===== 屬性延遲加載 範例 =====
// #1.  快速建立class (省略整個主要建構函式)，類中的變量必須馬上賦值
//      若不想馬上賦值(延遲加載)，需在 var 之前加上 lateinit 關鍵字
// #2.  lateinit 關鍵字 只能用在 var 上
// #3.  延遲加載的變數，不能放在 init區塊中(init {})，否則會報錯
class lateinit_example {
    var a = 0

    lateinit var b: String

    val c :String by lazy { //val常量延迟加载
        println("lazy initial parameter e")
        "i am e"
    }


}
fun main(args: Array<String>) {
    // 屬性延遲加載 範例
    val example = lateinit_example()
    println(example.c)

    example.b = "aa"
    println(example.b)
}