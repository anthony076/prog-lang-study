/*
# 枚舉類 :
# 語法 : enum class 類名 { 枚舉常量1, 枚舉常量2, 枚舉常量3, ...}
    - 每一個枚舉常量都是一個對象(object),不需要實例化枚舉類就可直接調用成員
    - 枚舉常量可以被初始化，需透過構造函數
    - 枚舉類中可 1_修改構造函數和添加成員 2_宣告自己的匿名類和對應的方法

# 枚舉類的使用
    - 每個枚舉常量都包含兩個屬性：name（枚舉常量名）和ordinal（枚舉常量位置）
    - 提供了values()和valueOf()方法來檢測指定的名稱與枚舉類中定義的任何枚舉常量是否匹配
    - 透過枚舉類使用when就不需要加上else，因為枚舉類已涵蓋了所有的情況
*/

// ===== 枚舉常量初始化與調用 =====
// 枚舉常量是物件且可初始化，不需實例話就可直接調用
enum class Color(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF)
}

// ===== 在枚舉類中建立匿名類 =====
enum class ProtocolState {
    // 建立子類 WAITING
    WAITING {
        override fun signal() = TALKING
    },

    // 建立子類 TALKING
    TALKING {
        override fun signal() = WAITING
    };

    abstract fun signal(): ProtocolState
}

fun main(args: Array<String>) {
    // 枚舉常量初始化與調用
    println(Color.RED)  // return RED

    // 在枚舉類中建立匿名類
    println(ProtocolState.WAITING.signal())  // return TALKING

    // 使用枚舉常量
    println(Color.values())              // return [LColor;@27c170f0
    println(Color.valueOf("RED"))  // return RED

    var color:Color = Color.GREEN
    println(color.name)         // return GREEN
    println(color.ordinal)      // return 1
    println(color.rgb)          // retrun 65280

    // 枚舉類＋when範例
    println(getChinese(Color.BLUE))     // return 藍

}

// 透過枚舉類使用when就不需要加上else，因為枚舉類已涵蓋了所有的情況
fun getChinese(color: Color) =
        when(color){
            Color.GREEN -> "綠"
            Color.RED -> "紅"
            Color.BLUE -> "藍"
        }