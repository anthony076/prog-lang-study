/*
什麼是反射
    -   通常用在函數式編程中(高階函數)

        高階函數 : 將一般函數的 1_輸入為函數 或 2_返回為函數，則此函數稱為高階函數
        Ref:
            https://tw.saowen.com/a/196a9c029f05b27d75d32b3240151165d04d6e6b8b874a3df74d28a9f6352123

    -   在運行時，透過引用(::)來獲取類的函數, 屬性, 父類, 接口, metadata, 泛型, ...等, 類的內部訊息
        又稱為 RTTI(Run-Time Type Information)，運行時類型訊息，讓程序可以訪問、檢測和修改它本身狀態或行為的一種能力

引用的方式
    - 類引用
    - 函數引用
    - 屬性引用
    - 構造器引用
*/

// ===== 在高階函數中的函數引用 =====
// 建立一個簡單的判斷一個Int整數是否是奇數的函數
fun isOdd(x:Int) = x % 2 != 0

// ===== 屬性引用範例 =====
// 屬性引用的對象，必須是第一級對象(全域宣告的)
var one = 1

fun main(args: Array<String>) {

    println("===== 在高階函數中的函數引用 =====")
    val nums = listOf<Int>(1,2,3)

    // .filter (T) -> Boolean 其中
    // (T) 代表輸入一個函數， -> Boolean 代表該函數會返回 boolean 值
    // 因為.filter() 輸入為一個函數，因此使用函數引用
    val filteredNums = nums.filter(::isOdd)
    println(filteredNums)

    println("===== 屬性引用範例 =====")
    println(::one.get())
    ::one.set(100)
    println(one)

    println("===== 綁定函數(將函數引用賦值變數) =====")
    val digitRegex = "\\d+".toRegex()
    digitRegex.matches("7")
    digitRegex.matches("A")

    // 引用 digitRegex 對象實例的 matches 方法並賦值給變數
    val isDigit = digitRegex::matches
    isDigit("7")
    isDigit("A")
}


