/*
    ChangeLog :

    ＃1. This tutorial is referenced from below links:
         http://javasampleapproach.com/kotlin/kotlin-convert-object-tofrom-json-gson-kotlin-language

*/

import java.io.FileReader
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

// 用 data class 儲存 json 的鍵值
data class Person(val name:String, val age:Int, val msg:List<String>)

fun main(args: Array<String>) {
    // 將 json-format的字串轉為 object
    from_json()

    // 將 object 轉為 json
    to_json()

    // 多個 json-format的字串放在 List 中
    list_example()
}

fun from_json(){
    println("==== 將 json-format的字串轉為 object (from_json()) ====")

    // 建立 json-format 的字串
    val jsonString = """{"name": "Kolineer", "age": "26", "messages" : ["Master Kotlin","At JavaSampleApproach"]}"""
    val gson = Gson()

    // 將 json-format的字串 轉為 json object
    val person1:Person = gson.fromJson(jsonString, Person::class.java)
    println(person1)

    // 從檔案讀取json-format的字串後，再轉換為 json object
    // 注意，根目錄為 Project 目錄
    val person2:Person = gson.fromJson(FileReader("person.json"), Person::class.java)
    // 指定路徑的寫法
    //val person2:Person = gson.fromJson(FileReader("src/person.json"), Person::class.java)
    println(person2)
}

fun to_json(){
    println("==== 將 Object 轉為 json (to_json()) ====")

    // 建立 gson object
    // 因為要美化 json 列印出來的結果，所以用改用 GsonBuilder()
    // 若沒有此需求，使用 val gson = Gson() 即可
    val gson:Gson = GsonBuilder().setPrettyPrinting().create()

    // 利用 Person 類儲存相關資訊
    val person = Person("Kolineer", 27, listOf("I am Kotlin Learner", "At JavaSampleAproach"))

    // public String toJson(Object src)
    val jsonPerson: String = gson.toJson(person)

    println(jsonPerson)
}

fun list_example(){
    println("==== 多個 json-format的字串放在 List 中 ====")

    val jsonList = """
        [
            {"name": "Kolineer", "age": "26", "messages" : ["Master Kotlin","At JavaSampleApproach"]},
            {"name":"Kolineer Master","age":30,"messages":["I am Kotlin Master","still learning Kotlin at JavaSampleAproach"]}
        ]
        """

    val gson = GsonBuilder().setPrettyPrinting().create()

    println("List from JSON ：\n")

    // jsonList 是一個 List, 需要指定類型為 List<Person>
    // 透過 TypeToken<List<Person>>() {}.type 取得泛型參數類型
    // Refer : https://blog.csdn.net/fran_lee/article/details/75331837
    var personList: List<Person> = gson.fromJson(jsonList, object : TypeToken<List<Person>>() {}.type)

    personList.forEach { println(it) }

    println("List to JSON ：\n")

    val jsonPersonList: String = gson.toJson(personList)
    println(jsonPersonList)
}
