/*
使用前，請先具備泛型和對象(Object)的基本知識
    關於泛型，請參考 class_泛型.kt
    關於對象(Object)，請參考 class_object與靜態成員_匿名類的實例

# 概念，擴展就像外掛一樣，是原有物體的延伸，為原有物體添加新的能力，而不修改原有物體既有的元素
  不需要在類中實現所有的方法，而是透過擴展逐步實現需要的功能

# 和擴展函數一樣，擴展屬性並沒有實際的將屬性成員插入類中，因此擴展屬性不能有初始化器，
  擴展屬性的行為只能由顯式提供的getters() 和 setters()定義，直接賦值是無效的
  無效的擴展屬性範例 val Foo.bar = 1

# 類靜態成員的擴展
  class MyClass{
    // 透過 companion 建立靜態成員
    companion object{ }
  }

  為類中的靜態成員建立擴展
  fun MyClass.companion.foo() { ...}

  調用方式
  MyClass.foo()
*/

fun main(args: Array<String>) {
    
}
