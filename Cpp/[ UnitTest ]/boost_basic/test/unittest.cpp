
/*
Boost 的使用，請參考
https://www.boost.org/doc/libs/1_53_0/libs/test/doc/html/tutorials/hello-the-testing-world.html
*/

// 必要，Boost.Test 的進入點
#define BOOST_TEST_MAIN
#define BOOST_TEST_MODULE MyTest    //非必要


#include <boost/test/included/unit_test.hpp>

// 從 Library(libsrc.a) 導入要測試的函數
#include "../src/add.h"


BOOST_AUTO_TEST_CASE(test3add2){
    BOOST_CHECK(add(3,3) == 5);
}
