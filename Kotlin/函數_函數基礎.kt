/*
函數基礎

# 對於只有一行敘述的函數(只有一行回傳結果的敘述)，可使用單一表達式 (用 = 取代函數中的 { } )
# 具名參數可調換位置，但只要其中一個寫上參數名，其他的就必須寫上
# 函數宣告的基本語法
    修飾符 fun 函數(引數1:類型=預設值, 引數2:類型=預設值,...):返回值類型 { 程式碼 }

    其中，
        預設值，視情況添加，非必要，具有預設值的引述往後擺
        修飾符，視情況添加，非必要
        返回值，若 1_沒有返回值 或 2_返回值為 Unit類型，在宣告函數時可省略不寫(Unit類型代表不返回)

# 可變長度的使用
    在函數中，透過 vararg 代表該變數為 可變長度的變數
        fun example(vararg args:String){ ... }

    調用該函數時，透過 *變數名 代表帶入的變數為 可變長度的變數
        example(*args)

# 匿名函數的變數名稱就等同於函數名稱

# ::函數名 代表引用既有函數

*/

fun main(args: Array<String>) {

    // 一般函數和單一表達式的函數差異
    general_function(5)
    single_express(x=5, y=10.1)

    // 可變長度引數的範例一
    usage_of_vararg("aa","bb","cc")

    // 可變長度引數的範例二
    val args = arrayOf("a","b","c")
    usage_of_vararg(*args)      //使用 ＊變數名，代表此變數為可變長度變數

    // 匿名函數範例，匿名函數的變數名稱就等同於函數名稱
    anonymous_example()

    // :: 函數引用範例
    reference_funciton()
}


// 一般函數和單一表達式的函數差異
// 對於只有一行敘述的函數，可使用單一表達式 (用 = 取代函數中的 { } )
fun general_function(x:Int):Int { return x+2 }
fun single_express(x:Int,y:Double) = x + y


// 定義具有可變長度引數的函數
fun usage_of_vararg(vararg args:String){
    println("==== usage_of_vararg() ====")

    for (item in args){
        println("$item ")
    }
}

// 定義匿名函數
fun anonymous_example(){
    println("==== anonymous_example() ====")

    // 匿名函數的變數名稱就等同於函數名稱
    val sum = fun(x:Int, y:Int) : Int{
        return x + y
    }

    println(sum(5,10))
}

// 引用函數的範例
fun reference_funciton(){
    println("==== reference_funciton() ====")

    // 定義 isOdd()，用來判斷數字是否為偶數
    fun isOdd(x:Int):Boolean{
        return (x % 2) != 0
    }

    // 定義 isOdd()，用來判斷是否有包含特定字串
    fun isOdd(s:String):Boolean{
        return s == "anthony"
    }

    val numbers = listOf(1,2,3)
    println(numbers.filter(::isOdd))

    val strings = arrayOf("Hello", "anthony")
    println(strings.filter(::isOdd))

}