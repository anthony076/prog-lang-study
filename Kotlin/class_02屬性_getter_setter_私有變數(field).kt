/*
#1 屬性的基本概念
    - 類別 (class) 中的屬性是為物件的 私有變數的值(backing field)，一般物件的私有數值不允許直接存取，
      所以必須透過 getter() 或 setter() 的方法進行訪問，
      並在 geter() 或 setter() 中，使用field標示符來引用該私有數值的值
    - field標示符 只能用在屬性的訪問器中 (只能用在 getter() 或 setter() 中)
    - 在 class 中透過 var 或 val建立屬性時，會自動生成訪問器和一個私有變數(backing field)
      但透過 get() 或 set() 來自定義訪問器時，則視情況來決定是否會建立私有變數(backing field)
      在定義訪問器時，有定義預設值才會建立私有變數(backing field)

      範例一，有賦值會建立，會建立私有變數，並透過 field 關鍵字 將 0 寫到私有變數中
            var counter = 0 // 此初始器值直接写入到幕后字段
              set(value) {
                if (value >= 0)
                  field = value
              }

      範例二，沒有賦值，不會建立私有變數
            val isEmpty: Boolean
            get() = this.size == 0

#2 在 class 中自定義 setter()/getter() 的方法
    - 若要自定義該引數的 setter() 或 getter()，
      則該引數在宣告class時，不要透過 val/var 宣告，不讓kotlin為該引數自動添加 setter() 或 getter()
      讓該引數成為主要建構函式的參數就好，才有機會自行修改該引述的 setter() 或 getter()
    - 使用 getter() 和 setter()
      在變數宣告的下一行，加入 get(){...} 或 set(){...}

Ref:
    https://hltj.gitbooks.io/kotlin-reference-chinese/content/txt/properties.html
*/


// ===== 自定義 setter()/getter()的範例 =====
class sample (var id:Long, _title:String, _content:String=""){
    // 若類型可以被推斷出來，可使用下列寫法
    // val isEmpty g et() = this.size == 0
    var title = _title
        set(value:String) {
            if (value.isNotEmpty()){
                field = value   //field 是變數的默認值, 在getter()或setter()中才能訪問到
            }
        }

    var content = _content
        get() {
            return if(field.isEmpty()) "Empty" else field
        }

    fun getDetails() = "item01.id=${id}, title=${title}, content=${content}"
}

fun main(args: Array<String>) {
    // 自定義 setter()/getter()的範例
    val sample = sample(3344, "Niceday")
    sample.title = ""
    println("title=${sample.title}")

    sample.content= ""
    println("content=${sample.content}")

    sample.content= "aa"
    println("content=${sample.content}")
}
