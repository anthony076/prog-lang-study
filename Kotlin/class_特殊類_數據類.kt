/*
# 數據類 : 只保存數據的類
# 語法 : data class User(val name: String, val age: Int)
  標記 data 的數據類，會自動產生以下的類成員，不需要手動建立，在調用數據類的時候可直接使用
    - getter()/setter()
    - equals()/hashCode() 對；
    - toString()，格式是 "User(name=John, age=42)"；
    - componentN()，對應主構造函數的第N個參數
    - copy()，複製對象並改變它的部分屬性

# 數據類的使用要求
    - 主構造函數需要至少有一個參數
    - 所有參數(強制)(必需)要標記為 val 或 var，可放在 1_主構造函數 或 2_類體中
    - 數據類 "不能" 用抽象(abstract)、開放(open 可繼承)、密封(sealed)或者內部(inner)等修飾符修飾；
    - 在數據類中若顯示實現 equals() hashCode() toString()，就不會自動生成這些函數
    - 不允許顯式實現 componentN() copy()

# 數據類的解構，請看底下範例


*/

// 建立數據類，方法一
data class Book(val x:Int, val y:Int)

// 建立數據類，方法二
data class Book1(val x:Int){
    val y:Int = 0
}


fun main(args: Array<String>) {
    val b = Book(123, 456)
    println(b.x)
    println(b.toString())
    println(b.component1())
    println(b.component2())

    // data 的解構
    val (x,y) = b
    println("x:$x, y:$y")
}