#include <iostream>
#include <cstring>
using namespace std;

void c_style_string_exampleA(){
    cout << "===== 利用字元陣列建立字串，利用 { } =====" << endl;

    //  c style 建立字串，利用字元陣列建立字串
    // 建用{ } 建立陣列初始值
    // 注意，此方式要手動加上 '\0'，編譯器才知道是建立字串而不是字元
    char str[] = { 'h','e','l','l','o' ,'\0'};
    cout << str << endl;

    // 字元陣列不接受直接賦值
    //str = "hello"; 錯誤，字元陣列不接受直接賦值，儘可以在陣列初始化時以字串常數賦值

    // 字元陣列可以接受依陣列的方式賦值
    str[0]='H';
    str[1]='E';
    str[2]='L';
    str[3]='L';
    str[4]='O';

    // ===== 計算字串長度 =====
    // 方法一
    int SIZE = (sizeof(str)/sizeof(str[0]));
    cout << SIZE << endl;   // 印出 6

    // 計算字串長度，方法二
    cout << sizeof(str) << endl;    // 印出 6

    // ===== 印出字串中的字元 =====
    for(int i = 0; i <= SIZE; i++) {
        // 判斷是否為字串結束符號
        if(str[i] == '\0')
            cout << " null"<< endl;
        else
            cout << "" << str[i];
    }

}

void c_style_string_exampleB(){
    cout << "===== 利用字元陣列建立字串，利用等號 =====" << endl;

    //  c style 建立字串，利用字元陣列建立字串
    // 建用 = 建立陣列初始值
    // 注意，此方式要使用雙引號 ""，告知編譯器是建立字串
    char str[] = "hello";
    cout << str << endl;

    // 字元陣列不接受直接賦值
    //str = "hello"; 錯誤，字元陣列不接受直接賦值，儘可以在陣列初始化時以字串常數賦值

    // ===== 計算字串長度 =====
    // 方法一
    int SIZE = (sizeof(str)/sizeof(str[0]));
    cout << SIZE << endl;   // 印出 6

    // 計算字串長度，方法二
    cout << sizeof(str) << endl;    // 印出 6

    // ===== 印出字串中的字元 =====
    for(int i = 0; i <= SIZE; i++) {
        // 判斷是否為字串結束符號
        if(str[i] == '\0')
            cout << " null"<< endl;
        else
            cout << "" << str[i];
    }

}

void c_stdlib_string(){
    cout << "===== 利用標準庫 cstring 處理字串 =====" << endl;

    char str1[80] = {'\0'};
    char str2[] = "caterpillar";

    cout << "str1: " << str1 << endl
         << "str2: " << str2 << endl
         << endl;

    // strcpy : 複製字串
    cout << "strcpy" << endl;
    strcpy(str1, str2);
    cout << "str1: " << str1 << endl
         << "str2: " << str2 << endl
         << endl;

    // strcat : 字串串接
    cout << "strcat" << endl;
    strcat(str1, str2);
    cout << "str1: " << str1 << endl
         << "str2: " << str2 << endl
         << endl;

    // strlen : 字串串接
    cout << "strlen" << endl;
    cout << "str1 Length : " << strlen(str1) << endl
         << "str2 Length : " << strlen(str2) << endl
         << endl;

    // strcmp : 字串比較
    cout << "strcmp" << endl;
    cout << "compare str1 and str2: " << strcmp(str1, str2) << endl
         << endl;
}

int main() {
    //  利用字元陣列建立字串，利用 { }
    c_style_string_exampleA();

    //  利用字元陣列建立字串，利用  =
    c_style_string_exampleB();

    // 利用標準庫 cstring 處理字串
    c_stdlib_string();
}