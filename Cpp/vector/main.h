//
// Created by ChingHua Yang on 2018/5/9.
//

#ifndef VECTOR_MAIN_H
#define VECTOR_MAIN_H
    void array_to_vector();
    void add_element_example();
    void travel_by_forloop();
    void travel_by_iterator();
    void insert_element();
    void del_element();
    void empty_element();
    void sort_element();
    void reverse_element();
    void find_element();
    void release_vector();
    void release_object_in_pointorsVector();
    void memory_relate_example();
    void assign_example();

#endif //VECTOR_MAIN_H
