
## 編譯器，llvm (clang)

- [下載](https://github.com/llvm/llvm-project/releases)
  
- 安裝包中已經包含clang

## 編譯器環境介紹
- GNU 環境

  GNU是一個Unix-like的作業系統，經常與Linux核心搭配在一起，GNU/Linux 就是一般常說的 Linux作業系統，

- 在 windown 下執行 GNU 軟體，需要模擬GNU的環境，有以下幾種方式
  - MinGW (Minimalist GNU for Windows)
    
    MinGW用來編譯能在Windows作業系統中執行的程式的工具鏈(toolchain)，

  - MSYS / MSYS2
    
    和 MinGW 搭配的 console，稱為 MSYS (Minimal SYStem)，為 cygwin 的改良，
    並使用 pacman 作為包的管理系統

  - cygwin

    Cygwin模擬出POSIX環境，能夠編譯並運行大部分的Unix-like程式，
    缺點太過肥大，且編譯出的執行檔需要 cygwin.dll 才能執行

## 編譯器環境，MSYS2 (gcc)

- [下載](https://www.msys2.org/)

- 安裝後第一次進入 MSYS2 需要手動更新系統
  > pacman -Syu

- 安裝 GCC 相關工具
  > pacman -S --needed base-devel mingw-w64-x86_64-toolchain

- 進入  mingw-w64 GCC 的開發環境
  > 從 Start Menu 選擇 MSYS MinGW 64-bit 的終端

## 編譯器環境，minigw (gcc)

## 編譯管理，Bazel / Bazelisk
- Bazelisk 是 Bazel 的安裝和版本管理
- google 開源的多語言跨平臺建置系統
- 優，編譯快，採用 cache 和增量構建
- 優，採用 pythonic 的語法，後綴為 .bzl
- 優，方便復用組件，可以很方便地獲取第三方依賴
- 缺，太龐大
- 缺，對IDE不友好
- 缺，需要給 header 寫 cc_library規則

## 編譯管理，ninja



