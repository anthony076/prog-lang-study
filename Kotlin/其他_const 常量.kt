/*
常量有兩種
    - 運行時常量，編譯時 不 知道該常量的值，透過 val 關鍵字定義
      運行時常量，可透過反射修改其值
    - 編譯期常量，編譯時 已 知道該常量的值，透過 const + val 關鍵字定義
      使用編譯期常量可增加運行效率
*/

// 定義編譯期常量
const val a = 100

// 定義運行時常量
val b = 200

fun main(args: Array<String>) {
    println(a)
    println(b)
}