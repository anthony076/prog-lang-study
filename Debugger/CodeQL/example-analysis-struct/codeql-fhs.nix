# 建構，$ nix-build codeql-fhs.nix
# 進入環境，$ result/bin/codeql-fhs

{ pkgs ? import <nixpkgs> {} }:

pkgs.buildFHSEnv {
  name = "codeql-fhs";

  targetPkgs = pkgs: with pkgs; [
    codeql
    gcc
    gnumake
    glibc
  ];

  runScript = "bash";

  shellHook = ''
    export NIXPKGS_ALLOW_UNFREE=1
  '';
}