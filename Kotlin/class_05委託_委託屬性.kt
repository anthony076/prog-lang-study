/*
https://hltj.gitbooks.io/kotlin-reference-chinese/content/txt/delegated-properties.html#%E5%B1%80%E9%83%A8%E5%A7%94%E6%89%98%E5%B1%9E%E6%80%A7%E8%87%AA-11-%E8%B5%B7

閱讀前，請先具備屬性和委託的基本知識，請參考
    屬性，class_02屬性_getter_setter_私有變數(field).kt
    委託，class_委託(by)_繼承替代_功能組合.kt

# 什麼是委託屬性
    - 所謂的委託屬性，就是對其屬性值的操作不再依賴於其自身的getter()/setter()方法，是將其託付給一個代理類，
      從而每個使用類中的該屬性可以通過代理類統一管理，再也不用在每個類中，對其聲明重複的操作方法。
    - 類似自定義 getter()/setter()，但不是透過函數，而是透過一個代理類來自定義訪問器的內容

      http://blog.csdn.net/io_field/article/details/53374809

# 為什麼要用委託屬性 ()
    一般來說，有初始值的屬性必須在構造器內初始化完畢，並完成屬性的賦值，
    在某些況狀下，不是單純的賦值，想要自行處理 getter 和 setter 的行為，
    且想要每个使用类中的该属性可以通过代理类统一管理時使用(進階)，如以下三種狀況
    - 延迟属性（lazy properties）: 其值只在首次访问时计算；
    - 可观察属性（observable properties）: 监听器会收到有关此属性变更的通知；
    - 把多个属性储存在一个映射（map）中，而不是每个存在单独的字段中

# 委託屬性的使用方法，自定義委託類 來委託屬性
  - 語法，var 變數名：類型 by 被委託類名()
    當我們使用屬性的get()或set()時，會自動調用被委託屬性的類中的 getValue()和setValue()
    所以，被委託屬性的類中必須定義getValue()和setValue()且必須在fun 定義前加上 operator 關鍵字
    operator fun getValue() , operator fun setValue()

  - 委託類中的 operator fun getValue() 和 operator fun setValue() 可以是 1_該類的成員函數，或 2_擴展函數提供
    當需要委託屬性到 到原本未提供的這些函數的對象時，使用擴展函數提供getValue()和 setValue()會更為方便

# 委託屬性的使用方法，使用官方內建委託語法 來委託屬性
  kotlin 標準庫為幾種標準常見的委託提供了工廠方法，簡化使用者的調用
    - Lazy 延遲屬性
      語法，
        val 變數名: 類型 by lazy
      使用時機，
      - 委託給 lazy 的變數值，只有第一次會被計算，之後的調用都只返回最後的結果
      - 其值只在首次訪問時計算，在計算後其值不變，只能用在val 宣告，不能用在var 宣告

    - Observable 可觀察屬性
      語法，
        val 變數名: 類型 by Delegates.observable( 舊值 ) { prop, old, new -> }
      使用時機，
        在使用委託屬性時，會自動保留舊值 新值 和 Handler

# 委託進階
  請參考 https://hltj.gitbooks.io/kotlin-reference-chinese/content/txt/delegated-properties.html

  - 把屬性存在映射裡
  - 局部委託屬性
  - 透過擴展函數提供委託(利用擴展函數提供 getValue() 和 setValue() )範例
  - provideDelegate 操作符

Ref
    https://www.jianshu.com/p/40a7dc55a971
    https://www.kotlincn.net/docs/reference/classes.html
    用 Map 为你的属性做代理
    https://juejin.im/entry/596c2d4b6fb9a06bb874a3be
*/

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

// ===== 自定義委託類 =====
// 建立被委託類，用來委託屬性，必要提供屬性 operator fun getValue() 和 operator fun setValue()

// getValue() 和 setValue() 參數說明:
//      thisRef: 必須與 屬性所有者 類型（對於擴展屬性——指被擴展的類型）相同或者是它的超類型；
//      property: 必須是類型 KProperty<*> 或其父類型
class Delegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, thank you for delegating '${property.name}' to me!"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value has been assigned to '${property.name}' in $thisRef.")
    }
}

class Delegate_property{
    // 將屬性p委託給 Delegate類
    // Delegate 是個類，需要實體化，所以變成 Delegate()
    var p: String by Delegate()
}

// ===== lazy 使用範例 =====
// lazy 只能用於 val 宣告
// 只有第一次初始化時會印出 Computed，之後的調用都只印出 Hello
val lazyValue:String by lazy {
    println("Computed")
    "Hello"
}

// ===== Observable 使用範例 =====
class User{
    var name: String by Delegates.observable("no name"){
        prop, old, new ->
        println("=".repeat(30))
        println("$prop" )
        println("$old -> $new" )
    }
}

fun main(args: Array<String>) {
    // 自定義委託類範例
    Delegate_example()

    // lazy 使用範例
    println("===== lazy 使用範例 =====")
    println(lazyValue)
    println(lazyValue)

    // Observable 使用範例
    val user = User()
    user.name = "first"
    user.name = "second"
}

fun Delegate_example(){
    val d = Delegate_property()
    println(d.p)
}

