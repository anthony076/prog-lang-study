
#include "gtest/gtest.h"
#include "../main.h"

TEST(fooTest, eq){
    EXPECT_EQ(1,1);
}

TEST(fooTest,TrueFalse){
    EXPECT_TRUE(3>0);
}

TEST(caseName, testName){
    EXPECT_EQ(3, add(1,2));
}


GTEST_API_ int main(int argc, char** argv){
    testing::InitGoogleTest(&argc,argv);
    return RUN_ALL_TESTS();
}

