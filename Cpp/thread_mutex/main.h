//
// Created by ChingHua Yang on 2018/5/2.
//

#ifndef THREAD_MUTEX_MAIN_H
#define THREAD_MUTEX_MAIN_H
    void issue();
    void manual_mutex();
    void mutex_with_lockguard();
    void recursive_mutex_example();
#endif //THREAD_MUTEX_MAIN_H
