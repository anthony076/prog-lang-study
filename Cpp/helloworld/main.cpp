
// 導入標準庫 c++ 標準庫 cstdio，for printf
#include <cstdio>

/*
#1  引數 int argc, const char * argv[] 是為了讀取執行時的參數，非必要，可不加
    int main(int argc, const char * argv[])
#2  main() 使入口函數，必須要有
#3  宣告函式的基本語法
    返回類型 函數名() {...}
 */
int main()
{
    printf("Hello Anthony");
}

