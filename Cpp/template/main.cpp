#include <iostream>
#include "main.h"

using namespace std;

// ==== 函數模板範例 ====
template <typename T>
T aa(T x, T y){
    return (x>y) ? x : y;
}

// ==== 類模板範例 ====
template <typename T, typename R>
class Compare{

private:
    T _a;
    R _b;

public:
    // 構造函數
    Compare(T a, R b):_a(a), _b(b) { };

    // 建立成員函數 getA()
    T getA(){ return _a; }

    R getB();

    // 建立成員函數 getB()
    // 改在外部定義類模板的成員函數
    //R getB(){ return _b; }        //在內部定義類模板的成員函數的方式
};

// 改在外部定義類模板的成員函數
template <typename T, typename R>     // 照抄模板宣告
R Compare<T,R>::getB() {
    return _b;
}

// ==== 在一般類中使用函數模板 ====
class Printer{
public:
    template <typename T>
    void print(const T& t){
        cout << t << endl;
        cout << *t << endl;
    }
};

// ==== 在類模板中的函數模板 ====
template <typename T>
class Scanner{
    private:
        T t;
    public:
        // 構造函數
        Scanner(const T& param):t(param){ };

        template <typename R>
        void getTplusR(const R& r);
};

// 同時使用函數模板和類模板，需要有兩層的 template 說明，需要照順序加
// 第一層 template <typename T>，先將類模板的泛型代碼加進來
// 第一層 template <typename R>，再將函數模板的泛型代碼加進來
template <typename T>
template <typename R>
void Scanner<T>::getTplusR(const R &r) {
    cout << t + r << endl;
}

// ==== 類模板中的靜態成員 ====
template<typename T>
class Person{
    public:
        Person(T Age):age(Age){ }
        //  定義靜態成員
        static int tall;
    private:
        int age;
};

// 靜態成員需要在外部建立初始值
template <typename T>
int Person<T>::tall = 170;

// ==== 限定模板類型 ====
// 模板一，未使用特化的模板
template <typename T>
class S{
public:
        void info(){
            cout << "In base template\n" << endl;
        }
};

// 模板二，偏特化，限定使用指針類型的模板，雖然是限定指針，但指針可以指向任何類型
// 模板宣告中，宣告為 T*，代表限定指針(*)但不限定指針指向的類型(T)
template<typename T>
class S<T*> {
public:
    void info(){
        cout << "In pointer template\n" << endl;
    }
};

// 模板三，偏特化，函數特化模板
template<typename T, typename U>
class S<T(U)> {
public:
    void info(){
        cout << "In function template\n" << endl;
    }
};

// 模版四，特化，限定使用某一種類型的模板，此例限定使用 int
// 特化就不需要在模板宣告中使用泛型代碼，直接在類名後方，使用< >來宣告要限定的類型
template<>
class S<int> {
public:
    void info(){
        cout << "In int template\n" << endl;
    }
};

// ==== 主程式 ====
int main(int argc, const char * argv[])
{
    // 函數模板範例
    function_template_example();

    // 類模板範例
    class_template_example();

    // 在一般類中使用函數模板
    function_template_in_general_class();

    // 在類模板中的函數模板
    function_template_in_class_template();

    // 類模板中的靜態成員
    static_in_class_template();

    // 限定模板類型
    template_specialization();
}

void function_template_example(){
    cout << "==== 函數模板範例 ====" << endl;

    int i1=5, i2=10;
    //double d1=1.1, d2=2.2;

    // 調用方法一，宣告引數類型後再帶入函數模板中
    cout << aa(i1,i2) << endl;
    // 調用方法二，在函數模板名中，透過 < > 帶入實際的類型
    cout << aa<double>(1.1, 2.2) << endl;

}

void class_template_example(){
    cout << "==== 類模板範例 ====" << endl;

    // 正確調用類模板用法，在類模板名中，透過 < > 帶入實際的類型
    Compare<int,int> c3(55,66);
    cout << c3.getA() << endl;
    cout << c3.getB() << endl;

    // 讓編譯器使用自動類型推斷，有風險
    Compare c1(5,5.5);
    cout << c1.getA() << endl;
    cout << c1.getB() << endl;

    Compare c2(3.3,"Hello");
    cout << c2.getA() << endl;
    cout << c2.getB() << endl;
}

void function_template_in_general_class(){
    cout << "==== 在一般類中使用函數模板 ====" << endl;

    Printer p;
    p.print<const char*>("abc");

    int a = 123;
    p.print<const int*>(&a);
}

void function_template_in_class_template(){
    cout << "==== 在類模板中的函數模板 ====" << endl;

    Scanner<int> s(10);
    s.getTplusR(100);       // 印出 110
}

void static_in_class_template(){
    cout << "==== 類模板中的靜態成員 ====" << endl;

    // 同一個int模板建立兩個實例，
    Person<int> p1(10);
    Person<int> p2(100);
    // 兩個int模板實例的靜態成員的初始值都是 170
    cout << "p1.tall: " << p1.tall << endl;    // 印出 170
    cout << "p2.tall: " << p2.tall << endl;    // 印出 170

    // 建立double模板
    Person<double> pd(1.1);
    //double模板實例的初始值也是 170
    cout << "pd.tall: " << pd.tall << endl;    // 印出 170

    // 將int模板的靜態成員改為 500
    p1.tall = 500;
    cout << "p1.tall: " << p1.tall << endl;    // 印出 500
    cout << "p2.tall: " << p2.tall << endl;    // p1 和 p2 都是同一個模板造出來的類，具有相同的靜態成員，因此 p2.tall = 500
    cout << "pd.tall: " << pd.tall << endl;    // pd 和 p1.p2 是不同模板造出來的類，不具有相同的靜態成員，因此 pd.tall 仍然維持170
}

void template_specialization(){
    cout << "==== 限定模板類型 ====" << endl;

    S<float> s1;    //使用一般模板
    s1.info();      //印出 In base template

    S<float*> s2;   //使用指針限定的偏特化模板
    s2.info();      //印出 In pointer template

    // 需要在事先將函數宣告在全局
    S<decltype(func)> s3;       //使用函數限定的偏特化模板
    s3.info();                  //印出 In function template

    S<int> s4;      //使用int限定的特化模板
    s4.info();      //印出 In int template

}


int func(int i)
{
    return 2*i;
}