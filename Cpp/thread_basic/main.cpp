
/*
本範例使用 Ｃ＋＋ STL 中的 Thread Library

#1  在建立 thread 對象時，傳入的 function 是以複製的方式傳遞，即變數都是獨立的
    若要共用變數，需要透過傳遞 reference 的方式來共享變數

Ref:
    C++ 的多執行序程式開發 Thread：基本使用
    https://kheresy.wordpress.com/2012/07/06/multi-thread-programming-in-c-thread-p1/
*/

#include <iostream>
#include <thread>
#include "main.h"

using namespace std;
using namespace std::this_thread;

int sharedData = 0;

void sumA(){
    double dSum = 0;
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            cout << "calculating..." << i << j<< endl;
            dSum += i*j;
        }
    }

    cout << "dSum: " << dSum << endl;
}

void sumB(int i){
    for (int i=0;i<5;i++){
        sleep_for(chrono::duration<int, std::milli>(500));
        cout << "i: " << i << endl;
    }
}

class funObj{
public:
    // shared properties
    int iData;

    // 構造函數
    funObj(){
        iData = 0;
    }

    // 重載操作符()
    void operator()(){
        ++iData;
    }
};

int main(int argc, const char * argv[])
{
    // 透過 thread 執行無參數函數
    thread_without_paras();

    // 透過 thread 執行有參數函數
    thread_with_paras();

    // 透過自訂class並重載運算符，達到在不同 thread 中共享變數的目的
    shared_variable_in_differentThread();

}

void thread_without_paras(){

    cout << "==== 透過 thread 執行無參數函數 ====" << endl;

    // 建立 thread 對象，並帶入要執行的函數
    thread threadA(sumA);

    cout << "Main thread is not blocking..." << endl;

    // 讓 main-thread 等待 threadA 結束後再結束
    // 若沒有此行，Ｍain thread 結束後，會一起把 threadA 結束
    threadA.join();
}

void thread_with_paras(){

    cout << "==== 透過 thread 執行有參數函數 ====" << endl;

    // 建立 thread 對象，並帶入要執行的函數
    thread threadA(sumB,10);

    // 讓 main-thread 等待 threadA 結束後再結束
    // 若沒有此行，Ｍain thread 結束後，會一起把 threadA 結束
    threadA.join();
}

void shared_variable_in_differentThread(){
    cout << "==== 透過自訂class並重載運算符，達到在不同 thread 中共享變數的目的 ====" << endl;

    funObj co;
    // 因為傳入的函數是用複製的，因此，co.iData = 0
    thread t1(co);
    t1.join();
    cout << co.iData << endl;

    // 取得 co 對象的 reference 後，在傳遞給 t2
    // 執行的是 co 這個對象的參考，而不是被複製的對象
    thread t2(ref(co));
    t2.join();
    cout << co.iData << endl;

}
