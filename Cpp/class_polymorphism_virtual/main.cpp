#include <iostream>
#include "main.h"

using namespace std;

// ==== 多型對同名函數造成的問題 ====

// 建立基類 Shape
class Shape {
protected:
    int width, height;

public:
    // 構造函數
    Shape( int w=0, int h=0){
        width = w;
        height = h;
    }

    /*
    不使用virtual將同名函數宣告為動態綁定類型的寫法
    會讓使用多型的同名函數，永遠只執行父類的同名函數
    intarea(){
    cout<<"area()fromShapeclass(parent)"<<endl;
    return0;
    }
    */

    //對於多型同名函數的正確宣告方式，使用virtual建立虛函數
    //使用virtual將同名函數宣告為動態綁定，即類型再賦值時決定
    //讓編譯器可以正確調用子類的同名方法
    virtual int area(){
        cout<< "area() from Shape class(parent)" << endl;
        return 0;
    }

};

// 從Shape類延伸出子類 Rectangle
class Rectangle : public Shape {
public:
    // 構造函數
    Rectangle( int w=0, int h=0):Shape(w,h){ }

    int area(){
        cout << "area() from Rectangle class (son)" << endl;
        return 0;
    }
};

// ==== 虛函數表範例 ====

class base{
public:
    virtual void v1(){ }
    virtual void v2(){ }

};

class derived : public base{
public:
    void v1(){ }
    void v2(){ }
};


int main(int argc, const char * argv[])
{
    // 多型對同名函數造成的問題
    polymorphism_issue();

    // 虛函數表範例
    vtable_example();

}

void polymorphism_issue(){
    cout << "==== 多型對同名函數造成的問題 ====" << endl;

    // 建立指向父類Shape的指針 pshape
    Shape *pshape;

    // 建立 子類對象實例 rec
    Rectangle rec(10,7);

    // 將父類指針pshape 指向子類對象的記憶體位址
    pshape = &rec;
    // 利用父類指針pshape執行area()方法
    pshape->area();     // 印出子類方法 area() from Rectangle class (son)
}

void vtable_example(){
    cout << "==== 虛函數表範例 ====" << endl;

    base b, *p;
    derived d;

    // 將父類指針指向父類對象
    p = &b;
    p->v1();
    p->v1();

    // 將父類指針指向子類對象
    p = &d;
    p->v1();
    p->v2();


}
