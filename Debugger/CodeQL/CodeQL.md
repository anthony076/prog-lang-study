## CodeQL 靜態代碼分析器

- 用途 : 用於尋找 bug 和漏洞，支援 c/cpp、python、javascript、java 等語言

  將源碼的內容轉換為數據庫內容，包含類名、函數、變量、表達式等訊息保存在數據庫中，
  透過ＱＬ的語法快速查詢和定位有問題的代碼

## 安裝 CodeQL

- nixos 

  - codeql 包含 UNFREE 的證書，需要透過 NIXPKGS_ALLOW_UNFREE=1 臨時開放權限
  - 此方法只適合臨時使用，要長期使用，需要建立fhs環境，參考，[在nixos上測試codeql](#範例-在-nixos-上測試-codeql)

  ```
  $ export NIXPKGS_ALLOW_UNFREE=1 
  nix-shell -p gcc codeql
  ```

- codeql命令的配置文件，`qlpack.yml`，用於安裝(執行codeql的相關依賴)
  
  - 用於 c/cpp 的 qlpack.yml 範例 

    ```yml
    name: test
    dependencies:
      codeql/cpp-all: '*'
    ```
  
  - 透過命令安裝依賴，`$ codeql pack install`

## CodeQL 命令的使用

- 創建(初始化) codeQL數據庫，
  
  > codeql database create --language=cpp --command=make output_db
  
  其中
  - `--language=cpp` : 創建用於cpp的 codeQL 數據庫
  
  - `--command=make` : 編譯源碼使用的命令，
    codeQL會調用該命令編譯源碼，並將代碼和相關訊息保存在數據庫中
    
  - `output_db` : 指定 codeQL數據庫的存放路徑

- 執行 ql 檔案中定義的查詢

  將查詢的結果會顯示在 terminal 中
  - 不指定輸出格式，`$ codeql query run test.ql --database=output_db`
  - 指定輸出格式為csv，`$ codeql query run test.ql --database=output_db --format=csv`

  將查詢的結果輸出為json，並透過 jq 命令檢視
  - 執行查詢，並將結果輸出為 bqrs，`$ codeql query run test.ql --database=output_db --output=result.bqrs`
  - 將 bqrs 轉換為 json 並寫入檔案，`$ codeql bqrs decode --format=json result.bqrs > result.json`
  - 利用 jq 檢視 json 內容，`$ cat result.json | jq .`

- 注意，
  - 為避免增量編譯只編譯有變化的代碼，編譯前，建議先做清理工作，例如，`$ make clean`

  - 若編譯系統`使用沙盒進行編譯`，例如 bazel，會導致 codeQL 無法注入而造成編譯失敗，
    並且無法建立代碼數據庫，此時，需要自行改寫編譯腳本

## vscode 插件的使用

- 注意，透過 codeql 命令，也可以使用 QL 語法進行查詢，若使用 vscode，推薦使用 codeQL 插件會比較方便查詢

- 安裝 [CodeQL for vscode](https://marketplace.visualstudio.com/items?itemName=GitHub.vscode-codeql)

- 在專案目錄下建立 CodeQL 的配置文件，./qlpack.yml
  
  ```yml
  name: test
  dependencies:
    codeql/cpp-all: '*'
  ```
  
  完成配置文件後，需要手動安裝依賴 codeql/cpp-all，
  - 在vscode中打開命令列，`<ctrl+shift+p>` ，輸入 `codeQL: Install Pack Dependencies`
  - 安裝完成後，重啟vscode
  - 編寫 *.ql 文件
  - 在 *.ql 中點右鍵，選擇 CodeQL: Run Query on Selected Database 進行查詢

## QL 語法

- 範例，利用 QL 語法進行代碼查詢
  
  例如，test.ql
  
  ```sql
  from 
    FunctionCall call, 
    Function func, 
    SizeofExprOperator sizeof,
    VariableAccess va
  where 
    call.getTarget() = func and
    func.getName() = "malloc" and
    call.getArgument(0) = sizeof and
    sizeof.getExprOperand() = va and
    va.getTarget().getType() instanceof PointerType
  select sizeof
  ```

## [範例] 在 nixos 上測試 codeql

- [完整範例](example-analysis-struct/)

- step，建立 fhs 環境
  - 建立 [codeql-fhs.nix](example-analysis-struct/codeql-fhs.nix)
  - 建構，$ nix-build codeql-fhs.nix
  - 進入環境，$ result/bin/codeql-fhs

- step，建立[測試源碼](example-analysis-struct/size_of_ptr.c)

- step，建立[Makefile](example-analysis-struct/Makefile)
  - 編譯源碼，`$ gcc -c -o size_of_ptr.o size_of_ptr.c`
  - 用於清理的命令(make clean)，`$ rm -rf *.o output_db`
  - 用於建立 codeql 數據庫的命令，`$ codeql database create --language=cpp --command=make output_db`

- step，建立codeql的配置文件，[qlpack.yml](example-analysis-struct/qlpack.yml)

- step，建立用於查詢的ql檔案，[test.ql](example-analysis-struct/test.ql)

- step，執行查詢前的操作
  - 建立codeql數據庫，透過 make 命令建立，`$ make db`
  - 安裝codeql依賴，`$ codeql pack install`
  - 編譯並執行查詢，`$ codeql query run test.ql --database=output_db`
    - 此命令會在查詢前先執行 make clean 的清理工作
    - 並透過 codeql database create --language=cpp --command=make output_db 中的 --command=make 進行編譯
    - 編譯後會在結果添加到 codeQL 數據庫後，才會執行查詢

## [範例] 範例集

- [利用codeQL檢查代碼執行邏輯是否有錯誤，以指針為例](https://www.youtube.com/watch?v=NVhjxg4yIfE)

## ref

- [CodeQL @ github](https://github.com/github/codeql)

- [CodeQL系列教學影片](https://www.youtube.com/playlist?list=PLeYA2mrASg3HoSQmgdObuoEViHzlESrzC)

- [CodeQL的安裝和基本使用＋實際使用範例](https://www.youtube.com/watch?v=P8Un1Sbll0E&list=PLeYA2mrASg3HoSQmgdObuoEViHzlESrzC&index=1&t=87s)

- [ql範例 for cpp](https://github.com/github/codeql/tree/main/cpp/ql/examples/snippets) 