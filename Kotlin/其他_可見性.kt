
/*
可見性修飾符適用於 Package、類、類屬性、屬性的setter()、對象、接口、構造函數、一般函式

# 可用於 Packages 的可見性修飾符
    - private 只能在文件内部可见，不可被其他文件 import
    - internal 在同一個模組內可見
    - public全局可見 (預設值)

# 可用於 class 的可見性修飾符
    類的訪問修飾符有private，protected，internal，public，可見性逐步加大
    - private 私人僅本類可見
    - protect 保護本類及其子類
    - internal 內部模塊內部可見
    - public 全局可見

Ref:
https://www.kotlincn.net/docs/reference/visibility-modifiers.html#%E6%9E%84%E9%80%A0%E5%87%BD%E6%95%B0
*/

open class Outer {
    private val a = 1                      // 只有Outer内部可见
    protected open val b = 2         // 只有Outer内部和子类可见
    internal val c = 3                        // 模块内可见
    val d = 4  // public by default       // 全世界都可见

    protected class Nested {           // 内部类首次登场
        public val e: Int = 5          // 内部类在后面有介绍，这里先不管
    }
}

class Subclass : Outer() {            // 继承了 Outer 类
    // a is not visible
    // b, c and d are visible
    // Nested and e are visible

    override val b = 5   // 'b' is protected
}

class Unrelated(o: Outer) {          // 构造器形参写了 Outer 类的 o 对象
    // o.a, o.b are not visible
    // o.c and o.d are visible (same module)
    // Outer.Nested is not visible, and Nested::e is not visible either
}

