
/*
new 和 malloc 的差異
malloc()    請求記憶體空間
new         請求記憶體空間，並建立對象

# new 的使用方式
    1   一定要透過指針變數
    2   指針變數 = new 類型

*/

#include <iostream>

using namespace std;

void new_delete_example(){
    cout << "==== 一般數值類型的動態分配 ====" << endl;

    // 建立指向int的指針變數
    int *pint = nullptr;

    // 為指針變數請求內存空間，並建立對象
    pint = new int;

    cout << "size of *pint: " <<sizeof(*pint) << endl;

    delete pint;

    // 建立指向double的指針變數
    double *pdouble = nullptr;

    // 為指針變數請求內存空間，並建立對象
    pdouble = new double;

    *pdouble = 29494.49;
    cout << "address of pdouble: " << &pdouble <<endl;
    cout << "size of *pdouble: " << sizeof(*pdouble) << endl;
    cout << "value of pdouble: " << *pdouble << endl;

    delete [] pdouble;
}

void dynamic_memory_of_1dArray(){
    cout << "==== 一維數組的動態內存分配 ====" << endl;

    // 建立指向array的指針變量
    int *parray = nullptr;

    // 為 array 申請空間，共 4個 byte 長
    parray = new int[3];

    // 賦值
    for(int i = 0; i < sizeof(*parray); i++){
        parray[i] = i + 100;
    }

    // 檢查
    for(int i = 0; i < sizeof(parray); i++){
        cout << parray[i] << endl;
    }

    delete parray;
}

void dynamic_memory_of_2dArray(){
    cout << "==== 二維數組的動態內存分配 ====" << endl;

    // 若二維陣列的第一維長度(m)為2，第二維長度(n)為5
    int m=2, n=5;
    int **pparr = nullptr;

    // ==== 申請內存 ====
    // 申請外層內存
    pparr = new int *[m];

    // 申請內層內存
    for (int i=0; i<m ; i++){
        pparr [i] = new int [n];
        cout << "index: " << i << endl;
    }

    // ==== 賦值 ====
    for (int i=0; i<m; i++){
        for (int j=0;j<n;j++){
            pparr[i][j] = j*i;
        }
    }

    // ==== 檢查 ====
    for (int i=0; i<m; i++){
        for (int j=0;j<n;j++){
            cout << pparr[i][j] << endl;
        }
    }

    // ==== 釋放內存 ====
    // 先釋放內層array中的元素
    for(int i=0; i<m; i++){
        delete [] pparr[i];
    }

    // 再釋放外層array中的元素
    delete [] pparr;
}

int main(int argc, const char * argv[])
{
    // 一般數值類型的動態分配
    new_delete_example();

    // 一維數組的動態內存分配;
    dynamic_memory_of_1dArray();

    // 二維數組的動態內存分配
    dynamic_memory_of_2dArray();

    // 三維數組的動態內存分配
    // http://www.runoob.com/cplusplus/cpp-dynamic-memory.html

}
