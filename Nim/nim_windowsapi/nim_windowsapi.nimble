# Package

version       = "0.1.0"
author        = "chinghua0731"
description   = "nim gui by windows api"
license       = "MIT"
srcDir        = "src"
bin           = @["nim_windowsapi"]

# Dependencies

requires "nim >= 0.18.0"
requires "oldwinapi >= 2.1.0"
