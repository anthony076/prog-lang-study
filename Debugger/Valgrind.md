## Valgrind 的使用

- Valgrind 不算是 debugger 但是常和 debugger 一起搭配使用，提供更多記憶體使用的細節
  
- Valgrind用於應用程式，並提供以下功能的檢測報告
  - 檢測應用程式的記憶體使用狀況
  - 檢測的記憶體洩漏
  - 是否有無效的內存訪問
  - 多線程的競賽條件
  - Cache 使用問題
  - 提供 graphical 的報告

## ref

- [用valgrind查看自己的代碼內存使用量](https://www.youtube.com/watch?v=kjkrpgWr9G4)