/*
#1  mutex 互斥鎖，用來控制 thread 之間的順序，有以下四種
    mutex                   手工鎖
    timed_mutex             時間鎖
    recursive_mutex         遞歸鎖
    recursive_timed_mutex   遞歸時間鎖

#2  互斥鎖操作流程
	在要執行的函數中放置互斥鎖，當線程執行到該互斥鎖時，會去檢查目前鎖的狀態，

	a.若鎖頭被鎖住，就會將線程掛起並進行等待，直到鎖頭釋放
	b.若鎖頭未被鎖住，就會繼續執行，並將鎖頭上鎖

	注意1，使用互斥鎖，必須手動去上鎖和解鎖，避免未解鎖造成線程一直被掛起
	注意2，使用.lock()和.unlock()當程式遇到return或發生錯誤時，有可能會遇到永久掛起的風險，
          可透過 lock_guard 或 unique_lock 來自動管理鎖的上鎖與解鎖的動作


#3  recursive_mutex 遞歸鎖
    讓 mutex 認得鎖住自己的執行序（同一個class），並且讓 mutex 在已經被鎖定的情況下，
    還是可以讓同一個執行序再去鎖定他、而不會被擋下來

#4  比 lock_guard 更彈性的 unique_lock
    https://kheresy.wordpress.com/2012/08/20/multi-thread-programming-in-c-thread-p3/

    -   unique_lock 不一定要擁有 mutex，所以可以透過 default constructor 建立出一個空的 unique_lock
    -   unique_lock 雖然一樣不可複製（non-copyable），但是它是可以轉移的。
        所以，unique_lock 不但可以被函式回傳，也可以放到 STL 的 container 裡。


Ref:
    https://kheresy.wordpress.com/2012/07/06/multi-thread-programming-in-c-thread-p1/
    https://kheresy.wordpress.com/2012/07/11/multi-thread-programming-in-c-thread-p2/
    https://kheresy.wordpress.com/2012/08/20/multi-thread-programming-in-c-thread-p3/
    https://askldjd.com/2009/10/26/prefer-simple-mutex-over-recursive-mutex/
    https://blog.csdn.net/sinat_22336563/article/details/71588703
*/

#include "main.h"
#include <iostream>

// For minigw on windows not support std::thread and std::mutex
#ifdef WINDOWS
#include "mingw-std-threads/mingw.mutex.h"
#include "mingw-std-threads/mingw.thread.h"
// For GCC on Linux or Mac support std::thread and std::mutex
#else
#include <thread>
#include <mutex>
#endif

using namespace std;

// 建立一個全域的互斥鎖
mutex gMutex;

// 沒有互斥鎖的函數 outputValueA()
void outputValueA(int n){
    for (int i =0;i<n;i++){
        this_thread::sleep_for(chrono::duration<int,std::milli>(5));
        cout << " " << i;
    }
    cout << endl;
}

// 有互斥鎖的函數 outputValueB()
void outputValueB(int n){
    /*
        在要執行的函數中放置互斥鎖，當線程執行到該互斥鎖時，
        會去檢查目前所的狀態，若鎖頭被鎖住，就會將線程掛起，直到鎖頭釋放
    */

    // 將互斥鎖的鎖頭鎖上
    gMutex.lock();

    for (int i =0;i<n;i++){
        this_thread::sleep_for(chrono::duration<int,std::milli>(5));
        cout << " " << i;
    }

    cout << endl;

    // 將互斥鎖的鎖頭釋放
    gMutex.unlock();
}

// 使用 lock_guard 自動管理互斥鎖
void outputValueC(int n){
    // 使用 lockguard 管理互斥或鎖就不需要手動 unlock
    lock_guard<mutex> mLock( gMutex );

    for (int i =0;i<n;i++){
        this_thread::sleep_for(chrono::duration<int,std::milli>(5));
        cout << " " << i;
    }

    cout << endl;
}

// 使用 recursive_mutex 的 class
class A{
private:
    // 建立遞歸鎖 rMutex 的對象
    recursive_mutex rMutex;

public:
    void function1(){
        // 建立 lock_guard
        // 透過 lock_guard 自動管理 recursive_mutex
        lock_guard<recursive_mutex> lock(rMutex);
        cout << "i am in function1()" << endl;
    }

    void function2(){
        // 建立 function2()的lock_guard
        // 透過 lock_guard 自動管理 recursive_mutex
        lock_guard<recursive_mutex> lock2(rMutex);
        cout << "i am in function2()" << endl;

        // 執行 function2()，實際上會將 rMutex 鎖上兩次 （function2 一次，function1 一次）
        function1();
    }
};

int main(int argc, const char * argv[])
{
    // 先後執行兩個線程造成的順序錯亂
    issue();

    // 手工鎖的使用
    manual_mutex();

    // 自動管理互斥或鎖
    mutex_with_lockguard();

    // 遞歸鎖使用範例
    recursive_mutex_example();
}

void issue(){
    cout << "==== 先後執行兩個線程造成的順序錯亂 ====" << endl;

    // normal function
    cout << "normal function: " << endl;
    outputValueA(3);    // 印出 0 1 2
    outputValueA(5);    // 印出 0 1 2 3 4

    cout << "thread function: " << endl;
    thread t1(outputValueA,3);
    thread t2(outputValueA,5);

    t1.join();
    t2.join();
}

void manual_mutex(){
    cout << "==== 手工鎖的使用 ====" << endl;

    thread t1(outputValueB,3);
    thread t2(outputValueB,5);

    t1.join();
    t2.join();
}

void mutex_with_lockguard(){
    cout << "==== 自動管理互斥或鎖 ====" << endl;

    thread t1(outputValueC,3);
    thread t2(outputValueC,5);

    t1.join();
    t2.join();
}

void recursive_mutex_example(){
    cout << "==== 遞歸鎖使用範例 ====" << endl;

    A a;
    a.function2();
    cout << "If you see this, mean recursive_mutex not in a infinite loop" << endl;

}