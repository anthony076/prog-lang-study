## [package] section
- 用於提供專案相關訊息
  
- `name`: 專案名稱

- `version`: 專案版本

- `authors`: 作者，可使用 [] 添加多個作者
  > authors = ["ching"]

- `license`: 授權
  - Apache-2.0
  - Apache-2.0/MIT
  - MIT
  - MIT/Apache-2.0
  - MPL-2.0
  - Unlicense/MIT

- `edition`: 版本

- `rust-version`: 指定 rust 的版本
  
- `publish`: 是否發佈

## [dependencies] section
- 用於添加專案必要的依賴
- 方法1，`只提供`依賴的版本訊息，直接指定版本
  ```toml
  # Cargo.toml
  ... 省略 ...

  [dependencies]
  gc = "0.0.1"
  ```

- 方法2，提供依賴的`多個相關訊息` 
  - `path參數`: 依賴的本地路徑或網路位址
  - `git參數`: 依賴庫位於 github 的位址
  - `version參數`: 依賴的版本
  - `features參數`: 要開啟的 features，參考，[條件式編譯](#features-section---用於條件式編譯)
  - `optional參數`: 當前依賴庫，是否是可選依賴，參考，[可選依賴](#features-section---用於條件式依賴)
  - `default-features參數`: 是否禁用當前依賴庫的 default-feature 功能
    > 注意，若其他依賴使用了同一個庫，但使用不同的 default-feature 值，則 default-feature 仍然是開啟的

  - 範例，使用本地的依賴庫
    ```toml
    # Cargo.toml
    ... 省略 ...

    [dependencies]
    gc = { path = "../boa_unicode", version = "1.0.141", features = ["derive", "rc"] }
    ```

  - 範例，使用 github 上的依賴庫
    ```toml
    # Cargo.toml
    ... 省略 ...

    [dependencies]
    color = { git = "https://github.com/bjz/color-rs" }
    ```

- 方法3，根據平台引入依賴
  ```toml
  # Cargo.toml
  ... 省略 ...

  # 根据特定的平台来引入依赖
  [target.'cfg(target_arch = "x86")'.dependencies]
  native = { path = "native/i686" }

  [target.'cfg(target_arch = "x86_64")'.dependencies]
  native = { path = "native/x86_64" }

  # 引入平台特定的依赖
  [target.x86_64-pc-windows-gnu.dependencies]
  winhttp = "0.4.0"

  [target.i686-unknown-linux-gnu.dependencies]
  openssl = "1.0.1"
  ```

## [dev-dependencies] section

定義在 [dev-dependencies] 的依賴，只會在 test、demo、benchmark的時候才會被引入

## [build-dependencies] section
指定僅用於構建時的依賴

## [features] section - 用於條件式編譯
- 概念
  
  feature 類似C語言透過 `define` 和 `#if` 進行條件式編譯的功能

  在 rust 中，透過在 Cargo.toml 檔案中`定義 feature後`，
  在代碼中，`透過cfg語句修飾的代碼`，只有在`對應的feature啟用`後，
  被修飾的代碼才會被編譯

- `step1`，在 cargo.toml 的檔案中，自定義 feature 的名稱，
  - 注意，自定義的 feature 名稱只能是數字、英文、`-`、`_`、`+`、
  - 範例
    ```toml
    # cargo.toml
    
    ... 省略 ...

    [features]
    # 定義一個名稱為 webp 的 feature，webp 沒有依賴其他 feature
    webp = []
    
    # 定義一個名稱為 ico 的 feature，且依賴 bmp-feature 和 png-feature
    # 當 ico-feature 啟用時，bmp-feature 和 png-feature 都會被啟用
    ico = ["bmp", "png"] 
    ```

- `step2`，在代碼中，透過`cfg語句`，對需要條件式編譯的代碼進行修飾，
  
  注意，cfg 用於指定那些代碼式條件式編譯的代碼，透過 feature 定義開啟編譯的時機

  ```rust
  [cfg(feature = "webp")] // 當 webp-feature 啟用後，才編譯 pub mod webp 的代碼
  pub mod webp;
  ```

- 默認啟用的 default-feature
  - 默認情況下，所有 feature 都被禁用，但寫在 default 的 feature 會默認啟用
    ```toml
    # cargo.toml
    ... 省略 ...

    [features]
    default = ["ico", "webp"] # 默認啟用 ico 和 webp 兩個 features
    bmp = []
    png = []
    ico = ["bmp", "png"]
    webp = []
    ```

  - 透過 `--no-default-features` 可以告知編譯器不要啟用 default-feature

- 啟用第三方 package 的 feature
  - 方法1，透過依賴`直接啟用`

    ```toml
    # cargo.toml
    ... 省略 ...

    [dependencies]
    # 直接啟用 flate2 的 zlib-features
    flate2 = { version = "1.0.3", default-features = false, features = ["zlib"] }
    ```

  - 方法2，透過 `包名/feature名` 的方式`間接啟用`

    ```toml
    # cargo.toml
    ... 省略 ...

    [features]
    # 間接啟用第三方庫 jpeg-decoder 的 rayon-feature
    parallel = ["jpeg-decoder/rayon"]
    ````

- feature 的統一化 (Feature unification)
  - 在同一個 package 中，不允許同名的 feature
  - 在不同 package 中，允許同名的 feature
  - 當一個依賴被多個包所使用時，這些包對該依賴所設置的 feature 將被進行合併，例如，
    - my-package 被 foo 使用，並開啟 fileapi 和 handleapi 等 feature
    - my-package 被 bar 使用，並開啟 std 和 winnt 等 feature
    - 最後，my-package 只會保留一份，且開啟 fileapi、handleapi、std、winnt、四個 feature
    
    <img src="doc/cargo-toml-usage/feature-unification.png" width=70% height=auto>

- [完整範例](#範例feature-使用範例)

## [features] section - 用於條件式依賴
- `step1`，透過依賴的`optional參數`，將依賴庫定義為`可選依賴庫`
  
  - 可選依賴庫，`optional = true`，默認不會被編譯，

  - 可選依賴庫會隱式自動建立的`同名feature`

    當依賴庫成為可選依賴庫後，會隱式的自動定義一個`與依賴庫同名的feature`，
    只有在該 feature 被啟用後，可選依賴庫才會導入並編譯，例如，
  
    ```toml
    [dependencies]
    # 透過 optional參數，將 gif 定義為可選依賴庫，會隱式自動建立 git-feature
    gif = { version = "0.11.1", optional = true }

    [features]
    # 可選依賴庫會隱式自動建立與依賴庫同名的feature，相當於以下顯式的語法
    # dep:gif 與一般的feature名區別，當開啟 git-feature 時，會`編譯可選依賴庫 gif`
    # 以下的語句代表，當啟用 git 的 feature後，將可選依賴庫 gif 進行編譯
    gif = ["dep:gif"]
    ```

  - 為可選依賴庫建立不同的 feature名
    - 語法: 與可選依賴庫同名的feature名 = ["def:庫名"]
      > 例如，mygif = ["dep:gif"]

    - 用於 `顯式列出可選依賴庫的feature` 或 `為可選依賴庫建立不同名的 feature名`
    - 注意，dep 關鍵字 在 Rust 1.6 之後才可使用

- `step2`，啟用可選依賴庫的 feature
  - 方法1，透過命令列參數，`--feature feature名`
  - 方法2，透過 [[feature]]，參考，[條件式編譯](#features-section---用於條件式編譯)
    ```toml
    [dependencies]
    # 將 ravif 定義為可選依賴庫，自動建立 ravif-feature
    ravif = { version = "0.6.3", optional = true }
    
    # 將 rgb 定義為可選依賴庫，自動建立 rgb-feature
    rgb = { version = "0.8.25", optional = true }

    [features]
    # 啟用可選依賴庫的同名的feature
    avif = ["ravif", "rgb"]
    ```

- 範例
  ```toml
  [dependencies]
  
  # gif 為可選依賴庫，且會自動建立一個名為 gif 的 feature
  # 當 gif-feature 啟用時，gif 依賴庫才會導入並編譯 
  gif = { version = "0.11.1", optional = true }
  ```

- [完整範例](#範例feature-使用範例)

## [lib] section

## [[bin]] section
- name參數: 定義執行檔的名稱
- path參數: 定義目標編譯檔案的路徑
- 可以有多個 [[bin]] section，使用 --bin 指定編譯對象或編譯全部
  - 若不使用 --bin 指定編譯對象，預設會編譯所有的 [[bin]] section 的編譯目標
  - 透過 cargo run --bin <name參數值> 指定要編譯哪一個 [[bin]] section 的編譯目標

- 預設路徑

  不透過`path參數`指定編譯檔案的路徑時，
  預設會自動在`src\bin\`目錄下尋找與`name參數`同名的檔案，

- 範例，自定義編譯目標，使用`預設路徑`
  ```
  [[bin]]
  # 在 src\bin\ 目錄下尋找 looad-js-string.rs 的檔案
  # 透過 cargo run --bin load-js-string 編譯並執行
  name = "load-js-string"
  ```

- 範例，自定義編譯目標，使用`自定義路徑`
  ```
  [[bin]]
  # 透過 cargo run --bin js-file 編譯並執行
  name = "js-file"

  # 指定目標檔案的路徑，
  path = "src/load-js-file/load-js-file.rs"
  ```

## [[bench]] section

## 範例，feature 使用範例
```toml
[features]

# 定義 profiler-feature
# 若啟用 profiler-feature，啟用 boa_profiler 庫的 profiler-feature
profiler = ["boa_profiler/profiler"]

# 定義 intl-feature
# 若啟用 intl-feature，編譯以下的的可選依賴庫
intl = [
    "dep:icu_locale_canonicalizer",
    "dep:icu_locid",
    "dep:icu_datetime",
]

# 定義 console-feature，用於代碼中的條件式編譯
console = []

[dependencies]
boa_profiler = { path = "../boa_profiler", version = "0.15.0" }

# 透過optional參數，將以下依賴庫定義為可選依賴庫
icu_locale_canonicalizer = { version = "0.6.0", features = ["serde"], optional = true }
icu_locid = { version = "0.6.0", features = ["serde"], optional = true }
icu_datetime = { version = "0.6.0", features = ["serde"], optional = true }
```

## 參考
- [Cargo.toml詳解](https://course.rs/cargo/reference/manifest.html)
- [The Cargo Book](https://doc.rust-lang.org/cargo/reference/manifest.html)
