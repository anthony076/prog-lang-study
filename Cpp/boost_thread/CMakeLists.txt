cmake_minimum_required(VERSION 3.10)
project(boost_thread)

set(CMAKE_CXX_STANDARD 11)

include_directories(/usr/local/Cellar/boost/1.67.0_1/include/)

# 指定 Boost Link Library 的位置
# http://www.voidcn.com/article/p-bcbxqesb-we.html
link_libraries(/usr/local/Cellar/boost/1.67.0_1/lib/libboost_thread-mt.a)
link_libraries(/usr/local/Cellar/boost/1.67.0_1/lib/libboost_system-mt.a)

add_executable(boost_thread main.cpp)