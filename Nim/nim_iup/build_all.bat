cd %~dp0

nim compile --verbosity:0 --hints:off  --out:iup_button.exe src/iup_button.nim
nim compile --verbosity:0 --hints:off  --out:iup_menu.exe src/iup_menu.nim
nim compile --verbosity:0 --hints:off  --out:iup_tab.exe src/iup_tab.nim

pause