#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

void unsafe_declare(){
    cout << "==== 不安全的指標宣告範例 ====" << endl;

    int *p_myInt;
    cout << "p_myInt 賦予地址前" << endl;
    cout << "p_myInt指針變數，儲存的位址 : " << p_myInt << endl;
    cout << "p_myInt指針變數，指向位置的內容 : " << *p_myInt << endl;
}

void recommend_declare(){
    cout << "==== 推薦的指標宣告範例 ====" << endl;

    // 將指標變數指向一個空指針，避免不可預期的錯誤發生
    int *p_myInt = nullptr;

    cout << "p_myInt 賦予地址前" << endl;
    cout << "p_myInt指針變數，儲存的位址 : " << p_myInt << endl;
    //cout << "p_myInt指針變數，指向位置的內容 : " << *p_myInt << endl;     //會報錯，指標變數賦予地址前不能取用

    int myInt = 123;
    cout << "myInt" << endl;
    cout << "myInt變數的內容 : " << myInt << endl;
    cout << "myInt變數的記憶體位址 : " << &myInt << endl;

    p_myInt = &myInt;
    cout << "將myInt記憶體位址賦予給p_myInt後" << endl;
    cout << "p_myInt指針變數，儲存的位址 : " << p_myInt << endl;
    cout << "p_myInt指針變數，指向位置的內容 : " << *p_myInt << endl;

}

void access_origin_variable(){
    cout << "==== 透過指標存取指向變數的內容 ====" << endl;

    int myInt = 123;

    // 透過指標變數，讀取原變數的內容
    int *p_myInt = &myInt;
    cout << "透過指標變數，讀取原變數的內容 : " << *p_myInt << endl;

    // 透過指標變數，修改原變數的內容
    *p_myInt = 456;
    cout << "直接讀取 原變數的內容 : " << myInt << endl;
    cout << "透過指標變數，讀取原變數的內容 : " << *p_myInt << endl;

}

void char_points_diffaddr(){
    cout << "==== 字元指標的重新賦值 ====" << endl;

    /*
     ＃1
     注意，因為字元指標可以當作一般的字串使用，
     因此，要知道 該字元指標所儲存的記憶體位址時，需要先將字元指標賦值給另一個一般指標
     否則，該字元指標可當作一般的字串使用，就不需要透過取值運算子(＆)

     #2
     char *str  該指標指向的對象為字元，但指標儲存的是記憶體位址，兩者類型不同
     因此，
        要取得指向對象的記憶體位址時，需要透過另一個指標
        要當一般字串使用時，不需要透過取值運算子(＆)，但每次重新指向時，會重新分配記憶體位址

     #3
     為什麼會重新分配記憶體位址
     因為字串是常數，只能讀不能修改，當使用＝做重新指向時，會指向新的記憶體空間

    */
    // 用來儲存字元指標所儲存的記憶體位址
    void *addr = nullptr;

    // 將字元指標指向一個字元
    char *str = "hello";

    // 取得字元指標儲存的記憶體位置
    addr = str;

    // 取得 字元指標str 指向的內容
    cout << str << endl;   // 印出 hello

    // 取得 字元指標str 儲存的記憶體位址
    cout << addr << endl;  // 印出 0x103827f0e

    // 對字元指標使用取值運算符(*str)，會得到字串的第一個字
    //cout << *str << endl;  // 印出 h

    /*
    將字元指標指向一個新字元，注意，會重新分配記憶體位址
    因次 "world" 是一個字串常數，只能讀不能寫，
    str = "world" 這個動作，會把 str 指到另一個沒在用的位址然後新增一個內含元素為 “world\0” 的 static char[5]
    之前那個字串 "hello\n" 還是留在原地沒有消失
    */
    str = "world";

    // 取得字元指標儲存的記憶體位置
    addr = str;

    cout << str << endl;
    cout << addr << endl;
}

void char_points_exmaple(){
    cout << "==== 字元指標的使用範例 ====" << endl;

    const char *weekday[10] ={"Monday", "Tuesday", "Wednesday",
                              "Thursday", "Friday", "Saturday",
                              "Sunday"};
    for (auto &i : weekday) {
        cout << i << endl;
    }
}

void array_pointer_content(){
    cout << "==== 陣列指標的使用範例 ====" << endl;

    // 建立陣列
    short array[] = { 11,22,33,44,55};

    // 建立陣列指標
    short* arrayptr;

    // 取得第一個元素的記憶體位址，方法一
    // array名稱可當指標用，指向第一個元素的記憶體位址
    arrayptr = array;
    cout << arrayptr << endl;   // 印出 0x61ff12

    // 取得第一個元素的記憶體位址，方法二
    arrayptr = &array[0];
    cout << arrayptr << endl;   // 印出 0x61ff12

    // 透過指標陣列印出陣列內容
    cout << "*arrayptr: " << *arrayptr << endl;   // 印出 11
    cout << "*(arrayptr+1): " << *(arrayptr+1) << endl;   // 印出 22
    cout << "*arrayptr+1: " << *arrayptr+1 << endl;   // 印出 12


    cout << "*array: " << *array << endl;   // 印出 11
    cout << "*(array+2): " << *(array+2) << endl;   // 印出 33

    // 列印出陣列內容
    cout << array[0] << endl;   // 印出 11
}

void array_pointer_select(){
    cout << "==== 單一字串指標的使用範例 ====" << endl;

    char string[] = "ANSI/ISO C++";
    const char* pstring = "Virtual C++";

    // 顯示字元陣列的全部內容
    cout << string << endl;     // 印出 ANSI/ISO C++
    // 顯示字元指標的全部內容
    cout << pstring<< endl;     // 印出 Virtual C++

    // 顯示字元陣列中第6個字元
    cout << string[6] << endl;  // 印出 S
    // 顯示字元指標中第6個字元至結束
    cout << pstring+6 << endl;  // 印出 l C++
}

void twoD_array_example(){
    cout << "==== 多字串指標的使用範例 ====" << endl;

    // 用字元陣列宣告2D array，共用4個元素，每個元素長20 byte
    char array[4][20] = {"this","is","string","test"};
    // 用字元指標陣列宣告2D array
    const char* parray[4] = {"this","is","string","test"};

    // 顯示字元陣列中，第3(0開始)個元素
    cout << array[2]<< endl;    // 印出 string
    // 顯示字串指標中，第3個元素，該指標指向第3個元素string
    cout << parray[2]<< endl;   // 印出 string

    // 顯示字元陣列中，第0個元素的第1 2字元
    cout << array[0][1] << array[0][2] << endl; // 印出 hi
    // 顯示字串指標中，第0個元素的第1字元開始至結束
    cout << parray[0]+1 << endl;    // 印出 his
}

void array_of_pointers(){
    cout << "==== 指標陣列範例：存放指標的一維陣列 ====" << endl;

    //int MAX = 3   // 錯誤，宣告陣列大小的引數，可以是常數，不能是變數
    const int MAX = 3;

    int var[MAX] = {10,100,200};

    // 建立指標陣列，存放指標的一維陣列
    int *ptr[MAX];

    for(int i=0; i<MAX; i++) {
        // 為指標陣列中的每一個指標進行初始化
        ptr[i] = &var[i];
        cout << *ptr[i] << endl;    // 印出 10 100 200
    }
}

void pass_pointer_to_function(int *p){
    cout << p << ":" << *p << endl;
}

int * return_pointer_from_function(){

    cout << "==== 從函數返回指標範例 ====" << endl;

    // 注意，指標在函數結束後，指向的對象就會消失，因此 C++ 不允許返回局部變量的地址，除非該局部變量定義為 static 變量
    //int a = 100;            // 錯誤宣告方式，該變量在函數結束後會消失，讓指標變成野指標
    static int a = 100;       // 正確的宣告方式，返回指標時，需要再函數中宣告指標的對象為不可變

    int *p = &a;

    return p;       // 返回指標
}

int * return_pointer_from_function_array(){

    cout << "==== 從函數返回指標範例，以 array 為例 ====" << endl;

    // 注意，指標在函數結束後，指向的對象就會消失，因此 C++ 不允許返回局部變量的地址，除非該局部變量定義為 static 變量
    //int a[] = { 100,200,300 };            // 錯誤宣告方式，該變量在函數結束後會消失，讓指標變成野指標
    static int a[] = { 100,200,300 };       // 正確的宣告方式，返回指標時，需要再函數中宣告指標的對象為不可變
    cout << "address of a[0]: " << a << endl;   // 印出 address of a[0]: 0x10b4cc184

    return a;   // 返回陣列名 = 返回指標，陣列名是指標常數
}


int main(int argc, const char * argv[])
{
    // 不安全的指標宣告範例
    //unsafe_declare();

    // 推薦的指標宣告範例
    //recommend_declare();

    // 透過指標存取指向變數的內容
    //access_origin_variable();

    // 字元指標的重新賦值
    //char_points_diffaddr();

    // 字元指標的使用範例
    //char_points_exmaple();

    // 陣列指標的使用範例
    //array_pointer_content();

    // 單一字串指標的使用範例
    //array_pointer_select();

    // 多字串指標的使用範例
    //twoD_array_example();

    // 指標陣列範例：存放指標的一維陣列
    //array_of_pointers();

    // 傳遞指標給函數範例
    /*
    cout << "==== 傳遞指標給函數範例 ====" << endl;

    int a = 100;
    cout << "變數a的記憶體位址為: " << &a << endl;
    pass_pointer_to_function(&a);
    */

    // 從函數返回指標範例
    int *pa = return_pointer_from_function();
    cout << pa << " : " << *pa << endl;     // 印出 0x104dc2180 : 100

    // 從函數返回指標範例，以 array 為例
    int *pb = return_pointer_from_function_array();
    cout << pb << " : " << *(pb+1) << endl;     // 印出 0x10b4cc184 : 200


}
