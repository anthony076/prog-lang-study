#include <iostream>
using namespace std;

int main() {

    // 定義變數的一般方式
    int a, b;

    a = 10;
    b = 20;
    int g = a + b;

    cout << "a+b = " << g << endl;

    // extern 的使用
    // 作用，告訴編譯器該變數定義已經在其他地方定義過了
    // 編譯器會試圖在其它位置或文件中找出 some_var 的定義
    extern double some_var;
    cout << some_var << endl;
}

