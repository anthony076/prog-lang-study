#include <iostream>
#include "main.h"

using namespace std;

// ==== 重載函數範例 ====
class PrintData {
public:
    void print(int i){
        cout << " int variable: " << i << endl;
    }
    void print(double f){
        cout << " double variable: " << f << endl;
    }

    void print(string c){
        cout << " string variable: " << c << endl;
    }
};

// ==== 二元運算符重載範例，定義在類中 ====

class Box{
    double _length ;
    double _width ;
    double _height ;

public:

    double getVolume(){
        return _length * _width * _height;
    }

    // 構造函數
    Box(double l = 0 , double w = 0, double h = 0):_length(l),_width(w),_height(h){}

    // 在類中重載＋運算符，用於將兩個Box對象相加
    // 前面的 Box，代表該重載運算符最後會返回一個Box對象
    // 在類中定義的運算符重載，會自動帶入被加數b1
    // 後面的 Box(位於引數的Box,b2)，會將加號後方的Box對象(加數)帶入，此處的b2代表對加數的引用
    // 程式碼中的 Box,b3，用於運算符運算後的結果
    Box operator+(Box &b2){
        // 建立新的Box對像，用於運算符運算後的結果
        Box b3;

        cout << "加數b2 : " << b2._length << " " << b2._width << " " << b2._height << endl;

        // 在類中定義的運算符重載，會自動帶入被加數b1，因此，用this 指針來存取b1的值
        // 將被加數b1的length(this->length) 加上加數b2的length(b.length)後，儲存在結果對象b3中(box.length)
        b3._length = this->_length + b2._length;
        b3._width = this->_width + b2._width;
        b3._height = this->_height + b2._height;

        // 返回最後的結果對象
        return b3;
    }
};


// ==== 二元運算符重載範例，定義在類外 ====
class Cube{

public:
    // 因為二元運算符重載是定義在類外，必須將參數宣告為 public 才能在類外被存取
    double _length ;
    double _width ;
    double _height ;

    double getVolume(){
        return _length * _width * _height;
    }

    // 構造函數
    Cube(double l = 0 , double w = 0, double h = 0):_length(l),_width(w),_height(h){}

};

// 在類外重載-運算符，用於將兩個Box對象相減，最後返回一個Cube對象
Cube operator-(Cube &c1, Cube &c2){
    // 建立新的Cube對像，用於運算符運算後的結果
    Cube c3;

    // 運算符前面是被減數c1
    cout << "被減數 : " << c1._length << " " << c1._width << " " << c1._height << endl;
    // 運算符前面是減數c2
    cout << "減數 : " << c2._length << " " << c2._width << " " << c2._height << endl;


    // 將被加數b1的length(this->length) 加上加數b2的length(b.length)後，儲存在結果對象b3中(box.length)
    c3._length = c1._length - c2._length;
    c3._width = c1._width - c2._width;
    c3._height = c1._height - c2._height;

    // 返回最後的結果對象
    return c3;
}


int main(int argc, const char * argv[])
{
    // 重載函數範例
    //simple_overloaded_example();

    // 二元運算符重載範例，定義在類中
    overloaded_operator_inClass();

    // 二元運算符重載範例，定義在類外
    overloaded_operator_outClass();
}

void simple_overloaded_example(){
    cout << "==== 重載函數範例 ====" << endl;

    PrintData pd;
    pd.print(0.5);
    pd.print(1);
    pd.print("aaa");
}

void overloaded_operator_inClass(){
    cout << "==== 二元運算符重載範例，定義在類中 ====" << endl;

    Box b1(6,7,5), b2(12,13,10);

    // 單純將 volume 的結果相加
    // volume = (6*7*5) + (12*13*10) = 210 + 1560 = 1770

    cout << b1.getVolume() + b2.getVolume() << endl;     // 印出   1770

    // 利用重載運算符做運算，將b1和b2的引數相加後再做運算
    // volume = (6+12) * (7+13) * (5+10) = 18 * 20 * 15 = 5400
    cout << (b1+b2).getVolume() << endl; // 印出   5400

}

void overloaded_operator_outClass(){
    cout << "==== 二元運算符重載範例，定義在類外 ====" << endl;

    Cube c1(10,10,10), c2(5,5,5);
    cout << (c1-c2).getVolume() << endl;    // 印出 125
}


