// 執行 $ codeql query run test.ql --database=output_db

//	輸出到檔案  
//		執行查詢，並輸出 bqrs 檔案，$ codeql query run test.ql --database=output_db --output=result.bqrs
//		將 bqrs檔案 轉換為 json 格式，$ codeql bqrs decode --format=json result.bqrs > result.json
// 		利用 jq 命令檢視結果，$ cat result.json | jq .
import cpp

from 
	FunctionCall call, 
	Function func, 
	SizeofExprOperator sizeof,
	VariableAccess va
where 
	call.getTarget() = func and
	func.getName() = "malloc" and
	call.getArgument(0) = sizeof and
	sizeof.getExprOperand() = va and
  va.getTarget().getType() instanceof PointerType
select sizeof