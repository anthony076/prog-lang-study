
## 函數式編程的核心

- 不可變變數

- 以遞歸取代迴圈 
- 以case取代if

- 純函數
- 高階函數
- 函數組合
- 惰性求值函數

## 不可變變數

- 在 python 中，變數預設是可變的，對於引用類型的變數，例如list，
  可以利用 list.copy() 來實現不可變性

  ```python
  a = [1,2,3]
  foo = a
  foo[0] = 10
  print(foo)    # [10,2,3]，foo是引用類型，修改foo也會修改a

  bar = a.copy()    # a = [10,2,3]，bar 複製了a的內容，但bar不是引用類型，bar是不可變的
  bar[0] = 100
  print(a)    # [10,2,3]    修改 bar 並不會改變a
  print(bar)    # [100,2,3] 修改 bar 只會改變bar
  ```

## 以遞歸取代迴圈

- 範例，以計算階乘為例

  <font color=blue>loop版本</font>

  ```python  
  def factorial_loop(n):
      result = 1
      for i in range(1, n + 1):
          result *= i
      return result

  print(factorial_loop(5))  # 輸出: 120
  ```

  <font color=blue>遞歸版本</font>

  ```python  
  def factorial_recursive(n):
      if n == 0:
          return 1
      else:
          return n * factorial_recursive(n - 1)

  print(factorial_recursive(5))  # 輸出: 120
  ```

## 使用 structure pattern matching 取代 if (python 3.10)

- 範例

  <font color=blue>使用if</font>

  ```python
  def handle_data(data:tuple):
      if isinstance(data, tuple):
          if len(data) == 2:
              a, b = data
              print(f"Tuple with two elements: {a} and {b}")
          elif len(data) == 3:
              a, b, c = data
              print(f"Tuple with three elements: {a}, {b}, and {c}")
          else:
              print("Tuple with unknown structure")
      elif isinstance(data, list):
          if data and data[0] == "special":
              print(f"Special list: {data}")
          else:
              print(f"General list: {data}")
      else:
          print("Unknown data type")

  # 測試
  handle_data((1, 2))
  handle_data((1, 2, 3))
  handle_data(["special", 42])
  handle_data([1, 2, 3])
  handle_data("string")
  ```

  <font color=blue>使用case</font>

  ```python
  def handle_data(data):
      match data:
          case (a, b):
              print(f"Tuple with two elements: {a} and {b}")
          case (a, b, c):
              print(f"Tuple with three elements: {a}, {b}, and {c}")
          case ["special", *rest]:
              print(f"Special list: {data}")
          case list():
              print(f"General list: {data}")
          case _:
              print("Unknown data type")

  # 測試
  handle_data((1, 2))
  handle_data((1, 2, 3))
  handle_data(["special", 42])
  handle_data([1, 2, 3])
  handle_data("string")
  ```

## 純函數

- 純函數指的是相同的輸入參數，會得要相同的輸出結果

  <font color=blue>非純函數的範例</font>

  ```python
  def append_to_list(element, my_list):
      my_list.append(element)
      return my_list

  # 測試
  my_list = [1, 2, 3]
  print(append_to_list(4, my_list))  # 輸出: [1, 2, 3, 4]
  print(append_to_list(5, my_list))  # 輸出: [1, 2, 3, 4, 5] (my_list被修改了)
  ```

  <font color=blue>改成純函數的版本</font>

  ```python
  def append_to_list_pure(element, my_list):
      new_list = my_list + [element]
      return new_list

  # 測試
  my_list = [1, 2, 3]
  new_list1 = append_to_list_pure(4, my_list)
  new_list2 = append_to_list_pure(5, my_list)

  print("Original list:", my_list)        # 輸出: [1, 2, 3]   my_list 沒有被修改
  print("New list 1:", new_list1)         # 輸出: [1, 2, 3, 4]
  print("New list 2:", new_list2)         # 輸出: [1, 2, 3, 5]
  ```

## 高階函數

- 高階函數（Higher-Order Function），指接受函數作為參數或返回一個函數的函數。
  簡單來說，高階函數可以操作其他函數。

  <font color=blue>以map()為例</font>
  
  ```py
  def square(x):
      return x * x

  numbers = [1, 2, 3, 4, 5]
  squared_numbers = map(square, numbers)

  # 將 map 物件轉換為列表以查看結果
  print(list(squared_numbers))  # 輸出: [1, 4, 9, 16, 25]
  ```

  <font color=blue>自定義高階函數</font>

  ```py
  def apply_function(func, value):
      return func(value)

  # 測試
  def increment(x):
      return x + 1

  print(apply_function(increment, 5))  # 輸出: 6
  print(apply_function(square, 3))     # 輸出: 9
  ```

## 函數組合

- 函數組合(Function composition) : 透過函數的組合構建更複雜行為的新函數，
  將多個函數組合在一起，使得一個函數的輸出成為下一個函數的輸入

  ```py
  def compose(f,g):
      return lambda x:g(f(x))

  def add_one(x):
      return x+1

  def square(x):
      return x*x

  composed_function = compose(add_one, square)
  print(composed_function(3)) # 輸出 : 16，先+1，再平方
  ```

## 惰性求值 

- 在 python 中，利用 generator 搭配 yield 和 next() 的語法實現惰性求值

  ```py
  def square_numbers(nums):
      for num in nums:
          yield num * num

  # 使用生成器
  nums = [1, 2, 3, 4, 5]
  squares = square_numbers(nums)

  # 打印生成器生成的平方数
  for square in squares:
      print(square)
  ```

## ref

- [7 Functional Programming Techniques to Enhance Your Python Code](https://www.youtube.com/watch?v=Rp9Ha0rVM1w)