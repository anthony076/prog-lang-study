
import java.util.Random


fun main(args: Array<String>) {

    // 方法一，使用java built-in function
    val randomA = Random().nextInt(100)
    println(randomA)

    // 方法二，透過內建的Ｍath功能
    val randomB = (Math.random() * 100).toInt()
    println(randomB)

}