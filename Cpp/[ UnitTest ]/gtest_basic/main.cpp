#include <iostream>
#include "./googletest/include/gtest/gtest.h"

using namespace std;

int add(int a, int b){
    return a+b;
}

/*
//gTest 不需要 main()，加了會報錯

int main(int argc, const char * argv[])
{
    cout << add(3,3) << endl;
}
*/

TEST(caseName, testName){
    EXPECT_EQ(3,add(1,2));
}

GTEST_API_ int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}