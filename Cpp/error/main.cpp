#include <iostream>
#include <exception>
#include "main.h"

using namespace std;

struct MyException : public exception{

    // const throw() 異常規格說明，放在函數名後方，用來表示 what() 可以拋出異常的類型
    //  狀況1，const throw() 代表不會拋出任何異常
    //  狀況2，const throw( A, B, C)  代表會拋出 A B C 三種異常
    //  狀況3，不添加異常規格說明，代表會拋出任意異常()
    const char * what() const throw()
    {
        return "C++ Exception";
    }
};

int main(int argc, const char * argv[])
{
    // 利用 throw 主動拋出異常範例
    cout << "==== 利用 throw 主動拋出異常範例 ====" << endl;
    cout << throw_example(1,3) << endl;

    // 利用 try-catch 捕捉異常範例
    // 若不捕捉異常，c++ 內建的異常
    // 請參考 http://www.runoob.com/cplusplus/cpp-exceptions-handling.html
    try_catch_example();

    // 自定義要拋出的異常，透過繼承和重載 exception類
    self_define_exception();
}


double throw_example(int a, int b) {
    if (b == 0){

        // 主動拋出異常
        throw "b cant be 0";
    }

    return ((double)a/b);
}

void try_catch_example(){
    //catch 會自動判斷吐出來的異常的類型，並在catch中找到對應的類型去執行

    cout << "==== 利用 try-catch 捕捉異常範例 ====" << endl;

    try{
        //throw_example() 發生異常時會拋出字串
        throw_example(5,0);
    } catch (const char* msg){  // throw_example() 發生異常時會拋出字串，所以 catch 也宣告字串變數來接
        cout << msg << endl;
    }

    try{
        // 拋出int類型 Error
        // 會跳到int類型的catch去執行
        throw 1;
    } catch(int code){
        cout << "int Code: " << code << endl;
    } catch(float code){
        cout << "float Code: " << code << endl;
    }

}

void self_define_exception(){
    cout << "==== 自定義異常類型，透過繼承和重載 exception類 ====" << endl;

    try{
        // 主動拋出自定義的異常
        throw MyException();
    } catch(MyException& e){
        // 執行自定義異常的內容
        cout << e.what() << endl;
    }

}