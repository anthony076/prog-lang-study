
// 注意，#define CATCH_CONFIG_MAIN 必須寫在 #include "catch.hpp" 之前

// 告訴 catch2 添加 main function
#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "functions.h"

TEST_CASE("add_value_test", "[main_test]"){
    CHECK( add(4,3) == 7 );
    CHECK( add(5,5) == 10 );
}

TEST_CASE("minus_value_test", "[main_test]"){
    CHECK( minus(3,1) == 2 );
    CHECK( minus(5,4) == 1 );
    REQUIRE_FALSE(1==2);
}

TEST_CASE("pprox_usage", "[approx]"){
    // float_test() 的結果近似 1.0
    CHECK( float_test() == Approx(1.0) );

    // 設定誤差範圍
    Approx target = Approx(5).epsilon(0.5); // 設定誤差為 5.0 +/- 50%
    CHECK( float_test() <= target );
}