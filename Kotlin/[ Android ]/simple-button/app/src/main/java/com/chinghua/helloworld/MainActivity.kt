package com.chinghua.helloworld

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    // ===== 初始化 =====
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R. layout.activity_main)
    }

    // ===== 處理元件的 onClick 事件 =====
    fun click(v:View){
        // 取得 TextView 元件
        val text_show = findViewById(R.id.text_show) as TextView
        // 取得 TextView 的內容
        val content = text_show.text.toString()

        when(v.id){
            // 以訊息提示的方式顯示 TextView 的內容
            R.id.text_show -> Toast.makeText(this, content, Toast.LENGTH_LONG).show()
            // 設定 hello Button 的動作
            R.id.btn_hello -> text_show.setText("Hello")
            // 設定 world Button 的動作
            R.id.btn_world -> text_show.setText("World")
        }
    }
}

