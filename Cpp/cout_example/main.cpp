/*
    cout 和 printf() 的差異 ：
    printf() 是函數，只接受字串，無法直接將函數的結果輸出
    透過 cout 將非字串的結果輸出到 console 中
*/

#include <iostream>
using namespace std;

// 計算 x square
void cal_square(double x){
    double result = x * x;
    // 輸出 square()的結果
    // return : The square of 200 is 40000
    cout << "The square of " << x << " is " << result << "\n";
}

int main(int argc, const char * argv[])
{
    cal_square(200);
    return 0;
}