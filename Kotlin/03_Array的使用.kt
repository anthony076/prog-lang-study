/*

# Array 的使用 ： http://www.codedata.com.tw/kotlin/kt07/

# Array 常用函數
    * 記憶點
        - 字串Array，可使用省略型態的寫法
          數據Array，不可使用省略型態的寫法
          因為數據Array有多種類型，字串只有一種類型
        - 指定元素, ArrayOf
          數據Array, xxxArrayOf()
          字串Array, arrayOf()
        - 不指定元素
          數據Array, xxxArray(個數)
          字串Array, arrayOfNulls(個數)

    * 指定元素內容，xxxArrayOf()
        val numbers : IntArray = intArrayOf(1,2,3,4,5)
      不指定元素內容，xxxArray()
        val numbers : Int
    * 建立字串Ａrray，指定元素內容，arrayOf()
        val strArray : Array<String> = arrayOf("a", "b")
        val strArray = arrayOf("a", "b")
      建立字串Ａrray，不指定元素內容，arrayOfNulls()
        var emptyArray : Array<String?> = arrayOfNulls(5)

# Array 常用的方法
    * 返回元素的 index (不包含值)
        語法，Ａrray.indices
    * 同時產生 index + 元素值，
        語法，Array.withIndex()
        範例，for ((i,j) in numbers.withIndex()){...}
    * 取得 Array 的長度
    array.size
    * 將 Array 的內容轉為字串並加以連接
    array.joinToString(" ")
    * 取得 Array 中部分的內容
    array.slice(2..4)
*/

fun main(args: Array<String>) {
    // 建立 數值Array 和 字串Array
    buildArray()
    buildStringArray()

    // 利用迴圈取得 Array 中的 index
    getIndex_only()
    // 利用迴圈同時取得 Array 中的 index 和 value
    gitIndex_both()

    // 快速遍歷的方法 array.forEach(){...}
    speedup_iterable()

    // Array常用的方法
    Array_common()
}

fun buildArray(){
    println("==== buildArray() ====")

    // 宣告 Array 變數，並指定Array的內容
    val numbers : IntArray = intArrayOf(1,2,3,4,5)
    for (i in numbers){
        println(i)
    }

    // 宣告 Array 變數，不指定Array的內容
    val intNumbers : IntArray = IntArray(3)
    intNumbers[0] = 11
    intNumbers[1] = 22
    intNumbers[2] = 33
    for (i in intNumbers){
        println(i)
    }
}

fun buildStringArray(){
    println("==== buildStringArray() ====")

    // 指定內容的字串Ａrray，可省略類型的宣告方法
    // 範例，val strArray = arrayOf("a","b")

    // 指定內容的字串Ａrray，一般的宣告方法
    val strArray : Array<String> = arrayOf("Hello", "Anthony")
    for (i in strArray){
        println(i)
    }

    // 不指定內容的字串Ａrray，可省略類型的宣告方法
    // 範例，val emptyArray = arrayOfNulls<String>(2)

    // 不指定內容的字串Ａrray，一般的宣告方法，類型必須加上？，表示可為空
    val emptyArray: Array<String?> = arrayOfNulls(2)
    emptyArray[0] = "a"
    emptyArray[1] = "b"
    for (i in emptyArray){
        println(i)
    }

}

fun getIndex_only(){
    println("==== getIndex_only() ====")
    val numbers : IntArray = intArrayOf(111,222,333,444,555)
    for (i in numbers.indices){
        println(i)
        println(numbers[i])
    }
}

fun gitIndex_both(){
    println("==== getIndex_both() ====")

    // 定義 IntArray
    val numbers : IntArray = intArrayOf(55,66,77,88,99)

    // 方法一，使用兩個變數承接 index 和 value 的值
    for ((index,value) in numbers.withIndex()){
        println("$index, $value")
    }

    println()

    // 方法二，使用屬性取得 index 和 value 的值
    for (item in numbers.withIndex()){
        println("${item.index}, ${item.value}")
    }
}

fun speedup_iterable(){
    println("==== speedup_iterable() ====")

    // 宣告 Array 變數，並指定Array的內容
    val numbers : IntArray = intArrayOf(1,2,3,4,5)

    // 方法一，一般方法
    // 其中 it 為 array中的元素，it 為 .forEach() 的內建變數
    // .forEach() 會把 array 中的每一個元素提取出來，並放到變數 it 中
    numbers.forEach {
        println(it)
    }

    println()

    // 方法二，使用 .forEach() + lambda 表達式
    // numbers.forEach({ it -> println(it)})
    numbers.forEach({ println(it)})
}

fun Array_common(){
    println("==== Array_common() ====")
    // 宣告 Array 變數，並指定Array的內容
    val numbers : IntArray = intArrayOf(1,2,3,4,5)

    // 取得 Array 的長度
    println(numbers.size)

    // 將 Array 的內容轉為字串並加以連接
    println(numbers.joinToString(" "))

    // 取得 Array 中部分的內容
    println(numbers.slice(2..4))

}

