/*
# 什麼是中綴表達式(infix notation)
  https://tw.saowen.com/a/196a9c029f05b27d75d32b3240151165d04d6e6b8b874a3df74d28a9f6352123
  中綴表示式 是操作符以中綴形式處於運算元的中間 (例如 3 + 4, 其中 + 為中綴操作符)

# 在迴圈中的 step、downTo 也都屬於官方內建的中綴表達式

# 中綴函數的使用限制
    - 中綴函數必須定義在 Class中，
        中綴函數必須是 Class的成員函數 或 Class的擴充套件函數
        ( 透過Ｃlass 取得第一個運算元 )
    - 中綴函數必須只有一個參數，位於中綴操作符後方的運算員會自動被帶入中綴函數的引數中
        例如 3 + 4， 4會被帶入中綴函數中
    - 中綴函數的參數不可有默認值，也不可使用可變長度的參數
    - 中綴函數調用的優先權：
        低於 算術運算符、類型轉換、rangeTo操作符
        高於 布林操作符(&&、||、is-、in-)
        完整的優先級層次結構，請參考 https://www.kotlincn.net/docs/reference/grammar.html#precedence

#Ref:
    https://www.kotlincn.net/docs/reference/functions.html#%E4%B8%AD%E7%BC%80%E8%A1%A8%E7%A4%BA%E6%B3%95
    https://www.jianshu.com/p/a50655be598a
*/

import kotlin.test.assertEquals

class Assertion<T>(private val target: T) {
    infix fun isEqualTo(other: T) {
        assertEquals(other, target)
    }
}

fun main(args: Array<String>) {
    // 範例一，將中綴函數定義在class中
    val result = Assertion(5)   // 透過 class 取得第一個運算元 5
    result isEqualTo 5                // 透過 中綴操作符取得第二個運算元 5，並調用中綴函數進行運算


    // 範例二，將中綴函數定義在Class的擴充套件函數中
    // 定義 Int Class的擴充套件函數 add
    infix fun Int.add(x:Int): Int  = this + x
    println(2 add 1)
    println(5 add 10)

}

