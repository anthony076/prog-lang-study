/*
reduce(acc, i) : 累加函數
    acc 累加的返回值
    i   當前遍歷列表中的值
*/

fun main(args: Array<String>) {
    val list = listOf(1,2,3,4,5)
    println(list.reduce{ acc, i -> acc + i })

    // 計算5的階乘
    println(factorial(10))
}

fun factorial( n: Int): Int{
    return (1..n).reduce{acc,i -> acc * i}
}

