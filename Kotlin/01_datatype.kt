/*
#1 main()   程式進入點
#2 宣告放外面
#3 val  不可變的變數（value）
   var  可變的變數 (variable)
   語法 : val 變數名：類型 ＝ 初始值

#4 var 可以覆蓋 val 的變數，val 不能覆蓋 var 的變數
   透過var建立變數時，編譯器會自動為該變數添加 getter() 和 setter()
   透過val建立變數時，編譯器只會為該變數添加 getter() 而沒有 setter()
   因此，透過 var 覆蓋 val 的變數時，只需要新增 setter()即可
        透過 val 覆蓋 var 的變數時，無法刪除 setter()


#5 kotlin中的數據類型
    1.Double型 双精度浮点型 64位 (Range : 2^64)
        範圍  4.9E-324 ~ 1.7976931348623157E308
    2.Float型 单精度浮点型 32位 (Range : 2^32)
        範圍  1.4E-45 ~ 3.4028235E38
    3.Long型 长整型 64位 (Range : 2^64)
        範圍  -9223372036854775808 ~ 9223372036854775807
    4.Int型 整型 32位 (Range : 2^32)
        範圍  -2147483648 ~ 2147483647
    5.Short 短整型 16位 (Range : 2^16)
        範圍  -32768 ~ 32767
    6.Byte 字节型 8位 (Range : 2^8)
        範圍  -128 ~ 127
*/

val aBoolean: Boolean = true //布尔值为真
val anotherBoolean: Boolean = false //布尔值为假

val aInt: Int = 8 //整型
val anotherInt: Int = 0xFF //整型
val binInt: Int = 0b00000011 //二进制
val maxInt: Int = Int.MAX_VALUE //最大整型值
val minInt: Int = Int.MIN_VALUE //最小整型值

val aLong: Long = 5641564851251941965 //长整型
val anotherLong: Long = 123 //长整型
val maxLong: Long = Long.MAX_VALUE //最大长整型值
val minLong: Long = Long.MIN_VALUE //最小长整型值

val aFloat: Float = 2.0F //单精度浮点型
val anotherFloat: Float = 1E3f //单精度浮点型
val maxFloat: Float = Float.MAX_VALUE //最大单精度浮点型值
val minFloat: Float = Float.MIN_VALUE //最小单精度浮点型值
val minRealFloat: Float = -Float.MAX_VALUE//真正最小单精度浮点型值

val aDouble: Double = 2.0 //双精度浮点型
val anotherDouble: Double = 3.1415926 //双精度浮点型
val maxDouble: Double = Double.MAX_VALUE //最大双精度浮点型值
val minDouble: Double = Double.MIN_VALUE //最小双精度浮点型值
val minRealDouble: Double = -Double.MAX_VALUE//真正最小双精度浮点型值

val aShort: Short = 127 //短整型
val maxShort: Short = Short.MAX_VALUE //最大短整型
val minShort: Short = Short.MIN_VALUE //最小短整型

val maxByte: Byte = Byte.MAX_VALUE //最大字节
val minByte: Byte = Byte.MIN_VALUE //最小字节

val aChar: Char = '0' //字符
val bChar: Char = '中' //字符
val cChar: Char = '\u000f' //Unicode字符

fun main(args: Array<String>) {

    //常用的類型轉換範例
    typeTransform()
}

fun typeTransform(){
    println(aInt.toFloat())
    println(aInt.toString())

    println(aDouble.toInt())
    println(aDouble.toString())

    
}